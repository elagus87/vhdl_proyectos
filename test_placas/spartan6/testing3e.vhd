----------------------------------------------------------------------------------
-- Company: UTNBA
-- Engineer: Agustin Ortiz
-- 
-- Create Date:    13:15:54 03/13/2020 
-- Design Name: 	 kit Test spartan 3e
-- Module Name:    testing3e - Behavioral 
-- Project Name: 	 kit Test
-- Target Devices: 
-- Tool versions:  14.7
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity testing3e is
    Port ( clk,rst : in  STD_LOGIC;
           sw : in  STD_LOGIC_VECTOR (3 downto 0);
           leds : out  STD_LOGIC_VECTOR (7 downto 0));
end testing3e;

architecture Behavioral of testing3e is

	signal st_reg,st_next:unsigned(25 downto 0);
	signal led_st_reg,led_st_next:std_logic_vector(3 downto 0);

begin

-- Temporizador de 1 seg--
reg : process( clk,rst )
begin
	if (rst='1') then
		st_reg <= (others=>'0');
		led_st_reg <= (others=>'0');
	elsif rising_edge(clk) then
		st_reg <= st_next;
		led_st_reg <= led_st_next;	
	end if;
end process ; -- reg

st_next <= (others=>'0') when st_reg=50000000 else
		   st_reg+1;

-- Led testigo --
led_st_next <= not(led_st_reg) when st_reg=0 else led_st_reg;
leds(3 downto 0) <= led_st_reg;
-- Leds con las llaves --
leds(7 downto 4) <= sw;

end Behavioral;

