#!/bin/bash
echo "Nombre del proyecto:"
read name
echo "Creando estructura de carpeta para CocoTB"
`mkdir $name`
`mkdir $name/hdl`
`mkdir $name/model`
`mkdir $name/test`
`touch $name/hdl/$name.vhd`
model_id="_model.py"
model_name="$name$model_id"
`touch $name/model/$model_name`
test_id="_test.py"
test_name="$name$test_id"
`touch $name/test/$test_name`
echo "Directorio creado con los archivos basicos"

# Makefile basico
DUT="$name"
TOPLEVEL_LANG="vhdl"
TEST="$test_name"
SHELL_PWD="\$(shell pwd)"
PWD_DEF="\$(PWD)"
OS="\$(OS)"
WPWD_DEF="\$(shell sh -c 'pwd -W')"
WPWD_SEL="\$(WPWD)"
PY_PATH="\$(PYTHONPATH)"
MAKE_INC="\$(shell cocotb-config --makefiles)/Makefile.inc"
MAKE_SIM="\$(shell cocotb-config --makefiles)/Makefile.sim"
test="_test"
MODULE="$DUT$test"
`echo -e "TOPLEVEL_LANG=$TOPLEVEL_LANG
SIM ?= ghdl \n
WAVES=1 \n
SIM_ARGS=--wave=$MODULE.ghw \n
PWD=$SHELL_PWD \n
ifeq ($OS,Msys) \n
WPWD=$WPWD_DEF \n
else \n
WPWD=$SHELL_PWD \n
endif \n
PYTHONPATH := $PWD_DEF/../model:$PY_PATH \n
VHDL_SOURCES = $WPWD_SEL/../hdl/$DUT.vhd \n
TOPLEVEL := $DUT \n
MODULE   := $MODULE #nombre del archivo que puede contener N test \n
include $MAKE_INC \n
include $MAKE_SIM \n
.ONESHELL: \n
wave: \n
	cd $PWD_DEF/sim_build \n
	gtkwave $MODULE.ghw ">> $name/test/Makefile`

echo "Makefile creado"
