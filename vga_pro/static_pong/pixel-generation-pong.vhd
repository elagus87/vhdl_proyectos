library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--##################################--

entity pong_graph_st is
   port(
      video_on: in std_logic;
      pixel_x,pixel_y: in std_logic_vector(9 downto 0);
      graph_rgb: out std_logic_vector(2 downto 0)
   );
end pong_graph_st;

--##################################--

architecture sq_ball_arch of pong_graph_st is
   -- coordenadas x, y de (0,0) a (639,479)
   signal pix_x, pix_y: unsigned(9 downto 0);
   constant MAX_X: integer:=640;
   constant MAX_Y: integer:=480;
   ----------------------------------------------
   -- Franja vertical como muro
   ----------------------------------------------
   -- limites de la pared
   constant WALL_X_L: integer:=32;
   constant WALL_X_R: integer:=35;
   ----------------------------------------------
   -- Barra vertical (paleta)
   ----------------------------------------------
   -- limites laterales
   constant BAR_X_L: integer:=600;
   constant BAR_X_R: integer:=603;
   -- limites superior e inferior
   constant BAR_Y_SIZE: integer:=72;
   constant BAR_Y_T: integer:=MAX_Y/2-BAR_Y_SIZE/2; --204
   constant BAR_Y_B: integer:=BAR_Y_T+BAR_Y_SIZE-1;
   ----------------------------------------------
   -- pelota cuadrada
   ----------------------------------------------
   constant BALL_SIZE: integer:=8;
   -- limites laterales
   constant BALL_X_L: integer:=580;
   constant BALL_X_R: integer:=BALL_X_L+BALL_SIZE-1;
   -- limites verticales
   constant BALL_Y_T: integer:=238;
   constant BALL_Y_B: integer:=BALL_Y_T+BALL_SIZE-1;
   ----------------------------------------------
   -- objetos de salidas
   ----------------------------------------------
   signal wall_on, bar_on, sq_ball_on: std_logic;
   signal wall_rgb, bar_rgb, ball_rgb:
          std_logic_vector(2 downto 0);

--*********************************--

begin
   pix_x <= unsigned(pixel_x);
   pix_y <= unsigned(pixel_y);
   ----------------------------------------------
   -- pared
   ----------------------------------------------
   -- pixel dentro de la pares
   wall_on <=
      '1' when (WALL_X_L<=pix_x) and (pix_x<=WALL_X_R) else
      '0';
   -- salida rgb
   wall_rgb <= "001"; -- azul
   ----------------------------------------------
   -- paleta
   ----------------------------------------------
   -- pixeles que conforman la paleta
   bar_on <=
      '1' when (BAR_X_L<=pix_x) and (pix_x<=BAR_X_R) and
               (bar_y_t<=pix_y) and (pix_y<=bar_y_b) else
      '0';
   -- salida rgb
   bar_rgb <= "010"; --verde
   ----------------------------------------------
   -- pelota
   ----------------------------------------------
   -- pixeles que conformal la pelota
   sq_ball_on <=
      '1' when (BALL_X_L<=pix_x) and (pix_x<=BALL_X_R) and
               (BALL_Y_T<=pix_y) and (pix_y<=BALL_Y_B) else
      '0';
   -- salida rgb
   ball_rgb <= "100";   -- rojo

--*********************************--

   ----------------------------------------------
   -- multiplexor RGB
   ----------------------------------------------
   process(video_on,wall_on,bar_on,sq_ball_on,
           wall_rgb, bar_rgb, ball_rgb)
   begin
      if video_on='0' then
          graph_rgb <= "000"; --negro
      else
         if wall_on='1' then
            graph_rgb <= wall_rgb;
         elsif bar_on='1' then
            graph_rgb <= bar_rgb;
         elsif sq_ball_on='1' then
            graph_rgb <= ball_rgb;
         else
            graph_rgb <= "110"; -- fondo amarillo
         end if;
      end if;
   end process;
end sq_ball_arch;