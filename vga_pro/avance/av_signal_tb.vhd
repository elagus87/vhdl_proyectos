--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:49:32 08/19/2019
-- Design Name:   
-- Module Name:   C:/Users/elagu/OneDrive/Documentos/vhdl_proyectos/vga_pro/avance/av_signal_tb.vhd
-- Project Name:  avance
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: av_signal
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY av_signal_tb IS
END av_signal_tb;
 
ARCHITECTURE behavior OF av_signal_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT av_signal
    generic(W:natural:=11);
    PORT(
         dMayor : IN  std_logic_vector(W-1 downto 0);
         dMenor : IN  std_logic_vector(W-1 downto 0);
         start : IN  std_logic;
         clk : IN  std_logic;
         rst : IN  std_logic;
         av : OUT  std_logic_vector(W+1 downto 0)
        );
    END COMPONENT;
    
  constant Tclk : time := 10 ns;
   constant Wt : natural:= 11; 

   --Inputs
   signal dMayor : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal dMenor : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal start : std_logic := '0';
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';

 	--Outputs
   signal av : std_logic_vector(Wt+1 downto 0);


 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: av_signal 
   GENERIC MAP (W => Wt)
   PORT MAP (
          dMayor => dMayor,
          dMenor => dMenor,
          start => start,
          clk => clk,
          rst => rst,
          av => av
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for Tclk/2;
		clk <= '0';
		wait for Tclk/2;
   end process;
 

   start_proc : process
   begin
      wait for Tclk*3;--3
      start<='1';
      wait for Tclk;--4
      start<='0';
      wait for 11*Tclk;--16
   end process ; -- start_proc

      -- Stimulus process
   stim_proc: process
   begin    
      rst<='1';
      wait for Tclk*2;--2 
      rst<='0';
		--diagonal 1er cuadrante, 11 iteraciones
      dMayor<=std_logic_vector(TO_SIGNED(10,Wt));
      dMenor<=std_logic_vector(TO_SIGNED(3,Wt));
		wait for Tclk;--3
      wait for Tclk;--4
      wait for Tclk*11;--15
		dMayor<=std_logic_vector(TO_SIGNED(10,Wt));
      dMenor<=std_logic_vector(TO_SIGNED(8,Wt));
		wait for Tclk*4;--20
		wait for Tclk*11;--20
      assert false
      report "fin de la simulacion" 
      severity failure;
   end process;

END;
