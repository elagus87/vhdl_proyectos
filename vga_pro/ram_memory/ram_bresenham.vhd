library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram_bresenham is
  port (CLK     : in std_logic;
        WE      : in std_logic;
        EN      : in std_logic;
        Xi      : in std_logic_vector(9 downto 0);--escritura
        Yi      : in std_logic_vector(9 downto 0);
        pixel_x :in std_logic_vector(9 downto 0);--barrido
        pixel_y :in std_logic_vector(9 downto 0);
        pixel_on: out std_logic--lectura
        );
end entity ; -- ram_bresenham

architecture arch of ram_bresenham is

    --correccion de coordenadas
    signal xip_r,yip_r:unsigned(9 downto 0); 
    signal xip,yip:unsigned(7 downto 0);
    signal pix_x_r,pix_y_r:unsigned(9 downto 0);
    signal pix_x,pix_y:unsigned(7 downto 0);

    --constant zero:std_logic_vector (255 downto 0):=(others=>'0');
    signal one:unsigned (255 downto 0):=(others=>'0');
    --datos
    signal ram_data_prev,ram_data_out,ram_data_in,new_data  :  std_logic_vector(255 downto 0);
    --estados
    type state_type is (s_wait,s_write,s_read,s_data_to_write);
    signal state_reg,state_next: state_type;
    --señales de RAM
    signal en_sig,we_sig:std_logic;
    signal ram_addr:std_logic_vector(7 downto 0);

    --los datos de entrada y el barrido entra en 10 bits pero mi memoria esta en 8

begin

  one<=TO_UNSIGNED(1,256);--señal que contiene un 1 en el LSB
  --ram basica
   u_ram: entity work.rams_04 
   port map(CLK  => clk,
      			WE   => we_sig,
      			EN   => en_sig,
      			ADDR => ram_addr,  
					  DI   => ram_data_in,
					  DO   => ram_data_out);

    -- corrijo las coordenadas del barrido
    pix_x_r<=unsigned(pixel_x)-to_unsigned(192,10);
    pix_y_r<=unsigned(pixel_y)-to_unsigned(112,10);
    pix_x<=pix_x_r(7 downto 0);
    pix_y<=pix_y_r(7 downto 0);
    -- corrijo las coordenadas provenientes del Bresenham
    xip_r<=unsigned(xi)-to_unsigned(192,10);--offsets de pantalla
    yip_r<=unsigned(yi)-to_unsigned(112,10);--por estar 256x256 en el centro de 640x480
    xip<=xip_r(7 downto 0);
    yip<=yip_r(7 downto 0);

    --columna(x) 1 en el punto marcado por x que ira a fila y
    --new_data<=zero(255 downto to_integer(xip))&'1'&zero(to_integer(xip)-2 downto 0);
    new_data<=std_logic_vector(one sll to_integer(xip));
  --registro de estado
  registro : process( clk )
  	  begin
  	    if rising_edge(clk) then
  	      state_reg<=state_next;
  	    end if ;
  end process ; -- registro

  ---maquina de estados
  data : process( clk,state_reg,WE,EN,yip,ram_data_out,new_data,pix_y )
      begin
      state_next<=s_data_to_write;
      en_sig<='0';
		pixel_on<='0';
      ram_data_in<=(others =>'0');
      ram_data_prev<=(others =>'0');
		we_sig<='0';
		ram_addr<=(others =>'0');
      case( state_reg ) is
            when s_data_to_write =>
              ram_addr<=std_logic_vector(yip);
              en_sig<='1';
              we_sig<='0';
              ram_data_prev<=ram_data_out;--lectura de dato previo
              state_next<=s_write;
            when s_write =>
				  ram_data_in<=(ram_data_prev or new_data);
              ram_addr<=std_logic_vector(yip);
              en_sig<='1';
              we_sig<='1';
              state_next<=s_wait;
            when s_read =>
              en_sig<='1';
              we_sig<='0';
              ram_addr<=std_logic_vector(pix_y);
              pixel_on<=ram_data_out(to_integer(pix_x));
              state_next<=s_wait;
            when s_wait =>
            if EN='1' then
              if we='1' then
                state_next<=s_data_to_write;
              else
                state_next<=s_read;
              end if ;
            else
              state_next<=s_wait;
            end if ;

      end case ;
   end process ; -- data
    

end architecture ; -- arch