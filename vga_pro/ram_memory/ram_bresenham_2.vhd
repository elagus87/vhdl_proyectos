library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ram_bresenham_2 is
  port (
    CLK     : in  std_logic;
    RST     : in  std_logic;
    WE      : in  std_logic;
    EN      : in  std_logic;
    ADDR    : in  std_logic_vector(  7 downto 0);
    DI      : in  std_logic_vector(255 downto 0);
    DO      : out std_logic_vector(255 downto 0)
  );
end ram_bresenham_2;

architecture Behavioral of ram_bresenham_2 is
  
  type state_type is (st_idle, st_read_only, st_read, st_write);
  signal state_reg: state_type;
  
  --señales de RAM
  signal we_reg			      :  std_logic;
	signal en_reg 		      :  std_logic;
  --los datos de entrada y el barrido entra en 10 bits pero mi memoria esta en 8

begin

  --ram basica
  u_ram: entity work.rams_04 
  port map(
		CLK  => clk,
    WE   => we_reg,
    EN   => en_reg,
    ADDR => ADDR,  
		DI   => DI,
		DO   => DO
	);
	
  registro : process( clk , rst ) begin
    if (rst = '1') then
      state_reg        <= st_idle;
      we_reg           <= '0';
      en_reg           <= '0';
    elsif rising_edge(clk) then
      case (state_reg) is 
        when st_idle =>
          if (en = '1' and we = '1' ) then
            state_reg        <= st_read;
            we_reg           <= '0';
            en_reg           <= '1';
          elsif (en = '1' and we = '0') then
            state_reg        <= st_read_only;
            we_reg           <= '0';
            en_reg           <= '1';
          end if;
        when st_read_only =>
            state_reg        <= st_idle;
            we_reg           <= '0';
            en_reg           <= '0';
        when st_read =>
            state_reg        <= st_write;
            we_reg           <= '1';
            en_reg           <= '1';
        when st_write =>
            state_reg        <= st_idle;
            we_reg           <= '0';
            en_reg           <= '0';
      end case;
  	end if ;
  end process ; -- registro
	
end architecture ; -- arch