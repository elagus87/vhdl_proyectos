library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--##################################--

entity shift_reg is
generic(W:natural:=11);
    Port ( clk : in  STD_LOGIC;
           syn_clr : in  STD_LOGIC;
           data : in  STD_LOGIC_VECTOR(W-1 downto 0);
           en : in  STD_LOGIC;
           load : in  STD_LOGIC;
           q : out  STD_LOGIC);
end shift_reg;

--##################################--

architecture Behavioral of shift_reg is
	signal s_reg,s_next:STD_LOGIC_VECTOR(W-1 downto 0);

--*********************************--

begin

	reg_estados : process( clk )
	begin
		if rising_edge(clk) then
			s_reg<=s_next;
		end if ;
	end process ; -- reg_estados

--*********************************--

	-- logica de estado futuro
	s_next<=(others=>'0') when syn_clr='1' else
           data when load ='1' else
		       s_reg(W-2 downto 0) & '0' when en='1' else
           s_reg;

--*********************************--

	-- salida
	q<=s_reg(W-1);
	
end Behavioral;

