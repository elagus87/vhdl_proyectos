--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:01:24 08/19/2019
-- Design Name:   
-- Module Name:   C:/Users/elagu/OneDrive/Documentos/vhdl_proyectos/vga_pro/delta/delta_tb.vhd
-- Project Name:  delta
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: delta
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY delta_tb IS
END delta_tb;
 
ARCHITECTURE behavior OF delta_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT delta
    generic(W:natural:=11);
    PORT(
         c0 : IN  std_logic_vector(W-1 downto 0);
         c1 : IN  std_logic_vector(W-1 downto 0);
         clk : IN  std_logic;
         start : IN  std_logic;
         rst : IN  std_logic;
         inc : OUT  STD_LOGIC_VECTOR (1 downto 0);
         d : OUT  std_logic_vector(W-1 downto 0)
        );
    END COMPONENT;
   constant Tclk : time := 10 ns;
   constant Wt : natural:= 11;

   --Inputs
   signal c0_t : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal c1_t : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal clk_t : std_logic := '0';
   signal start_t : std_logic := '0';
   signal rst_t : std_logic := '0';

 	--Outputs
   signal inc_t : STD_LOGIC_VECTOR (1 downto 0);
   signal d_t : std_logic_vector(Wt-1 downto 0);


 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: delta 
   GENERIC MAP (W => Wt)
   PORT MAP (
          c0 => c0_t,
          c1 => c1_t,
          clk => clk_t,
          start => start_t,
          rst => rst_t,
          inc => inc_t,
          d => d_t
        );

   -- Clock process definitions
   clk_process :process
   begin
  		clk_t <= '0';
  		wait for Tclk/2;
  		clk_t <= '1';
  		wait for Tclk/2;
   end process;
 

   start : process
   begin
      wait for Tclk*2;--2
      wait for Tclk*1;--3
      start_t<='1';
      wait for Tclk*1;--4
      start_t<='0';
      wait for Tclk*3;--7
      start_t<='1';
      wait for Tclk*1;--8
      start_t<='0';
      wait for Tclk*3;--11
      start_t<='1';
      wait for Tclk*1;--12
      start_t<='0';
      wait for Tclk*3;--15
      start_t<='1';
      wait for Tclk*1;--16
      start_t<='0';
      wait for Tclk*3;--19
      start_t<='1';
      wait for Tclk*1;--20
      start_t<='0';
      wait for Tclk*3;--23
      start_t<='1';
      wait for Tclk*1;--24
      start_t<='0';
      wait for Tclk*3;--27
      start_t<='1';
      wait for Tclk*1;--28
      start_t<='0';
      wait for Tclk*3;--31
      start_t<='1';
      wait for Tclk*1;--32
      start_t<='0';
      wait for Tclk*3;--35
   end process ; -- start
   -- Stimulus process
   stim_proc: process
   begin		
      rst_t<='1';
      wait for Tclk*2;--2	
      rst_t<='0';
      --horizontal positiva
      c0_t<=std_logic_vector(TO_SIGNED(0,Wt));
      c1_t<=std_logic_vector(TO_SIGNED(450,Wt));
      wait for Tclk*4;--6
      --vertical positiva
      c0_t<=std_logic_vector(TO_SIGNED(450,Wt));
      c1_t<=std_logic_vector(TO_SIGNED(0,Wt));
      wait for Tclk*4;--10
      c0_t<=std_logic_vector(TO_SIGNED(225,Wt));
      c1_t<=std_logic_vector(TO_SIGNED(0,Wt));
      wait for Tclk*4;--14
      c0_t<=std_logic_vector(TO_SIGNED(0,Wt));
      c1_t<=std_logic_vector(TO_SIGNED(225,Wt));
      wait for Tclk*4;--18
      --horizontal negativa
      c0_t<=std_logic_vector(TO_SIGNED(0,Wt));
      c1_t<=std_logic_vector(TO_SIGNED(-450,Wt));
      wait for Tclk*4;--22
      --vertical negativa
      c0_t<=std_logic_vector(TO_SIGNED(-450,Wt));
      c1_t<=std_logic_vector(TO_SIGNED(0,Wt));
      wait for Tclk*4;--26
      c0_t<=std_logic_vector(TO_SIGNED(-225,Wt));
      c1_t<=std_logic_vector(TO_SIGNED(0,Wt));
      wait for Tclk*4;--30
      c0_t<=std_logic_vector(TO_SIGNED(0,Wt));
      c1_t<=std_logic_vector(TO_SIGNED(-225,Wt));
      wait for Tclk*4;--34
      -- insert stimulus here
      assert false
      report "fin de la simulacion" 
      severity failure;
   end process;

END;
