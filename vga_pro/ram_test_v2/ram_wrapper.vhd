library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ram_wrapper is
   generic(
      RAM_ADDR_WIDTH: natural :=   8;
      RAM_DI_WIDTH  : natural := 256;
      RAM_DO_WIDTH  : natural := 256
   );
   port (
      CLK  : in   std_logic;
      RST  : in   std_logic;
      WE   : in   std_logic;
      EN   : in   std_logic;
      ADDR : in   std_logic_vector(RAM_ADDR_WIDTH-1 downto 0);
      DI   : in   std_logic_vector(  RAM_DI_WIDTH-1 downto 0);
      DO   : out  std_logic_vector(  RAM_DO_WIDTH-1 downto 0)
   );
end ram_wrapper;

architecture Behavioral of ram_wrapper is
  
   type   state_type is (
      ST_IDLE,
      ST_READ_ONLY,
      ST_READ,
      ST_WRITE
   );
   
   signal state_reg  : state_type                     ;
   signal we_reg     : std_logic                      ; --señales de RAM
   signal en_reg     : std_logic                      ; --señales de RAM
   signal one        : unsigned        (RAM_DI_WIDTH-1 downto 0) ; --los datos de entrada y el barrido entra en 10 bits pero mi memoria esta en 8
   signal data       : std_logic_vector(RAM_DI_WIDTH-1 downto 0) ; --los datos de entrada y el barrido entra en 10 bits pero mi memoria esta en 8

begin

   one   <= TO_UNSIGNED(1,256);--señal que contiene un 1 en el LSB
   data  <= std_logic_vector(one sll to_integer(unsigned(DI)));

  --ram basica
   u_ram: entity work.rams_04 
   generic map(
      RAM_ADDR_WIDTH => RAM_ADDR_WIDTH,
      RAM_DI_WIDTH   => RAM_DI_WIDTH  ,
      RAM_DO_WIDTH   => RAM_DO_WIDTH
   )
   port map(
      CLK  => clk,
      WE   => we_reg,
      EN   => en_reg,
      ADDR => ADDR,
      DI   => data,
      DO   => DO
   );
	
   registro : process( clk , rst ) begin
      if (rst = '1') then
         state_reg        <= ST_IDLE;
         we_reg           <= '0';
         en_reg           <= '0';
      elsif rising_edge(clk) then
         case (state_reg) is 
            when ST_IDLE =>
               if (en = '1' and we = '1' ) then
                  state_reg        <= ST_READ;
                  we_reg           <= '0';
                  en_reg           <= '1';
               elsif (en = '1' and we = '0') then
                  state_reg        <= ST_READ_ONLY;
                  we_reg           <= '0';
                  en_reg           <= '1';
               end if;
            when ST_READ_ONLY =>
               state_reg        <= ST_IDLE;
               we_reg           <= '0';
               en_reg           <= '0';
            when ST_READ =>
               state_reg        <= ST_WRITE;
               we_reg           <= '1';
               en_reg           <= '1';
            when ST_WRITE =>
               state_reg        <= ST_IDLE;
               we_reg           <= '0';
               en_reg           <= '0';
         end case;
      end if ;
   end process ; -- registro
	
end architecture ; -- arch
