library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ram_wrapper is
   generic(W         : natural := 11;
           RAM_DO_W  : natural := 256);
   port (
      CLK  : in   std_logic;
      RST  : in   std_logic;
      WE   : in   std_logic;
      EN   : in   std_logic;
      X    : in   std_logic_vector(W-1 downto 0);
      Y    : in   std_logic_vector(W-1 downto 0);
      DO   : out  std_logic_vector(RAM_DO_W-1 downto 0)
   );
end ram_wrapper;

architecture Behavioral of ram_wrapper is
  
   type   state_type is (
      ST_IDLE,
      ST_READ_ONLY,
      ST_READ,
      ST_WRITE
   );
   
   signal ram_vector : std_logic_vector((256*256)-1 downto 0) ;
   signal state_reg  : state_type                     ;

begin

   registro : process( clk , rst ) begin
      if (rst = '1') then
         state_reg        <= ST_IDLE;

      elsif rising_edge(clk) then
         case (state_reg) is 
            when ST_IDLE =>
               if (en = '1' and we = '1' ) then
                  state_reg        <= ST_READ;
               elsif (en = '1' and we = '0') then
                  state_reg        <= ST_READ;
               end if;
            when ST_READ =>
            -- leo "fila" Y
               DO<=ram_vector(to_integer(unsigned(Y*RAM_DO_W)) to  to_integer(unsigned(Y*RAM_DO_W)-1));
               state_reg        <= ST_WRITE;
            when ST_WRITE =>
            -- escrivo (Y*256)+X
               ram_vector(to_integer(unsigned((Y*RAM_DO_W)+X)))<='1';
               state_reg        <= ST_IDLE;
         end case;
      end if ;
   end process ; -- registro
	
end architecture ; -- arch
