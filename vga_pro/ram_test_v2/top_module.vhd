library ieee;
use ieee.std_logic_1164.all;

--##################################--

entity top_module is
   generic(
      RAM_ADDR_WIDTH: natural :=   8;
      RAM_DI_WIDTH  : natural := 256;
      RAM_DO_WIDTH  : natural := 256;
      ROM_ADDR_WIDTH: natural :=   8;
      ROM_DO_WIDTH  : natural := 256
   );
   port (
      clk   :  in    std_logic;
      rst   :  in    std_logic;
      en    :  in    std_logic;
      hsync :  out   std_logic;
      vsync :  out   std_logic;
      rgb   :  out   std_logic_vector(2 downto 0)
   );
end top_module;

--##################################--


architecture arch of top_module is

   signal pixel_x       : std_logic_vector(  9 downto 0);
   signal pixel_y       : std_logic_vector(  9 downto 0);
   signal h_end         : std_logic;
   signal v_end         : std_logic;
   signal video_on      : std_logic;
   signal pixel_tick    : std_logic;

   signal rom_addr_out  : std_logic_vector(ROM_ADDR_WIDTH-1 downto 0);
   signal rom_data      : std_logic_vector(  ROM_DO_WIDTH-1 downto 0);

   signal ram_we        : std_logic;
   signal ram_en        : std_logic;
   signal ram_addr_out  : std_logic_vector(RAM_ADDR_WIDTH-1 downto 0);
   signal ram_data_in   : std_logic_vector(  RAM_DI_WIDTH-1 downto 0);
   signal ram_y  : std_logic_vector(  RAM_DO_WIDTH-1 downto 0);

   signal rgb_reg, rgb_next: std_logic_vector(2 downto 0);

--*********************************--
   attribute keep_hierarchy : string;
   attribute keep_hierarchy of arch: architecture is "TRUE";
   
begin

   -- instancia de  VGA sync
   u_vga_sync: entity work.vga_sync
   port map(
      clk         => clk         ,
      rst         => rst         ,
      video_on    => video_on    ,
      p_tick      => pixel_tick  ,
      hsync       => hsync       ,
      vsync       => vsync       ,
      pixel_x     => pixel_x     ,
      pixel_y     => pixel_y     ,
      h_end       => h_end       ,
      v_end       => v_end
   );

   u_fsm_sr: entity work.fsm_sr_lineal
   generic map(
      RAM_ADDR_WIDTH => RAM_ADDR_WIDTH,
      RAM_DO_WIDTH   => RAM_DO_WIDTH  ,
      ROM_ADDR_WIDTH => ROM_ADDR_WIDTH,
      ROM_DO_WIDTH   => ROM_DO_WIDTH
   )
   port map(
      clk          => clk         ,
      rst          => rst         ,
      en           => en          ,
      pixel_x      => pixel_x     ,
      pixel_y      => pixel_y     ,
      h_end        => h_end       ,
      v_end        => v_end       ,
      video_on     => video_on    ,
      do_rom       => rom_data    ,
      do_ram       => ram_data_out,
      rom_addr_out => rom_addr_out,
      ram_y        => ram_y       ,
      ram_we       => ram_we      ,
      ram_en       => ram_en      ,
      graph_rgb    => rgb_next
   );


   u_rom_clock: entity work.reloj_simple_256x256
   port map(
      address     => rom_addr_out ,
      data        => rom_data
   );

   --u_ram_wrapper: entity work.ram_wrapper
   --generic map(
   --   RAM_ADDR_WIDTH => RAM_ADDR_WIDTH,
   --   RAM_DI_WIDTH   => RAM_DI_WIDTH  ,
   --   RAM_DO_WIDTH   => RAM_DO_WIDTH
   --)
   --port map(
   --   CLK  => clk         ,
   --   rst  => rst         ,
   --   WE   => ram_we      ,
   --   EN   => ram_en      ,
   --   ADDR => ram_addr_out,
   --   DI   => ram_data_in ,
   --   DO   => ram_data_out
   --);

   u_ram_wrapper: entity work.ram_wrapper_lineal
   generic map(W => 11)
   port map(
      CLK  => clk         ,
      rst  => rst         ,
      WE   => ram_we      ,
      EN   => ram_en      ,
      X    => ram_x       ,
      Y    => ram_y       ,
      DO   => ram_data_out
   );

--*********************************--
--*********************************--

   rgb_buffer:process (clk)
   begin
      if rising_edge(clk) then
         if (pixel_tick='1') then
            rgb_reg <= rgb_next;
         end if;
      end if;
   end process;
	
	rgb <= rgb_reg;

end arch;
