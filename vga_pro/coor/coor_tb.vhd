LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY coor_tb IS
END coor_tb;
 
ARCHITECTURE behavior OF coor_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT coor
	 generic(W:natural:=11);
    PORT(
         av : IN  std_logic_vector(W+1 downto 0);
         ci,cf : IN  std_logic_vector(W-1 downto 0);
         inc_I : IN  STD_LOGIC_VECTOR (1 downto 0);
         inc_R : IN  STD_LOGIC_VECTOR (1 downto 0);
         dMenor : IN  std_logic_vector(W-1 downto 0);
         start : IN  std_logic;
         rst : IN  std_logic;
         clk : IN  std_logic;
         done: OUT std_logic;
         coor : OUT  std_logic_vector(W-1 downto 0)
        );
    END COMPONENT;
   constant Tclk : time := 10 ns;
   constant Wt : natural:= 11;  

   --Inputs
   signal av : std_logic_vector(Wt+1 downto 0) := (others => '0');
   signal ci,cf : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal inc_I : STD_LOGIC_VECTOR (1 downto 0) := "00";
   signal inc_R : STD_LOGIC_VECTOR (1 downto 0) := "00";
   signal dMenor : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal start : std_logic := '0';
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal coor_t : std_logic_vector(Wt-1 downto 0);
   signal done: std_logic;

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: coor PORT MAP (
          av => av,
          ci => ci,
          cf => cf,
          inc_I => inc_I,
          inc_R => inc_R,
          dMenor => dMenor,
          start => start,
          rst => rst,
          clk => clk,
          coor => coor_t,
          done => done
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for Tclk/2;
		clk <= '1';
		wait for Tclk/2;
   end process;
 
	start_proc : process
   begin
      wait for Tclk*3;--3
      start<='1';
      wait for Tclk;--4
      start<='0';
      wait for Tclk*10;--14
      start<='1';
      wait for Tclk;--15
      start<='0';
      wait for Tclk*10;--26
   end process ; -- start_proc

	av_proc : process
   begin
      wait for Tclk*3;--3
      --start<='1';		
      wait for Tclk;--4
      --start<='0';
		av<= std_logic_vector(TO_SIGNED(-4,Wt+2));
		wait for Tclk;--5
		av<= std_logic_vector(TO_SIGNED(2,Wt+2));
		wait for Tclk;--6
		av<= std_logic_vector(TO_SIGNED(-12,Wt+2));
		wait for Tclk;--7
		av<= std_logic_vector(TO_SIGNED(-6,Wt+2));
		wait for Tclk;--8
		av<= std_logic_vector(TO_SIGNED(0,Wt+2));
		wait for Tclk;--9
		av<= std_logic_vector(TO_SIGNED(-14,Wt+2));
		wait for Tclk;--10
		av<= std_logic_vector(TO_SIGNED(-8,Wt+2));
		wait for Tclk;--11
		av<= std_logic_vector(TO_SIGNED(-2,Wt+2));
		wait for Tclk;--12
		av<= std_logic_vector(TO_SIGNED(4,Wt+2));
		wait for Tclk;--13
		av<= std_logic_vector(TO_SIGNED(-10,Wt+2));
		wait for Tclk;--14
		av<= std_logic_vector(TO_SIGNED(-4,Wt+2));
		wait for Tclk;--15
      --start<='1';
      wait for Tclk;--16
      --start<='0';
    av<= std_logic_vector(TO_SIGNED(6,Wt+2));
    wait for Tclk;--17
    av<= std_logic_vector(TO_SIGNED(2,Wt+2));
    wait for Tclk;--18
    av<= std_logic_vector(TO_SIGNED(-2,Wt+2));
    wait for Tclk;--19
    av<= std_logic_vector(TO_SIGNED(14,Wt+2));
    wait for Tclk;--20
    av<= std_logic_vector(TO_SIGNED(10,Wt+2));
    wait for Tclk;--21
    av<= std_logic_vector(TO_SIGNED(6,Wt+2));
    wait for Tclk;--22
    av<= std_logic_vector(TO_SIGNED(2,Wt+2));
    wait for Tclk;--23
    av<= std_logic_vector(TO_SIGNED(-2,Wt+2));
    wait for Tclk;--24
    av<= std_logic_vector(TO_SIGNED(14,Wt+2));
    wait for Tclk;--25
    av<= std_logic_vector(TO_SIGNED(10,Wt+2));
    wait for Tclk;--26
    av<= std_logic_vector(TO_SIGNED(6,Wt+2));
    wait for Tclk;--27
    wait for Tclk;--28
		
   end process ; -- av_proc
    -- Stimulus process
   stim_proc: process
   begin		
      rst     <= '1';
      wait for Tclk*2;--2 
      rst<='0';
		 -- simulo caso de x entre 0 y 10, y entre 0 y 3
		 
		 ci<= std_logic_vector(TO_SIGNED(0,Wt));
     cf<= std_logic_vector(TO_SIGNED(10,Wt));
		 inc_I<= "01";
		 inc_R<= "01";
		 dMenor<= std_logic_vector(TO_UNSIGNED(3,Wt));

      wait for Tclk;--3
      wait for Tclk*11;--14
      wait for Tclk;--15

     -- simulo caso de x entre 0 y -8, y entre 0 y 10
     ci<= std_logic_vector(TO_SIGNED(0,Wt));
     cf<= std_logic_vector(TO_SIGNED(-8,Wt));
     inc_I<= "11";
     inc_R<= "00";
     dMenor<= std_logic_vector(TO_UNSIGNED(3,Wt));

      wait for Tclk*11;--26
      assert false
      report "fin de la simulacion" 
      severity failure;
   end process;

END;
