/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/agus/UTN/vhdl_proyectos/vga_pro/coor/coor.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_1242562249;

char *ieee_p_1242562249_sub_17126692536656888728_1035706684(char *, char *, int , int );
unsigned char ieee_p_1242562249_sub_3044623114557230561_1035706684(char *, char *, char *, char *, char *);
char *ieee_p_1242562249_sub_3525738511873186323_1035706684(char *, char *, char *, char *, char *, char *);
unsigned char ieee_p_1242562249_sub_3538360624587631881_1035706684(char *, char *, char *, int );
unsigned char ieee_p_1242562249_sub_3538367364753032487_1035706684(char *, char *, char *, int );
unsigned char ieee_p_2592010699_sub_2763492388968962707_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_1496293165_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(30, ng0);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 7112);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 13U);
    xsi_driver_first_trans_fast(t1);

LAB2:    t7 = (t0 + 6920);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1496293165_3212880686_p_1(char *t0)
{
    char t3[16];
    char *t1;
    char *t2;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;

LAB0:    xsi_set_current_line(31, ng0);

LAB3:    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t4 = ((IEEE_P_2592010699) + 4000);
    t5 = (t0 + 10496U);
    t1 = xsi_base_array_concat(t1, t3, t4, (char)99, (unsigned char)2, (char)97, t2, t5, (char)101);
    t6 = (t0 + 7176);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 12U);
    xsi_driver_first_trans_fast(t6);

LAB2:    t11 = (t0 + 6936);
    *((int *)t11) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1496293165_3212880686_p_2(char *t0)
{
    char t8[16];
    char t23[16];
    char t34[16];
    char *t1;
    char *t2;
    unsigned char t4;
    unsigned int t5;
    char *t6;
    char *t7;
    char *t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    unsigned char t19;
    unsigned int t20;
    char *t21;
    char *t22;
    int t24;
    char *t25;
    char *t26;
    unsigned int t27;
    unsigned char t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t35;
    char *t36;
    unsigned int t37;
    unsigned char t38;
    char *t39;
    char *t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;

LAB0:    xsi_set_current_line(32, ng0);
    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t1 = (t0 + 10726);
    t4 = 1;
    if (2U == 2U)
        goto LAB5;

LAB6:    t4 = 0;

LAB7:    if (t4 != 0)
        goto LAB3;

LAB4:    t16 = (t0 + 1672U);
    t17 = *((char **)t16);
    t16 = (t0 + 10728);
    t19 = 1;
    if (2U == 2U)
        goto LAB15;

LAB16:    t19 = 0;

LAB17:    if (t19 != 0)
        goto LAB13;

LAB14:
LAB23:    t35 = ieee_p_1242562249_sub_17126692536656888728_1035706684(IEEE_P_1242562249, t34, 0, 11);
    t36 = (t34 + 12U);
    t37 = *((unsigned int *)t36);
    t37 = (t37 * 1U);
    t38 = (11U != t37);
    if (t38 == 1)
        goto LAB25;

LAB26:    t39 = (t0 + 7240);
    t40 = (t39 + 56U);
    t41 = *((char **)t40);
    t42 = (t41 + 56U);
    t43 = *((char **)t42);
    memcpy(t43, t35, 11U);
    xsi_driver_first_trans_fast(t39);

LAB2:    t44 = (t0 + 6952);
    *((int *)t44) = 1;

LAB1:    return;
LAB3:    t9 = ieee_p_1242562249_sub_17126692536656888728_1035706684(IEEE_P_1242562249, t8, 1, 11);
    t10 = (11U != 11U);
    if (t10 == 1)
        goto LAB11;

LAB12:    t11 = (t0 + 7240);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t9, 11U);
    xsi_driver_first_trans_fast(t11);
    goto LAB2;

LAB5:    t5 = 0;

LAB8:    if (t5 < 2U)
        goto LAB9;
    else
        goto LAB7;

LAB9:    t6 = (t2 + t5);
    t7 = (t1 + t5);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB6;

LAB10:    t5 = (t5 + 1);
    goto LAB8;

LAB11:    xsi_size_not_matching(11U, 11U, 0);
    goto LAB12;

LAB13:    t24 = (-(1));
    t25 = ieee_p_1242562249_sub_17126692536656888728_1035706684(IEEE_P_1242562249, t23, t24, 11);
    t26 = (t23 + 12U);
    t27 = *((unsigned int *)t26);
    t27 = (t27 * 1U);
    t28 = (11U != t27);
    if (t28 == 1)
        goto LAB21;

LAB22:    t29 = (t0 + 7240);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    t32 = (t31 + 56U);
    t33 = *((char **)t32);
    memcpy(t33, t25, 11U);
    xsi_driver_first_trans_fast(t29);
    goto LAB2;

LAB15:    t20 = 0;

LAB18:    if (t20 < 2U)
        goto LAB19;
    else
        goto LAB17;

LAB19:    t21 = (t17 + t20);
    t22 = (t16 + t20);
    if (*((unsigned char *)t21) != *((unsigned char *)t22))
        goto LAB16;

LAB20:    t20 = (t20 + 1);
    goto LAB18;

LAB21:    xsi_size_not_matching(11U, t27, 0);
    goto LAB22;

LAB24:    goto LAB2;

LAB25:    xsi_size_not_matching(11U, t37, 0);
    goto LAB26;

}

static void work_a_1496293165_3212880686_p_3(char *t0)
{
    char t8[16];
    char t25[16];
    char t36[16];
    char *t1;
    char *t2;
    unsigned char t4;
    unsigned int t5;
    char *t6;
    char *t7;
    char *t9;
    char *t10;
    unsigned int t11;
    unsigned char t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    unsigned char t21;
    unsigned int t22;
    char *t23;
    char *t24;
    int t26;
    char *t27;
    char *t28;
    unsigned int t29;
    unsigned char t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t37;
    char *t38;
    unsigned int t39;
    unsigned char t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;

LAB0:    xsi_set_current_line(35, ng0);
    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t1 = (t0 + 10730);
    t4 = 1;
    if (2U == 2U)
        goto LAB5;

LAB6:    t4 = 0;

LAB7:    if (t4 != 0)
        goto LAB3;

LAB4:    t18 = (t0 + 1512U);
    t19 = *((char **)t18);
    t18 = (t0 + 10732);
    t21 = 1;
    if (2U == 2U)
        goto LAB15;

LAB16:    t21 = 0;

LAB17:    if (t21 != 0)
        goto LAB13;

LAB14:
LAB23:    t37 = ieee_p_1242562249_sub_17126692536656888728_1035706684(IEEE_P_1242562249, t36, 0, 11);
    t38 = (t36 + 12U);
    t39 = *((unsigned int *)t38);
    t39 = (t39 * 1U);
    t40 = (11U != t39);
    if (t40 == 1)
        goto LAB25;

LAB26:    t41 = (t0 + 7304);
    t42 = (t41 + 56U);
    t43 = *((char **)t42);
    t44 = (t43 + 56U);
    t45 = *((char **)t44);
    memcpy(t45, t37, 11U);
    xsi_driver_first_trans_fast(t41);

LAB2:    t46 = (t0 + 6968);
    *((int *)t46) = 1;

LAB1:    return;
LAB3:    t9 = ieee_p_1242562249_sub_17126692536656888728_1035706684(IEEE_P_1242562249, t8, 1, 11);
    t10 = (t8 + 12U);
    t11 = *((unsigned int *)t10);
    t11 = (t11 * 1U);
    t12 = (11U != t11);
    if (t12 == 1)
        goto LAB11;

LAB12:    t13 = (t0 + 7304);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t9, 11U);
    xsi_driver_first_trans_fast(t13);
    goto LAB2;

LAB5:    t5 = 0;

LAB8:    if (t5 < 2U)
        goto LAB9;
    else
        goto LAB7;

LAB9:    t6 = (t2 + t5);
    t7 = (t1 + t5);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB6;

LAB10:    t5 = (t5 + 1);
    goto LAB8;

LAB11:    xsi_size_not_matching(11U, t11, 0);
    goto LAB12;

LAB13:    t26 = (-(1));
    t27 = ieee_p_1242562249_sub_17126692536656888728_1035706684(IEEE_P_1242562249, t25, t26, 11);
    t28 = (t25 + 12U);
    t29 = *((unsigned int *)t28);
    t29 = (t29 * 1U);
    t30 = (11U != t29);
    if (t30 == 1)
        goto LAB21;

LAB22:    t31 = (t0 + 7304);
    t32 = (t31 + 56U);
    t33 = *((char **)t32);
    t34 = (t33 + 56U);
    t35 = *((char **)t34);
    memcpy(t35, t27, 11U);
    xsi_driver_first_trans_fast(t31);
    goto LAB2;

LAB15:    t22 = 0;

LAB18:    if (t22 < 2U)
        goto LAB19;
    else
        goto LAB17;

LAB19:    t23 = (t19 + t22);
    t24 = (t18 + t22);
    if (*((unsigned char *)t23) != *((unsigned char *)t24))
        goto LAB16;

LAB20:    t22 = (t22 + 1);
    goto LAB18;

LAB21:    xsi_size_not_matching(11U, t29, 0);
    goto LAB22;

LAB24:    goto LAB2;

LAB25:    xsi_size_not_matching(11U, t39, 0);
    goto LAB26;

}

static void work_a_1496293165_3212880686_p_4(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;

LAB0:    xsi_set_current_line(42, ng0);
    t1 = (t0 + 2152U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 2272U);
    t3 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB5;

LAB6:
LAB3:    t1 = (t0 + 6984);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(43, ng0);
    t1 = xsi_get_transient_memory(11U);
    memset(t1, 0, 11U);
    t5 = t1;
    memset(t5, (unsigned char)2, 11U);
    t6 = (t0 + 7368);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 11U);
    xsi_driver_first_trans_fast(t6);
    goto LAB3;

LAB5:    xsi_set_current_line(45, ng0);
    t2 = (t0 + 3112U);
    t5 = *((char **)t2);
    t2 = (t0 + 7368);
    t6 = (t2 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t5, 11U);
    xsi_driver_first_trans_fast(t2);
    goto LAB3;

}

static void work_a_1496293165_3212880686_p_5(char *t0)
{
    char t13[16];
    char t31[16];
    char t46[16];
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    unsigned char t12;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    unsigned int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    unsigned char t30;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;
    unsigned int t38;
    unsigned int t39;
    unsigned char t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t47;
    char *t48;
    char *t49;
    char *t50;
    char *t51;
    char *t52;
    unsigned int t53;
    unsigned int t54;
    unsigned char t55;
    char *t56;
    char *t57;
    char *t58;
    char *t59;
    char *t60;
    char *t61;

LAB0:    xsi_set_current_line(49, ng0);
    t1 = (t0 + 1992U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB3;

LAB4:    t10 = (t0 + 3272U);
    t11 = *((char **)t10);
    t10 = (t0 + 10560U);
    t12 = ieee_p_1242562249_sub_3538360624587631881_1035706684(IEEE_P_1242562249, t11, t10, 0);
    if (t12 != 0)
        goto LAB5;

LAB6:    t28 = (t0 + 2792U);
    t29 = *((char **)t28);
    t28 = (t0 + 10528U);
    t30 = ieee_p_1242562249_sub_3538367364753032487_1035706684(IEEE_P_1242562249, t29, t28, 0);
    if (t30 != 0)
        goto LAB9;

LAB10:
LAB13:    t47 = (t0 + 2952U);
    t48 = *((char **)t47);
    t47 = (t0 + 10544U);
    t49 = (t0 + 3432U);
    t50 = *((char **)t49);
    t49 = (t0 + 10576U);
    t51 = ieee_p_1242562249_sub_3525738511873186323_1035706684(IEEE_P_1242562249, t46, t48, t47, t50, t49);
    t52 = (t46 + 12U);
    t53 = *((unsigned int *)t52);
    t54 = (1U * t53);
    t55 = (11U != t54);
    if (t55 == 1)
        goto LAB15;

LAB16:    t56 = (t0 + 7432);
    t57 = (t56 + 56U);
    t58 = *((char **)t57);
    t59 = (t58 + 56U);
    t60 = *((char **)t59);
    memcpy(t60, t51, 11U);
    xsi_driver_first_trans_fast(t56);

LAB2:    t61 = (t0 + 7000);
    *((int *)t61) = 1;

LAB1:    return;
LAB3:    t1 = (t0 + 1192U);
    t5 = *((char **)t1);
    t1 = (t0 + 7432);
    t6 = (t1 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t5, 11U);
    xsi_driver_first_trans_fast(t1);
    goto LAB2;

LAB5:    t14 = (t0 + 2952U);
    t15 = *((char **)t14);
    t14 = (t0 + 10544U);
    t16 = (t0 + 3432U);
    t17 = *((char **)t16);
    t16 = (t0 + 10576U);
    t18 = ieee_p_1242562249_sub_3525738511873186323_1035706684(IEEE_P_1242562249, t13, t15, t14, t17, t16);
    t19 = (t13 + 12U);
    t20 = *((unsigned int *)t19);
    t21 = (1U * t20);
    t22 = (11U != t21);
    if (t22 == 1)
        goto LAB7;

LAB8:    t23 = (t0 + 7432);
    t24 = (t23 + 56U);
    t25 = *((char **)t24);
    t26 = (t25 + 56U);
    t27 = *((char **)t26);
    memcpy(t27, t18, 11U);
    xsi_driver_first_trans_fast(t23);
    goto LAB2;

LAB7:    xsi_size_not_matching(11U, t21, 0);
    goto LAB8;

LAB9:    t32 = (t0 + 2952U);
    t33 = *((char **)t32);
    t32 = (t0 + 10544U);
    t34 = (t0 + 3592U);
    t35 = *((char **)t34);
    t34 = (t0 + 10576U);
    t36 = ieee_p_1242562249_sub_3525738511873186323_1035706684(IEEE_P_1242562249, t31, t33, t32, t35, t34);
    t37 = (t31 + 12U);
    t38 = *((unsigned int *)t37);
    t39 = (1U * t38);
    t40 = (11U != t39);
    if (t40 == 1)
        goto LAB11;

LAB12:    t41 = (t0 + 7432);
    t42 = (t41 + 56U);
    t43 = *((char **)t42);
    t44 = (t43 + 56U);
    t45 = *((char **)t44);
    memcpy(t45, t36, 11U);
    xsi_driver_first_trans_fast(t41);
    goto LAB2;

LAB11:    xsi_size_not_matching(11U, t39, 0);
    goto LAB12;

LAB14:    goto LAB2;

LAB15:    xsi_size_not_matching(11U, t54, 0);
    goto LAB16;

}

static void work_a_1496293165_3212880686_p_6(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(54, ng0);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 7496);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 11U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 7016);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1496293165_3212880686_p_7(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    unsigned char t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;

LAB0:    xsi_set_current_line(55, ng0);
    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 10544U);
    t3 = (t0 + 1352U);
    t4 = *((char **)t3);
    t3 = (t0 + 10448U);
    t5 = ieee_p_1242562249_sub_3044623114557230561_1035706684(IEEE_P_1242562249, t2, t1, t4, t3);
    if (t5 != 0)
        goto LAB3;

LAB4:
LAB5:    t11 = (t0 + 7560);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    *((unsigned char *)t15) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t11);

LAB2:    t16 = (t0 + 7032);
    *((int *)t16) = 1;

LAB1:    return;
LAB3:    t6 = (t0 + 7560);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t6);
    goto LAB2;

LAB6:    goto LAB2;

}


extern void work_a_1496293165_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1496293165_3212880686_p_0,(void *)work_a_1496293165_3212880686_p_1,(void *)work_a_1496293165_3212880686_p_2,(void *)work_a_1496293165_3212880686_p_3,(void *)work_a_1496293165_3212880686_p_4,(void *)work_a_1496293165_3212880686_p_5,(void *)work_a_1496293165_3212880686_p_6,(void *)work_a_1496293165_3212880686_p_7};
	xsi_register_didat("work_a_1496293165_3212880686", "isim/coor_tb_isim_beh.exe.sim/work/a_1496293165_3212880686.didat");
	xsi_register_executes(pe);
}
