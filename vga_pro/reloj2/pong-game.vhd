library ieee;
use ieee.std_logic_1164.all;

--##################################--

entity reloj_top_st is
   port (
      clk,rst: 		in std_logic;
      hsync, vsync: 	out  std_logic;
      rgb: 				out std_logic_vector(2 downto 0)
   );
end reloj_top_st;

--##################################--


architecture arch of reloj_top_st is
   signal pixel_x, pixel_y: std_logic_vector (9 downto 0);
   signal video_on, pixel_tick: std_logic;
   signal rgb_reg, rgb_next: std_logic_vector(2 downto 0);
	
--*********************************--

begin
   -- instancia de  VGA sync
   vga_sync_unit: entity work.vga_sync
      port map(clk=>clk, 
               rst=>rst,
               video_on=>video_on, 
               p_tick=>pixel_tick,
               hsync=>hsync, 
               vsync=>vsync,
               pixel_x=>pixel_x, 
               pixel_y=>pixel_y);
   -- instancia del generador grafico
   reloj_st: entity work.reloj_st
      port map (video_on=>video_on,
                pixel_x=>pixel_x, 
                pixel_y=>pixel_y,
                graph_rgb=>rgb_next);

--*********************************--

   rgb_buffer:process (clk)
   begin
      if rising_edge(clk) then
         if (pixel_tick='1') then
            rgb_reg <= rgb_next;
         end if;
      end if;
   end process;

--*********************************--

   rgb <= rgb_reg;
end arch;