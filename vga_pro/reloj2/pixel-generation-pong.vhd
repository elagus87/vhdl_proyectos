library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--##################################--

entity reloj_st is
   port(
      video_on: in std_logic;
      pixel_x,pixel_y: in std_logic_vector(9 downto 0);
      graph_rgb: out std_logic_vector(2 downto 0)
   );
end reloj_st;

--##################################--

architecture reloj_arch of reloj_st is
   -- coordenadas x, y de (0,0) a (639,479)
   signal pix_x, pix_y: unsigned(9 downto 0);
   constant MAX_X: integer:=640;
   constant MAX_Y: integer:=480;
   ----------------------------------------------

   ----------------------------------------------
   -- Reloj
   ----------------------------------------------

	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
   constant CLK_SIZE: integer:=450;
   constant CLK_WORD: integer:=9; -- log2(CLK_SIZE)
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
   -- limites laterales
   constant CLK_X_L: integer:=320-(CLK_SIZE/2);
   constant CLK_X_R: integer:=CLK_X_L+CLK_SIZE-1;
   -- limites verticales
   constant CLK_Y_T: integer:=240-(CLK_SIZE/2);
   constant CLK_Y_B: integer:=CLK_Y_T+CLK_SIZE-1;

	signal y_top,x_left: unsigned(9 downto 0);

   ----------------------------------------------
   -- objetos de salidas
   ----------------------------------------------
--   signal wall_on, bar_on: std_logic;
	signal center_clk_on: std_logic;
	signal clk_on: std_logic;
--   signal wall_rgb, bar_rgb:std_logic_vector(2 downto 0);
	signal clk_rgb:std_logic_vector(2 downto 0);
          

	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
   ----------------------------------------------
   -- ROM para reloj
   ----------------------------------------------
	signal rom_addr: std_logic_vector( CLK_WORD-1 downto 0);
	signal rom_col: unsigned(CLK_WORD-1 downto 0);
   signal rom_data: std_logic_vector(CLK_SIZE-1 downto 0);
   signal rom_bit: std_logic;
	
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

--*********************************--

begin


	reloj: entity work.reloj_module
   port map(
				address=> rom_addr,
				data=> rom_data);
					
   pix_x <= unsigned(pixel_x);
   pix_y <= unsigned(pixel_y);

   ----------------------------------------------
   -- pelota
   ----------------------------------------------
   -- pixeles que conformal la pelota
	-- limites

	
   center_clk_on <=
      '1' when (CLK_X_L<=pix_x) and (pix_x<=CLK_X_R) and
               (CLK_Y_T<=pix_y) and (pix_y<=CLK_Y_B) else
      '0';
   -- salida rgb
   clk_rgb <= "100";   -- rojo
	
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	-- mapeo de la localizacion actual a ROM addr/col
	-- cambiar los tama�os en funcion de la imagen
	
	y_top<=to_unsigned(CLK_Y_T,10);
	x_left<=to_unsigned(CLK_X_L,10);

   rom_addr <= std_logic_vector (pix_y(CLK_WORD-1 downto 0) - y_top(CLK_WORD-1 downto 0));
   rom_col <= pix_x(CLK_WORD-1 downto 0) - x_left(CLK_WORD-1 downto 0);
   rom_bit <= rom_data(to_integer(rom_col)-1);
	
	   -- pixeles donde est� el reloj
   clk_on <=
      '1' when (center_clk_on='1') and (rom_bit='1') else
      '0';
   
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

--*********************************--


   process(center_clk_on, clk_rgb,video_on,clk_on)
   begin
      if video_on='0' then
          graph_rgb <= "000"; --negro
      else
         if clk_on='1' then
            graph_rgb <= clk_rgb;
         else
            graph_rgb <= "110"; -- fondo amarillo
         end if;
      end if;
   end process;
end reloj_arch;