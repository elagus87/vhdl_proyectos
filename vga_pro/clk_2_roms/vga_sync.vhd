library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

--##################################--

entity vga_sync is
  port(
    clk       : in  std_logic;
    rst       : in  std_logic;
    hsync     : out std_logic;
    vsync     : out std_logic;
    video_on  : out std_logic;
    p_tick    : out std_logic;
    pixel_x   : out std_logic_vector(9 downto 0);
    pixel_y   : out std_logic_vector(9 downto 0)
);
end vga_sync;

--##################################--

architecture arch of vga_sync is
  --resolution: 800-by-600 pixels
  --pixel rate: 50 MHz
  --horizontal display region: 800 pixels
  --horizontal right border: 64 pixels
  --horizontal left border: 56 pixels
  --horizontal retrace: 120 pixels
  --vertical display region: 600 lines
  --vertical bottom border: 23 lines
  --vertical top border: 37 lines
  --a vertical retrace: 6 lines

  --parametros para VGA de 640*480--
  constant HD: integer  :=  640; --pixels horizontales
  constant HF: integer  :=   16; --front porch, borde derecho
  constant HB: integer  :=   48; --back porch, borde izquierdo
  constant HR: integer  :=   96; --retraso
  
  constant VD: integer  :=  480; --pixels verticales
  constant VF: integer  :=   10; --front porch, borde inferior
  constant VB: integer  :=   33; --back porch, borde superior
  constant VR: integer  :=    2; --retraso

  --contador de modulo 2--
  signal mod2_reg     : std_logic;
  signal mod2_next    : std_logic;

  --contadores de sincronismo de modulos 800 y 525
  signal h_count_reg  : unsigned(9 downto 0);
  signal h_count_next : unsigned(9 downto 0);
  signal v_count_reg  : unsigned(9 downto 0);
  signal v_count_next : unsigned(9 downto 0);
  --buffers de salida
  signal v_sync_reg   : std_logic;
  signal v_sync_next  : std_logic;
  signal h_sync_reg   : std_logic;
  signal h_sync_next  : std_logic;
  --status
  signal h_end        : std_logic;
  signal v_end        : std_logic;
  signal pixel_tick   : std_logic;

--*********************************--

begin
registros : process( clk,rst )
	begin
		if rst='1' then
			mod2_reg<='0';
			v_count_reg<=(others=>'0');
			h_count_reg<=(others=>'0');
			v_sync_reg<='0';
			h_sync_reg<='0';
		elsif rising_edge(clk) then
			mod2_reg<=mod2_next;
			v_count_reg<=v_count_next;
			h_count_reg<=h_count_next;
			v_sync_reg<=v_sync_next;
			h_sync_reg<=h_sync_next;			
		end if ;
end process ; -- registros

--*********************************--

--contador de modulo 2 genera 25MHz a partir de 50MHz
mod2_next<= not mod2_reg;--enable tick
pixel_tick<='1' when mod2_reg='1' else '0';--pixel tick
--fin de contadores horizontal y vertical
h_end<='1' when h_count_reg=(HD+HF+HB+HR-1) else
	   '0';	--marca 800 ticks
v_end<='1' when v_count_reg=(VD+VF+VB+VR-1) else
	   '0'; --marca 525 ticks

--*********************************--

--contador horizontal de modulo 800
mod800 : process( h_count_reg,h_end,pixel_tick )
	begin
		if (pixel_tick = '1') then--25MHztick
			if (h_end = '1') then
				h_count_next<=(others=>'0');
			else
				h_count_next<=h_count_reg+1;
			end if;
		else
			h_count_next<=h_count_reg;
		end if;
end process ; -- mod800

--contador vertical de modulo 525
--se cuenta una linea por cada 800 pixels horizontales
mod525 : process( v_count_reg,v_end,h_end,pixel_tick )
	begin
		if pixel_tick='1' and h_end='1' then
			if v_end='1' then
				v_count_next<=(others=>'0');
			else
				v_count_next<=v_count_reg+1;
			end if;
		else
			v_count_next<=v_count_reg;
		end if;
end process ; -- mod525

--*********************************--

--buffers
h_sync_next<= '1' when (h_count_reg >=(HD+HF))
				  and  (h_count_reg<=(HD+HF+HR-1)) else
			  '0';
v_sync_next<= '1' when (v_count_reg >=(VD+VF))
				  and  (v_count_reg<=(VD+VF+VR-1)) else
			  '0';
-- video on/off
video_on <= '1' when (h_count_reg<HD) and (v_count_reg<VD) else
	 		   '0';

--*********************************--

--salidas
hsync<=h_sync_reg;
vsync<=v_sync_reg;
pixel_x<=std_logic_vector(h_count_reg);
pixel_y<=std_logic_vector(v_count_reg);
p_tick<=pixel_tick;

end arch;

