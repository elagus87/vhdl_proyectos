library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fsm_sr is
   generic(
      RAM_ADDR_WIDTH: natural :=   8;
      RAM_DO_WIDTH  : natural := 256;
      ROM_ADDR_WIDTH: natural :=   8;
      ROM_DO_WIDTH  : natural := 256
   );
   port(
      clk      : in   std_logic;
      rst      : in   std_logic;
      en       : in   std_logic;
      pixel_x  : in   std_logic_vector(               9 downto 0);
      pixel_y  : in   std_logic_vector(               9 downto 0);
      video_on : in   std_logic;
      do_rom_a : in   std_logic_vector(  ROM_DO_WIDTH-1 downto 0);
      do_rom   : in   std_logic_vector(  ROM_DO_WIDTH-1 downto 0);
      addr_out : out  std_logic_vector(ROM_ADDR_WIDTH-1 downto 0);
      graph_rgb: out  std_logic_vector(               2 downto 0)
   );
end fsm_sr;

architecture Behavioral of fsm_sr is
   
   type   state_type is (
      ST_IDLE     ,
      ST_READ_RAM ,
      ST_READ_ROM ,
      ST_SHIFT_REG
   );
   
   signal   state_reg      : state_type;
   signal   u_pixel_y      : unsigned(9 downto 0);
   signal   u_pixel_x      : unsigned(9 downto 0);
   signal   u_addr_fixed   : unsigned(9 downto 0);
   signal   addr_fixed     : std_logic_vector(7 downto 0);
   signal   shift_reg      : std_logic_vector(ROM_DO_WIDTH-1 downto 0);
   signal   shift_count    : unsigned(ROM_ADDR_WIDTH-1 downto 0);
   signal   clk_on         : std_logic;
   signal   sr_lsb         : std_logic;
   signal   center_clk_on  : std_logic;

   constant ADDR_OFFSET    : unsigned(9 downto 0)  := to_unsigned(112,10);
   constant MAX_X          : unsigned(9 downto 0)  := to_unsigned(640,10);
   constant MAX_Y          : unsigned(9 downto 0)  := to_unsigned(480,10);
   constant CLK_SIZE       : unsigned(9 downto 0)  := to_unsigned(256,10);
   constant CLK_WORD       : unsigned(9 downto 0)  := to_unsigned(  8,10);                -- log2(CLK_SIZE)
   constant CLK_X_L        : unsigned(9 downto 0)  := to_unsigned(320,10) - (CLK_SIZE/2); -- limites laterales
   constant CLK_X_R        : unsigned(9 downto 0)  := CLK_X_L + CLK_SIZE - 1;             -- limites laterales
   constant CLK_Y_T        : unsigned(9 downto 0)  := to_unsigned(240,10) - (CLK_SIZE/2); -- limites verticales
   constant CLK_Y_B        : unsigned(9 downto 0)  := CLK_Y_T + CLK_SIZE - 1;             -- limites verticales

begin

   u_pixel_y      <= unsigned(pixel_y);
   u_pixel_x      <= unsigned(pixel_x);
   u_addr_fixed   <= u_pixel_y - ADDR_OFFSET;
   addr_fixed     <= std_logic_vector(u_addr_fixed(7 downto 0));

   fsm : process( clk , rst ) begin
      if (rst = '1') then
         state_reg   <= ST_IDLE;
         addr_out    <= (others => '0');
         shift_reg   <= (others => '0');
         shift_count <= (others => '0');
      elsif rising_edge(clk) then
         if (en = '1') then
            case (state_reg) is
               when ST_IDLE      =>
                  if ((u_pixel_y >= CLK_Y_T) and (u_pixel_y <= CLK_Y_B)) then
                     state_reg <= ST_READ_RAM;
                     addr_out  <= addr_fixed;
                  end if;
               when ST_READ_RAM  =>
                  state_reg <= ST_READ_ROM;
                  shift_reg <= do_rom_a;
               when ST_READ_ROM  =>
                  state_reg   <= ST_SHIFT_REG;
                  shift_reg   <= (shift_reg or do_rom);
                  shift_count <= (others => '0');
               when ST_SHIFT_REG =>
                   if ((u_pixel_x >= CLK_X_L) and (u_pixel_x <= CLK_X_R)) then
                     shift_reg   <= '0' & shift_reg(255 downto 1);
							--shift_reg   <= shift_reg(254 downto 0) & '0';
                     shift_count <= shift_count + 1;
                  else
                     state_reg <= ST_IDLE;
                  end if;
            end case;
         end if;
      end if;
   end process;

	sr_lsb <= shift_reg(0);
   --sr_lsb <= shift_reg(255);

   center_clk_on <=
      '1' when (CLK_X_L <= u_pixel_x) and (u_pixel_x <= CLK_X_R) and
               (CLK_Y_T <= u_pixel_y) and (u_pixel_y <= CLK_Y_B) else
      '0';

   clk_on <=
      '1' when (center_clk_on = '1') and (sr_lsb = '1') else
      '0';

   graph_rgb <=
      "000" when video_on = '0' else -- negro
      "101" when clk_on   = '1' else -- rojo
      "110";                         -- amarillo

end Behavioral;
