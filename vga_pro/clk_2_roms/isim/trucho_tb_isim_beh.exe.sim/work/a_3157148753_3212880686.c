/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/agus/UTN/vhdl_proyectos/vga_pro/clk_2_roms/fsm_sr.vhd";
extern char *IEEE_P_1242562249;
extern char *IEEE_P_2592010699;

char *ieee_p_1242562249_sub_1006216973935652998_1035706684(char *, char *, char *, char *, int );
unsigned char ieee_p_1242562249_sub_1434220770695818471_1035706684(char *, char *, char *, char *, char *);
unsigned char ieee_p_1242562249_sub_1434220770698190313_1035706684(char *, char *, char *, char *, char *);
char *ieee_p_1242562249_sub_1701011461141789389_1035706684(char *, char *, char *, char *, char *, char *);
char *ieee_p_2592010699_sub_16439767405979520975_503743352(char *, char *, char *, char *, char *, char *);
unsigned char ieee_p_2592010699_sub_2763492388968962707_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_3157148753_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(58, ng0);

LAB3:    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t1 = (t0 + 9296);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 10U);
    xsi_driver_first_trans_fast(t1);

LAB2:    t7 = (t0 + 9088);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3157148753_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(59, ng0);

LAB3:    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t1 = (t0 + 9360);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 10U);
    xsi_driver_first_trans_fast(t1);

LAB2:    t7 = (t0 + 9104);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3157148753_3212880686_p_2(char *t0)
{
    char t1[16];
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;

LAB0:    xsi_set_current_line(60, ng0);

LAB3:    t2 = (t0 + 2792U);
    t3 = *((char **)t2);
    t2 = (t0 + 14568U);
    t4 = (t0 + 4848U);
    t5 = *((char **)t4);
    t4 = (t0 + 14664U);
    t6 = ieee_p_1242562249_sub_1701011461141789389_1035706684(IEEE_P_1242562249, t1, t3, t2, t5, t4);
    t7 = (t1 + 12U);
    t8 = *((unsigned int *)t7);
    t9 = (1U * t8);
    t10 = (10U != t9);
    if (t10 == 1)
        goto LAB5;

LAB6:    t11 = (t0 + 9424);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t6, 10U);
    xsi_driver_first_trans_fast(t11);

LAB2:    t16 = (t0 + 9120);
    *((int *)t16) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(10U, t9, 0);
    goto LAB6;

}

static void work_a_3157148753_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    unsigned int t3;
    unsigned int t4;
    unsigned int t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;

LAB0:    xsi_set_current_line(61, ng0);

LAB3:    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t3 = (9 - 7);
    t4 = (t3 * 1U);
    t5 = (0 + t4);
    t1 = (t2 + t5);
    t6 = (t0 + 9488);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t6);

LAB2:    t11 = (t0 + 9136);
    *((int *)t11) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3157148753_3212880686_p_4(char *t0)
{
    char t25[16];
    char t29[16];
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    unsigned char t10;
    unsigned char t11;
    unsigned char t12;
    char *t13;
    unsigned char t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    int t30;
    unsigned int t31;
    char *t32;
    static char *nl0[] = {&&LAB11, &&LAB12, &&LAB13, &&LAB14};

LAB0:    xsi_set_current_line(64, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 992U);
    t3 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB5;

LAB6:
LAB3:    t1 = (t0 + 9152);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(65, ng0);
    t1 = (t0 + 9552);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(66, ng0);
    t1 = xsi_get_transient_memory(8U);
    memset(t1, 0, 8U);
    t2 = t1;
    memset(t2, (unsigned char)2, 8U);
    t5 = (t0 + 9616);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 8U);
    xsi_driver_first_trans_fast_port(t5);
    xsi_set_current_line(67, ng0);
    t1 = xsi_get_transient_memory(256U);
    memset(t1, 0, 256U);
    t2 = t1;
    memset(t2, (unsigned char)2, 256U);
    t5 = (t0 + 9680);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 256U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(68, ng0);
    t1 = xsi_get_transient_memory(8U);
    memset(t1, 0, 8U);
    t2 = t1;
    memset(t2, (unsigned char)2, 8U);
    t5 = (t0 + 9744);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 8U);
    xsi_driver_first_trans_fast(t5);
    goto LAB3;

LAB5:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 1352U);
    t5 = *((char **)t2);
    t4 = *((unsigned char *)t5);
    t10 = (t4 == (unsigned char)3);
    if (t10 != 0)
        goto LAB7;

LAB9:
LAB8:    goto LAB3;

LAB7:    xsi_set_current_line(71, ng0);
    t2 = (t0 + 2632U);
    t6 = *((char **)t2);
    t11 = *((unsigned char *)t6);
    t2 = (char *)((nl0) + t11);
    goto **((char **)t2);

LAB10:    goto LAB8;

LAB11:    xsi_set_current_line(73, ng0);
    t7 = (t0 + 2792U);
    t8 = *((char **)t7);
    t7 = (t0 + 14568U);
    t9 = (t0 + 5688U);
    t13 = *((char **)t9);
    t9 = (t0 + 14776U);
    t14 = ieee_p_1242562249_sub_1434220770698190313_1035706684(IEEE_P_1242562249, t8, t7, t13, t9);
    if (t14 == 1)
        goto LAB18;

LAB19:    t12 = (unsigned char)0;

LAB20:    if (t12 != 0)
        goto LAB15;

LAB17:
LAB16:    goto LAB10;

LAB12:    xsi_set_current_line(78, ng0);
    t1 = (t0 + 9552);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(79, ng0);
    t1 = (t0 + 1992U);
    t2 = *((char **)t1);
    t1 = (t0 + 9680);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 256U);
    xsi_driver_first_trans_fast(t1);
    goto LAB10;

LAB13:    xsi_set_current_line(81, ng0);
    t1 = (t0 + 9552);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(82, ng0);
    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 14632U);
    t5 = (t0 + 2152U);
    t6 = *((char **)t5);
    t5 = (t0 + 14520U);
    t7 = ieee_p_2592010699_sub_16439767405979520975_503743352(IEEE_P_2592010699, t25, t2, t1, t6, t5);
    t8 = (t25 + 12U);
    t26 = *((unsigned int *)t8);
    t27 = (1U * t26);
    t3 = (256U != t27);
    if (t3 == 1)
        goto LAB21;

LAB22:    t9 = (t0 + 9680);
    t13 = (t9 + 56U);
    t15 = *((char **)t13);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t7, 256U);
    xsi_driver_first_trans_fast(t9);
    xsi_set_current_line(83, ng0);
    t1 = xsi_get_transient_memory(8U);
    memset(t1, 0, 8U);
    t2 = t1;
    memset(t2, (unsigned char)2, 8U);
    t5 = (t0 + 9744);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memcpy(t9, t1, 8U);
    xsi_driver_first_trans_fast(t5);
    goto LAB10;

LAB14:    xsi_set_current_line(85, ng0);
    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 14584U);
    t5 = (t0 + 5448U);
    t6 = *((char **)t5);
    t5 = (t0 + 14744U);
    t4 = ieee_p_1242562249_sub_1434220770698190313_1035706684(IEEE_P_1242562249, t2, t1, t6, t5);
    if (t4 == 1)
        goto LAB26;

LAB27:    t3 = (unsigned char)0;

LAB28:    if (t3 != 0)
        goto LAB23;

LAB25:    xsi_set_current_line(90, ng0);
    t1 = (t0 + 9552);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);

LAB24:    goto LAB10;

LAB15:    xsi_set_current_line(74, ng0);
    t20 = (t0 + 9552);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = (unsigned char)1;
    xsi_driver_first_trans_fast(t20);
    xsi_set_current_line(75, ng0);
    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t1 = (t0 + 9616);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 8U);
    xsi_driver_first_trans_fast_port(t1);
    goto LAB16;

LAB18:    t15 = (t0 + 2792U);
    t16 = *((char **)t15);
    t15 = (t0 + 14568U);
    t17 = (t0 + 5808U);
    t18 = *((char **)t17);
    t17 = (t0 + 14792U);
    t19 = ieee_p_1242562249_sub_1434220770695818471_1035706684(IEEE_P_1242562249, t16, t15, t18, t17);
    t12 = t19;
    goto LAB20;

LAB21:    xsi_size_not_matching(256U, t27, 0);
    goto LAB22;

LAB23:    xsi_set_current_line(86, ng0);
    t15 = (t0 + 3432U);
    t16 = *((char **)t15);
    t26 = (255 - 255);
    t27 = (t26 * 1U);
    t28 = (0 + t27);
    t15 = (t16 + t28);
    t18 = ((IEEE_P_2592010699) + 4000);
    t20 = (t29 + 0U);
    t21 = (t20 + 0U);
    *((int *)t21) = 255;
    t21 = (t20 + 4U);
    *((int *)t21) = 1;
    t21 = (t20 + 8U);
    *((int *)t21) = -1;
    t30 = (1 - 255);
    t31 = (t30 * -1);
    t31 = (t31 + 1);
    t21 = (t20 + 12U);
    *((unsigned int *)t21) = t31;
    t17 = xsi_base_array_concat(t17, t25, t18, (char)99, (unsigned char)2, (char)97, t15, t29, (char)101);
    t31 = (1U + 255U);
    t11 = (256U != t31);
    if (t11 == 1)
        goto LAB29;

LAB30:    t21 = (t0 + 9680);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    t24 = (t23 + 56U);
    t32 = *((char **)t24);
    memcpy(t32, t17, 256U);
    xsi_driver_first_trans_fast(t21);
    xsi_set_current_line(88, ng0);
    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 14648U);
    t5 = ieee_p_1242562249_sub_1006216973935652998_1035706684(IEEE_P_1242562249, t25, t2, t1, 1);
    t6 = (t25 + 12U);
    t26 = *((unsigned int *)t6);
    t27 = (1U * t26);
    t3 = (8U != t27);
    if (t3 == 1)
        goto LAB31;

LAB32:    t7 = (t0 + 9744);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    t13 = (t9 + 56U);
    t15 = *((char **)t13);
    memcpy(t15, t5, 8U);
    xsi_driver_first_trans_fast(t7);
    goto LAB24;

LAB26:    t7 = (t0 + 2952U);
    t8 = *((char **)t7);
    t7 = (t0 + 14584U);
    t9 = (t0 + 5568U);
    t13 = *((char **)t9);
    t9 = (t0 + 14760U);
    t10 = ieee_p_1242562249_sub_1434220770695818471_1035706684(IEEE_P_1242562249, t8, t7, t13, t9);
    t3 = t10;
    goto LAB28;

LAB29:    xsi_size_not_matching(256U, t31, 0);
    goto LAB30;

LAB31:    xsi_size_not_matching(8U, t27, 0);
    goto LAB32;

}

static void work_a_3157148753_3212880686_p_5(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;

LAB0:    xsi_set_current_line(97, ng0);

LAB3:    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t3 = (0 - 255);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 9808);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = t7;
    xsi_driver_first_trans_fast(t8);

LAB2:    t13 = (t0 + 9168);
    *((int *)t13) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3157148753_3212880686_p_6(char *t0)
{
    unsigned char t1;
    unsigned char t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned char t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    unsigned char t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    unsigned char t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;

LAB0:    xsi_set_current_line(100, ng0);
    t4 = (t0 + 5448U);
    t5 = *((char **)t4);
    t4 = (t0 + 14744U);
    t6 = (t0 + 2952U);
    t7 = *((char **)t6);
    t6 = (t0 + 14584U);
    t8 = ieee_p_1242562249_sub_1434220770695818471_1035706684(IEEE_P_1242562249, t5, t4, t7, t6);
    if (t8 == 1)
        goto LAB11;

LAB12:    t3 = (unsigned char)0;

LAB13:    if (t3 == 1)
        goto LAB8;

LAB9:    t2 = (unsigned char)0;

LAB10:    if (t2 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB14:    t29 = (t0 + 9872);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    t32 = (t31 + 56U);
    t33 = *((char **)t32);
    *((unsigned char *)t33) = (unsigned char)2;
    xsi_driver_first_trans_fast(t29);

LAB2:    t34 = (t0 + 9184);
    *((int *)t34) = 1;

LAB1:    return;
LAB3:    t24 = (t0 + 9872);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    t27 = (t26 + 56U);
    t28 = *((char **)t27);
    *((unsigned char *)t28) = (unsigned char)3;
    xsi_driver_first_trans_fast(t24);
    goto LAB2;

LAB5:    t19 = (t0 + 2792U);
    t20 = *((char **)t19);
    t19 = (t0 + 14568U);
    t21 = (t0 + 5808U);
    t22 = *((char **)t21);
    t21 = (t0 + 14792U);
    t23 = ieee_p_1242562249_sub_1434220770695818471_1035706684(IEEE_P_1242562249, t20, t19, t22, t21);
    t1 = t23;
    goto LAB7;

LAB8:    t14 = (t0 + 5688U);
    t15 = *((char **)t14);
    t14 = (t0 + 14776U);
    t16 = (t0 + 2792U);
    t17 = *((char **)t16);
    t16 = (t0 + 14568U);
    t18 = ieee_p_1242562249_sub_1434220770695818471_1035706684(IEEE_P_1242562249, t15, t14, t17, t16);
    t2 = t18;
    goto LAB10;

LAB11:    t9 = (t0 + 2952U);
    t10 = *((char **)t9);
    t9 = (t0 + 14584U);
    t11 = (t0 + 5568U);
    t12 = *((char **)t11);
    t11 = (t0 + 14760U);
    t13 = ieee_p_1242562249_sub_1434220770695818471_1035706684(IEEE_P_1242562249, t10, t9, t12, t11);
    t3 = t13;
    goto LAB13;

LAB15:    goto LAB2;

}

static void work_a_3157148753_3212880686_p_7(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    unsigned char t7;
    unsigned char t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(105, ng0);
    t2 = (t0 + 4072U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB3;

LAB4:
LAB8:    t13 = (t0 + 9936);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = (unsigned char)2;
    xsi_driver_first_trans_fast(t13);

LAB2:    t18 = (t0 + 9200);
    *((int *)t18) = 1;

LAB1:    return;
LAB3:    t2 = (t0 + 9936);
    t9 = (t2 + 56U);
    t10 = *((char **)t9);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    goto LAB2;

LAB5:    t2 = (t0 + 3912U);
    t6 = *((char **)t2);
    t7 = *((unsigned char *)t6);
    t8 = (t7 == (unsigned char)3);
    t1 = t8;
    goto LAB7;

LAB9:    goto LAB2;

}

static void work_a_3157148753_3212880686_p_8(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    unsigned char t13;
    unsigned char t14;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;

LAB0:    xsi_set_current_line(109, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)2);
    if (t4 != 0)
        goto LAB3;

LAB4:    t11 = (t0 + 3752U);
    t12 = *((char **)t11);
    t13 = *((unsigned char *)t12);
    t14 = (t13 == (unsigned char)3);
    if (t14 != 0)
        goto LAB5;

LAB6:
LAB7:    t21 = (t0 + 15762);
    t23 = (t0 + 10000);
    t24 = (t23 + 56U);
    t25 = *((char **)t24);
    t26 = (t25 + 56U);
    t27 = *((char **)t26);
    memcpy(t27, t21, 3U);
    xsi_driver_first_trans_fast_port(t23);

LAB2:    t28 = (t0 + 9216);
    *((int *)t28) = 1;

LAB1:    return;
LAB3:    t1 = (t0 + 15756);
    t6 = (t0 + 10000);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 3U);
    xsi_driver_first_trans_fast_port(t6);
    goto LAB2;

LAB5:    t11 = (t0 + 15759);
    t16 = (t0 + 10000);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    memcpy(t20, t11, 3U);
    xsi_driver_first_trans_fast_port(t16);
    goto LAB2;

LAB8:    goto LAB2;

}


extern void work_a_3157148753_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3157148753_3212880686_p_0,(void *)work_a_3157148753_3212880686_p_1,(void *)work_a_3157148753_3212880686_p_2,(void *)work_a_3157148753_3212880686_p_3,(void *)work_a_3157148753_3212880686_p_4,(void *)work_a_3157148753_3212880686_p_5,(void *)work_a_3157148753_3212880686_p_6,(void *)work_a_3157148753_3212880686_p_7,(void *)work_a_3157148753_3212880686_p_8};
	xsi_register_didat("work_a_3157148753_3212880686", "isim/trucho_tb_isim_beh.exe.sim/work/a_3157148753_3212880686.didat");
	xsi_register_executes(pe);
}
