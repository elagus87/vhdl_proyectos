
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity rams_04 is
   generic(
      RAM_ADDR_WIDTH: natural :=   8;
      RAM_DI_WIDTH  : natural := 256;
      RAM_DO_WIDTH  : natural := 256
   );
   port(
      CLK  : in   std_logic;
      WE   : in   std_logic;
      EN   : in   std_logic;
      ADDR : in   std_logic_vector(RAM_ADDR_WIDTH-1 downto 0);
      DI   : in   std_logic_vector(  RAM_DI_WIDTH-1 downto 0);
      DO   : out  std_logic_vector(  RAM_DO_WIDTH-1 downto 0)
   );
end rams_04;

architecture syn of rams_04 is

   type ram_type is array (2**RAM_ADDR_WIDTH-1 downto 0) of std_logic_vector (RAM_DI_WIDTH-1 downto 0);
   signal RAM: ram_type;
    
begin

   process (clk) begin
      if CLK'event and CLK = '1' then
         if EN = '1' then
            if WE = '1' then
               RAM(conv_integer(ADDR)) <= DI;
            end if;
            DO <= RAM(conv_integer(ADDR)) ;
         end if;
      end if;
   end process;

end syn;


					
