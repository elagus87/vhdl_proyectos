library ieee;
use ieee.std_logic_1164.all;

--##################################--

entity pong_top_ani is
   port (
      clk,rst: 		in std_logic;
		A   : 			in    std_logic;
      B   : 			in    std_logic;
      hsync, vsync: 	out  std_logic;
      rgb: 				out std_logic_vector(2 downto 0)
   );
end pong_top_ani;

--##################################--


architecture arch of pong_top_ani is
   signal pixel_x, pixel_y: std_logic_vector (9 downto 0);
   signal video_on, pixel_tick: std_logic;
   signal rgb_reg, rgb_next: std_logic_vector(2 downto 0);
	signal btn: std_logic_vector(1 downto 0);
	
--*********************************--

begin
	-- instancia del encoder
	cotrol: entity work.encoder
		port map(CLK=>clk, 
               A=>A,
               B=>B, 
               FWD=>btn(0),
               REV=>btn(1)
					);
	
   -- instancia de  VGA sync
   vga_sync_unit: entity work.vga_sync
      port map(clk=>clk, 
               rst=>rst,
               video_on=>video_on, 
               p_tick=>pixel_tick,
               hsync=>hsync, 
               vsync=>vsync,
               pixel_x=>pixel_x, 
               pixel_y=>pixel_y);
   -- instancia del generador grafico
   pong_graph_an_unit: entity work.pong_graph_animate
      port map (clk=>clk, 
					 rst=>rst,
                btn=>btn, 
					 video_on=>video_on,
                pixel_x=>pixel_x, 
					 pixel_y=>pixel_y,
                graph_rgb=>rgb_next);

--*********************************--

   rgb_buffer:process (clk)
   begin
      if rising_edge(clk) then
         if (pixel_tick='1') then
            rgb_reg <= rgb_next;
         end if;
      end if;
   end process;

--*********************************--

   rgb <= rgb_reg;
end arch;