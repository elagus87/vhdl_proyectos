# VGA para spartan 3e

[![S3E](https://www.xilinx.com/content/dam/xilinx/imgs/prime/S3E-top-1000.png)](https://www.xilinx.com/products/boards-and-kits/1-elhacw.html)
___

## calculos de Timing para señales de sincronización 
![hor](https://bitbucket.org/elagus87/vhdl_proyectos/raw/1bb85dc9f22a0652e2b32ecdb2aa8c7cb1656f7a/s3e/vga_pro/imagenes/horizontal_scan.PNG)
![ver](https://bitbucket.org/elagus87/vhdl_proyectos/raw/1bb85dc9f22a0652e2b32ecdb2aa8c7cb1656f7a/s3e/vga_pro/imagenes/vertical_scan.PNG)

Se definen:

- $$P=800\cdot\frac{pixel}{line}$$
- $$l=525\cdot\frac{lineas}{screen}$$
- $$s=60\cdot\frac{screen}{second}$$
 
Entonces:
- $$pixel\_rate=p\cdot l\cdot s\approx 25M\cdot \frac{pixel}{second}$$

___

## Esquema de conexiones
![con](https://bitbucket.org/elagus87/vhdl_proyectos/raw/1bb85dc9f22a0652e2b32ecdb2aa8c7cb1656f7a/s3e/vga_pro/imagenes/conexion.PNG)

___

## Primer ejemplo

Se busca lograr una imagen estatica del clasico juego

![static](https://bitbucket.org/elagus87/vhdl_proyectos/raw/1544d3533f6ae9160574f03c9bef153dc88c91c2/s3e/vga_pro/ej1.PNG)