----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:38:42 09/13/2019 
-- Design Name: 
-- Module Name:    coordinador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--##################################--

entity coordinador is
    Port ( xh : in  STD_LOGIC_VECTOR (10 downto 0);
           yh : in  STD_LOGIC_VECTOR (10  downto 0);
           xm : in  STD_LOGIC_VECTOR (10 downto 0);
           ym : in  STD_LOGIC_VECTOR (10  downto 0);
           xs : in  STD_LOGIC_VECTOR (10 downto 0);
           ys : in  STD_LOGIC_VECTOR (10  downto 0);
           h  : in  STD_LOGIC;
           m  : in  STD_LOGIC;
           s  : in  STD_LOGIC;
           xo : out  STD_LOGIC_VECTOR (10 downto 0);
           yo : out  STD_LOGIC_VECTOR (10 downto 0));
end coordinador;

--##################################--

architecture Behavioral of coordinador is

begin

--*********************************--

	xo <= xs when s='1' else
		  xm when m='1' else
		  xh when h='1' else
		  (others => '0');

	yo <= ys when s='1' else
		  ym when m='1' else
		  yh when h='1' else
		  (others => '0');
--*********************************--


end Behavioral;

