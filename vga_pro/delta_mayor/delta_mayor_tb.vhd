--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:59:33 08/19/2019
-- Design Name:   
-- Module Name:   C:/Users/elagu/OneDrive/Documentos/vhdl_proyectos/vga_pro/delta_mayor/delta_mayor_tb.vhd
-- Project Name:  delta_mayor
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: delta_mayor
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY delta_mayor_tb IS
END delta_mayor_tb;
 
ARCHITECTURE behavior OF delta_mayor_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT delta_mayor
    generic(W:natural:=11);
    PORT(
         dx : IN  std_logic_vector(W-1 downto 0);
         dy : IN  std_logic_vector(W-1 downto 0);
         Inc_Yi : IN  std_logic_vector(1 downto 0);
         Inc_Xi : IN  std_logic_vector(1 downto 0);
         start : IN  std_logic;
         clk : IN  std_logic;
         rst : IN  std_logic;
         Inc_Yr : OUT  std_logic_vector(1 downto 0);
         Inc_Xr : OUT  std_logic_vector(1 downto 0);
         dMayor : OUT  std_logic_vector(W-1 downto 0);
         dMenor : OUT  std_logic_vector(W-1 downto 0)
        );
    END COMPONENT;
   constant Tclk : time := 10 ns;
   constant Wt : natural:= 11; 

   --Inputs
   signal dx : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal dy : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal Inc_Yi : std_logic_vector(1 downto 0) := (others => '0');
   signal Inc_Xi : std_logic_vector(1 downto 0) := (others => '0');
   signal start : std_logic := '0';
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';

 	--Outputs
   signal Inc_Yr : std_logic_vector(1 downto 0);
   signal Inc_Xr : std_logic_vector(1 downto 0);
   signal dMayor : std_logic_vector(Wt-1 downto 0);
   signal dMenor : std_logic_vector(Wt-1 downto 0);

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: delta_mayor PORT MAP (
          dx => dx,
          dy => dy,
          Inc_Yi => Inc_Yi,
          Inc_Xi => Inc_Xi,
          start => start,
          clk => clk,
          rst => rst,
          Inc_Yr => Inc_Yr,
          Inc_Xr => Inc_Xr,
          dMayor => dMayor,
          dMenor => dMenor
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for Tclk/2;
		clk <= '1';
		wait for Tclk/2;
   end process;
 

      start_proc : process
   begin
      wait for Tclk*2;--2
      wait for Tclk*1;--3
      start<='1';
      wait for Tclk*1;--4
      start<='0';
      wait for Tclk*3;--7
      start<='1';
      wait for Tclk*1;--8
      start<='0';
      wait for Tclk*3;--11
      start<='1';
      wait for Tclk*1;--12
      start<='0';
      wait for Tclk*3;--15
      start<='1';
      wait for Tclk*1;--16
      start<='0';
      wait for Tclk*3;--19

   end process ; -- start_proc
   -- Stimulus process
   stim_proc: process
   begin    
      rst<='1';
      Inc_Xi<="01";
      Inc_Yi<="11";
      wait for Tclk*2;--2 
      rst<='0';
      --horizontal positiva
      dx<=std_logic_vector(TO_SIGNED(0,Wt));
      dy<=std_logic_vector(TO_SIGNED(450,Wt));
      wait for Tclk*4;--6
      --vertical positiva
      dx<=std_logic_vector(TO_SIGNED(450,Wt));
      dy<=std_logic_vector(TO_SIGNED(0,Wt));
      wait for Tclk*4;--10
      dx<=std_logic_vector(TO_SIGNED(225,Wt));
      dy<=std_logic_vector(TO_SIGNED(0,Wt));
      wait for Tclk*4;--14
      dx<=std_logic_vector(TO_SIGNED(0,Wt));
      dy<=std_logic_vector(TO_SIGNED(225,Wt));
      wait for Tclk*4;--18
  
      -- insert stimulus here
      assert false
      report "fin de la simulacion" 
      severity failure;
   end process;

END;
