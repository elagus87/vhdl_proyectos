LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY pre_top_tb IS
END pre_top_tb;
 
ARCHITECTURE behavior OF pre_top_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT pre_top_av
    generic(W:natural:=11);
    PORT(
         x1 : IN  std_logic_vector(W-1 downto 0);
         x2 : IN  std_logic_vector(W-1 downto 0);
         y1 : IN  std_logic_vector(W-1 downto 0);
         y2 : IN  std_logic_vector(W-1 downto 0);
         start : IN  std_logic;
         rst : IN  std_logic;
         clk : IN  std_logic;
         wait_sig : IN  std_logic;
         av : OUT  std_logic_vector(W+1 downto 0);
         incXi : OUT  std_logic_vector(1 downto 0);
         incYi : OUT  std_logic_vector(1 downto 0);
         incXr : OUT  std_logic_vector(1 downto 0);
         incYr : OUT  std_logic_vector(1 downto 0)
        );
    END COMPONENT;

   constant Tclk : time := 10 ns;
   constant Wt : natural:= 11;  

   --Inputs
   signal x1 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal x2 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal y1 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal y2 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal start : std_logic := '0';
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';
   signal wait_sig : std_logic := '0';

 	--Outputs
   signal av : std_logic_vector(Wt+1 downto 0);
   signal incXi : std_logic_vector(1 downto 0);
   signal incYi : std_logic_vector(1 downto 0);
   signal incXr : std_logic_vector(1 downto 0);
   signal incYr : std_logic_vector(1 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: pre_top_av PORT MAP (
          x1 => x1,
          x2 => x2,
          y1 => y1,
          y2 => y2,
          start => start,
          rst => rst,
          clk => clk,
          wait_sig => wait_sig,
          av => av,
          incXi => incXi,
          incYi => incYi,
          incXr => incXr,
          incYr => incYr
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for Tclk/2;
		clk <= '0';
		wait for Tclk/2;
   end process;

   start_proc : process
   begin
      wait for Tclk*3;--3
      start<='1';
      wait for Tclk;--4
      start<='0';
      wait for Tclk*15;--20
      start<='1';
      wait for Tclk;--21
      start<='0';
      wait for Tclk*14;--35
   end process ; -- start_proc
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst     <= '1';
      wait_sig<= '1';
      wait for Tclk*2;--2 
      rst<='0';

      x1      <= (others =>'0');
      y1      <= (others =>'0');
      x2      <= std_logic_vector(TO_UNSIGNED(10,Wt));
      y2      <= std_logic_vector(TO_UNSIGNED(3,Wt));

      wait for Tclk;--3
      wait_sig<='0'; 
      wait for Tclk;--4
      wait for Tclk*13;--17
      wait_sig<='1';
      wait for Tclk;--18

      x1      <= (others =>'0');
      y1      <= (others =>'0');
      x2      <= std_logic_vector(TO_UNSIGNED(10,Wt));
      y2      <= std_logic_vector(TO_UNSIGNED(10,Wt));

      wait for Tclk;--19
      wait_sig<='0'; 
      wait for Tclk;--20
      wait for Tclk*14;--34
      wait_sig<='1';
      wait for Tclk;--35
      assert false
      report "fin de la simulacion" 
      severity failure;
   end process;

END;
