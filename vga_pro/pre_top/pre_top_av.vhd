library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pre_top_av is
	generic(W:natural:=11);
    Port ( x1 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           x2 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           y1 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           y2 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           start : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           wait_sig : in  STD_LOGIC;
           av : out  STD_LOGIC_VECTOR (W+1 downto 0);
    	   incXi : out STD_LOGIC_VECTOR(1 downto 0);
    	   incYi : out STD_LOGIC_VECTOR(1 downto 0);
    	   incXr : out STD_LOGIC_VECTOR(1 downto 0);
    	   incYr : out STD_LOGIC_VECTOR(1 downto 0));
end pre_top_av;

architecture Behavioral of pre_top_av is
	signal dx,dy : STD_LOGIC_VECTOR(W-1 downto 0);
	signal dMayor,dMenor : STD_LOGIC_VECTOR(W-1 downto 0);
	signal inc_Xi,inc_Yi,inc_Xr,inc_Yr:STD_LOGIC_VECTOR(1 downto 0);
	signal start_reg1,start_reg2,start_next1,start_next2:STD_LOGIC;

begin


	registros : process( clk,rst )
	begin
		if rst='1' then
			start_reg1 <= '0';
			start_reg2 <= '0';
		elsif rising_edge(clk) then
			start_reg1 <= start_next1;
			start_reg2 <= start_next2;
		end if ;
	end process ; -- registros

	start_next1 <= start;
	start_next2 <= start_reg1;

   dx_unit: entity work.delta
      port map(c0=>x1, 
      		   c1=>x2,
               rst=>rst,                
               start=>start_next1,
               clk=>clk,
               d=>dx,
               inc=>inc_Xi);

   dy_unit: entity work.delta
      port map(c0=>y1, 
      		   c1=>y2,
               rst=>rst,                
               start=>start_next1,
               clk=>clk,
               d=>dy,
               inc=>inc_Yi);


   delta_may: entity work.delta_mayor
      port map(dx=>dx, 
      		   dy=>dy,
               rst=>rst,                
               start=>start_reg1,
               clk=>clk,
			   Inc_Yi=>inc_Yi,
			   Inc_Xi=>inc_Xi,
               dMayor=>dMayor,
               dMenor=>dMenor,
               Inc_Xr=>inc_Xr,
               Inc_Yr=>inc_Yr);

   avance: entity work.av_signal
      port map(rst=>rst,                
               start=>start_reg2,
               clk=>clk,
               wait_sig=>wait_sig,
               dMayor=>dMayor,
               dMenor=>dMenor,
               av=>av);
				
	
    -- logica de salida
    incXr <= inc_Xr;
    incYr <= inc_Yr;
    incXi <= inc_Xi;
    incYi <= inc_Yi;

end Behavioral;

