library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--##################################--

entity pong_graph_animate is
   port(
        clk, rst: std_logic;
        btn: std_logic_vector(1 downto 0);
        video_on: in std_logic;
        pixel_x,pixel_y: in std_logic_vector(9 downto 0);
        graph_rgb: out std_logic_vector(2 downto 0)
   );
end pong_graph_animate;

--##################################--

architecture arch of pong_graph_animate is
   signal refr_tick: std_logic;
   -- coordenadas x, y  (0,0) a (639,479)
   signal pix_x, pix_y: unsigned(9 downto 0);
   constant MAX_X: integer:=640;
   constant MAX_Y: integer:=480;
   ----------------------------------------------
   -- Franja vertical como muro
   ----------------------------------------------
   -- limites de la pared
   constant WALL_X_L: integer:=32;
   constant WALL_X_R: integer:=35;
   ----------------------------------------------
   -- Barra vertical (paleta)
   ----------------------------------------------
   -- limites laterales
   constant BAR_X_L: integer:=600;
   constant BAR_X_R: integer:=603;
   -- limites superior e inferior
   signal bar_y_t, bar_y_b: unsigned(9 downto 0);
   constant BAR_Y_SIZE: integer:=72;
   -- registro de seguimiento del limite superior (con posicion X fija)
   signal bar_y_reg, bar_y_next: unsigned(9 downto 0);
   -- velocidad de la paleta en pixeles cada 1/60 seg
   constant BAR_V: integer:=4;
   ----------------------------------------------
   -- Pelota Cuadrada
   ----------------------------------------------
	
	
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
   constant BALL_SIZE: integer:=149;
   
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	-- limites laterales
   signal ball_x_l, ball_x_r: unsigned(9 downto 0);
   -- limites superior e inferior
   signal ball_y_t, ball_y_b: unsigned(9 downto 0);
   -- registros de seguimiento a izquiera y arriba
   signal ball_x_reg, ball_x_next: unsigned(9 downto 0);
   signal ball_y_reg, ball_y_next: unsigned(9 downto 0);
   -- registro de seguimiento de la velocidad de la pelota
   signal x_delta_reg, x_delta_next: unsigned(9 downto 0);
   signal y_delta_reg, y_delta_next: unsigned(9 downto 0);
   -- la velocidad puede ser positiva po negativa
   constant BALL_V_P: unsigned(9 downto 0)
            :=to_unsigned(1,10);
   constant BALL_V_N: unsigned(9 downto 0)
            :=unsigned(to_signed(-1,10));
				
				
				
				
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
   ----------------------------------------------
   -- ROM para reloj
   ----------------------------------------------
	signal rom_addr: std_logic_vector( 7 downto 0);
	signal rom_col: unsigned(7 downto 0);
   signal rom_data: std_logic_vector(149 downto 0);
   signal rom_bit: std_logic;
	
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
   ----------------------------------------------
   -- Salidas del objeto
   ----------------------------------------------
   signal wall_on, bar_on, sq_ball_on, rd_ball_on: std_logic;
   signal wall_rgb, bar_rgb, ball_rgb:
          std_logic_vector(2 downto 0);
			 
--*********************************--

begin
	   reloj: entity work.reloj_module
      port map(
					address=> rom_addr,
					data=> rom_data);
   -- registros
   process (clk,rst)
   begin
      if rst='1' then
         bar_y_reg <= (others=>'0');
         ball_x_reg <= (others=>'0');
         ball_y_reg <= (others=>'0');
         x_delta_reg <= ("0000000100");
         y_delta_reg <= ("0000000100");
      elsif (clk'event and clk='1') then
         bar_y_reg <= bar_y_next;
         ball_x_reg <= ball_x_next;
         ball_y_reg <= ball_y_next;
         x_delta_reg <= x_delta_next;
         y_delta_reg <= y_delta_next;
      end if;
   end process;
   pix_x <= unsigned(pixel_x);
   pix_y <= unsigned(pixel_y);
   -- refr_tick: 1-clock tick asserted at start of v-sync
   --       i.e., when the screen is refreshed (60 Hz)
   refr_tick <= '1' when (pix_y=481) and (pix_x=0) else
                '0';
   ----------------------------------------------
   -- Pared (linea vertical)
   ----------------------------------------------
   -- pixel de la pared
   wall_on <=
      '1' when (WALL_X_L<=pix_x) and (pix_x<=WALL_X_R) else
      '0';
   -- rgb output
   wall_rgb <= "001"; -- azul
   ----------------------------------------------
   -- Paleta (barra vertical a derecha)
   ----------------------------------------------
   -- limites
   bar_y_t <= bar_y_reg;
   bar_y_b <= bar_y_t + BAR_Y_SIZE - 1;
   -- pixel con paleta
   bar_on <=
      '1' when (BAR_X_L<=pix_x) and (pix_x<=BAR_X_R) and
               (bar_y_t<=pix_y) and (pix_y<=bar_y_b) else
      '0';
   -- rgb output
   bar_rgb <= "010"; --verde
	
   -- nueva posicion Y de la paleta
   process(bar_y_reg,bar_y_b,bar_y_t,refr_tick,btn)
   begin
      bar_y_next <= bar_y_reg; -- quieta
      if refr_tick='1' then
         if btn(1)='1' and bar_y_b<(MAX_Y-1-BAR_V) then
            bar_y_next <= bar_y_reg + BAR_V; -- abajo
         elsif btn(0)='1' and bar_y_t > BAR_V then
            bar_y_next <= bar_y_reg - BAR_V; -- arriba
         end if;
      end if;
   end process;

   ----------------------------------------------
   -- Pelota cuadrada
   ----------------------------------------------
   -- limites
   ball_x_l <= ball_x_reg;
   ball_y_t <= ball_y_reg;
   ball_x_r <= ball_x_l + BALL_SIZE - 1;
   ball_y_b <= ball_y_t + BALL_SIZE - 1;
   -- pixeles donde est� la pelota cuadrada
   sq_ball_on <=
      '1' when (ball_x_l<=pix_x) and (pix_x<=ball_x_r) and
               (ball_y_t<=pix_y) and (pix_y<=ball_y_b) else
      '0';
		
		
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	-- mapeo de la localizacion actual a ROM addr/col
	-- cambiar los tama�os en funcion de la imagen
   rom_addr <= std_logic_vector (pix_y(7 downto 0) - ball_y_t(7 downto 0));
   rom_col <= pix_x(7 downto 0) - ball_x_l(7 downto 0);
   rom_bit <= rom_data(to_integer(rom_col));
   
	--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
   -- pixeles donde est� la pelota redondeada
   rd_ball_on <=
      '1' when (sq_ball_on='1') and (rom_bit='1') else
      '0';
   -- rgb output
   ball_rgb <= "100";   -- red
	
   -- nueva posicion de la pelota
   ball_x_next <= ball_x_reg + x_delta_reg when refr_tick='1' else
                  ball_x_reg ;
   ball_y_next <= ball_y_reg + y_delta_reg when refr_tick='1' else
                  ball_y_reg ;
						
   -- nueva velocidad de la pelota
   process(x_delta_reg,y_delta_reg,ball_y_t,ball_x_l,ball_x_r,
           ball_y_t,ball_y_b,bar_y_t,bar_y_b)
   begin
      x_delta_next <= x_delta_reg;
      y_delta_next <= y_delta_reg;
      if ball_y_t < 1 then -- llego arriva
         y_delta_next <= BALL_V_P;
      elsif ball_y_b > (MAX_Y-1) then   -- llego abajo
         y_delta_next <= BALL_V_N;
      elsif ball_x_l <= WALL_X_R  then -- llego a la pared
         x_delta_next <= BALL_V_P;     -- rebota
      elsif (BAR_X_L<=ball_x_r) and (ball_x_r<=BAR_X_R) then
         -- reach x of right bar
         if (bar_y_t<=ball_y_b) and (ball_y_t<=bar_y_b) then
            x_delta_next <= BALL_V_N; -- golpea la paleta y rebota
         end if;
      end if;
   end process;
   ----------------------------------------------
   -- Multiplexado RGB
   ----------------------------------------------
   process(video_on,wall_on,bar_on,rd_ball_on,
           wall_rgb, bar_rgb, ball_rgb)
   begin
      if video_on='0' then
          graph_rgb <= "000"; --negro
      else
         if wall_on='1' then
            graph_rgb <= wall_rgb;
         elsif bar_on='1' then
            graph_rgb <= bar_rgb;
         elsif rd_ball_on='1' then
            graph_rgb <= ball_rgb;
         else
            graph_rgb <= "110"; -- fondo amarillo
         end if;
      end if;
   end process;
end arch;
