library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity controlpath is
    Port ( done_x : in  STD_LOGIC;
           done_y : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           start : in  STD_LOGIC;
           start_orden : out  STD_LOGIC;
           start_calc : out  STD_LOGIC;
           done : out STD_LOGIC);
end controlpath;

architecture control of controlpath is

	type state_type is (s_wait,s_init,s_orden,s_calc,s_done);
    signal state_reg,state_next: state_type;

begin

	registro : process( clk,rst )
	begin
		if rst='1' then
			state_reg<=s_wait;
		elsif rising_edge(clk) then
			state_reg<=state_next;		
		end if ;
	end process ; -- registro

	fsm : process( state_reg,clk,done_y,done_x,start )
	begin
	state_next<=state_next;
	start_orden<='0';
	start_calc<='0';
	done<='0';
		case( state_reg ) is
			when s_wait =>
				if start='1' then
					state_next<=s_init;
				else
					state_next<=state_reg;
				end if ;
			when s_init =>
				state_next<=s_orden;
				start_orden<='1';
			when s_orden =>
				state_next<=s_calc;
				start_calc<='1';
			when s_calc =>
				if (done_x and done_y)='1' then 
				--llegaron ambos al final 
					state_next<=s_wait;
					done<='1';				
				elsif (done_x or done_y)='1' then
				--llego a la coordenada final alguno,
				--debere esperar el otro
					state_next<=s_done;
				else
					state_next<=state_reg;
				end if ;
			when s_done =>
				if (done_x or done_y)='1' then
				--el segundo llego al final
					state_next<=s_wait;
					done<='1';
				else
					state_next<=state_reg;
				end if ;
		end case ;
	end process ; -- fsm

end control;

