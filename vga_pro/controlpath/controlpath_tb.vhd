LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY controlpath_tb IS
END controlpath_tb;
 
ARCHITECTURE behavior OF controlpath_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT controlpath
    PORT(
         done_x : IN  std_logic;
         done_y : IN  std_logic;
         clk : IN  std_logic;
         rst : IN  std_logic;
         start : IN  std_logic;
         start_orden : OUT  std_logic;
         start_calc : OUT  std_logic;
         done : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal done_x : std_logic := '0';
   signal done_y : std_logic := '0';
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal start : std_logic := '0';

 	--Outputs
   signal start_orden : std_logic;
   signal start_calc : std_logic;
   signal done : std_logic;

   -- Clock period definitions
   constant Tclk : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: controlpath PORT MAP (
          done_x => done_x,
          done_y => done_y,
          clk => clk,
          rst => rst,
          start => start,
          start_orden => start_orden,
          start_calc => start_calc,
          done => done
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for Tclk/2;
		clk <= '0';
		wait for Tclk/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst     <= '1';
      wait for Tclk*2;--2 
      rst<='0';
      start<='1';
      wait for Tclk;
      start<='0';
      wait for 11*Tclk;
      done_x<='1';
      done_y<='1';
      wait for Tclk;
      done_x<='0';
      done_y<='0';
      wait on done;
      report "...fin de ciclo" ;
      start<='1';
      wait for Tclk;
      start<='0';
      wait for 20*Tclk;
      done_x<='1';
      wait for Tclk;
      done_x<='0';
		wait for Tclk*3;
		done_y<='1';
      wait for Tclk;
      done_y<='0';
      wait on done;
		report "...fin de ciclo" ;
      start<='1';
      wait for Tclk;
      start<='0';
      wait for 20*Tclk;
      done_y<='1';
      wait for Tclk;
      done_y<='0';
		wait for Tclk;
		done_x<='1';
      wait for Tclk;
      done_x<='0';
      wait on done;
      assert false
      report "...fin de la simulacion" 
      severity failure;
   end process;

END;
