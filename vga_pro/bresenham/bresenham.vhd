library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--##################################--

entity bresenham is
	generic(W:natural:=11);
    Port ( x1 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           x2 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           y1 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           y2 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           start : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
     	   X : out STD_LOGIC_VECTOR(W-1 downto 0);
    	   Y : out STD_LOGIC_VECTOR(W-1 downto 0);
			load : out STD_LOGIC;
    	   done : out STD_LOGIC);
end bresenham;

--##################################--

architecture Behavioral of bresenham is

	signal dx,dy : STD_LOGIC_VECTOR(W-1 downto 0);
	signal dMayor_sig,dMenor_sig : STD_LOGIC_VECTOR(W-1 downto 0);
	signal inc_Xi,inc_Yi,inc_Xr,inc_Yr:STD_LOGIC_VECTOR(1 downto 0);
  signal start_calc,start_orden:STD_LOGIC;
  signal av_sig:STD_LOGIC_VECTOR(W+1 downto 0);
  signal done_x,done_y:STD_LOGIC;

--*********************************--
  begin
  
  --Data path
  --*********************************--
  --calculo de los delta
   dx_unit: entity work.delta
   generic map(W => W)
      port map(c0=>x1, 
      		     c1=>x2,
               rst=>rst,                
               start=>start,
               clk=>clk,
               d=>dx,
               inc=>inc_Xi);

   dy_unit: entity work.delta
   generic map(W => W)
      port map(c0=>y1, 
      		     c1=>y2,
               rst=>rst,                
               start=>start,
               clk=>clk,
               d=>dy,
               inc=>inc_Yi);

  --*********************************--
  -- ordenamiento 
   delta_may: entity work.delta_mayor
   generic map(W => W)
      port map(dx=>dx, 
      		     dy=>dy,
               rst=>rst,                
               start=>start_orden,
               clk=>clk,
			         Inc_Yi=>inc_Yi,
			         Inc_Xi=>inc_Xi,
               dMayor=>dMayor_sig,
               dMenor=>dMenor_sig,
               Inc_Xr=>inc_Xr,
               Inc_Yr=>inc_Yr);
  --*********************************--
  --calculo de pasos de avances
   avance: entity work.av_signal
   generic map(W => W)
      port map(rst=>rst,                
               start=>start_calc,
               clk=>clk,
               dMayor=>dMayor_sig,
               dMenor=>dMenor_sig,
               av=>av_sig);
  --*********************************--
  --calculo de las coordenadas
  coorx: entity work.coor
   generic map(W => W)
      port map(rst=>rst,                
               start=>start_calc,
               clk=>clk,
               av=>av_sig,
               ci=>x1,
               cf=>x2,
               Inc_I=>Inc_Xi,
               Inc_R=>Inc_Xr,
               dMenor=>dMenor_sig,
               done=>done_x,
               coor=>X);

  coory: entity work.coor
   generic map(W => W)
      port map(rst=>rst,                
               start=>start_calc,
               clk=>clk,
               av=>av_sig,
               ci=>y1,
               cf=>y2,
               Inc_I=>Inc_Yi,
               Inc_R=>Inc_Yr,
               dMenor=>dMenor_sig,
               done=>done_y,
               coor=>Y);
	--*********************************--
  --*********************************--
  -- Control path
  control: entity work.controlpath
      port map(rst=>rst,                
               start=>start,
               clk=>clk,
               done_x=>done_x,
               done_y=>done_y,
               start_calc=>start_calc,
               start_orden=>start_orden,
					load=>load,
               done=>done);



end Behavioral;

