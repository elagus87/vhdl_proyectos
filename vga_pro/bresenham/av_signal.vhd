library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--##################################--

entity av_signal is
	generic(W:natural:=11);
    Port ( dMayor,dMenor : 	in 	STD_LOGIC_VECTOR (W-1 downto 0);
    	   start:	in 	STD_LOGIC;
           clk,rst :		in 	STD_LOGIC;
           av : 			out STD_LOGIC_VECTOR (W+1 downto 0));
end av_signal;

--##################################--

architecture arch of av_signal is

	signal resta: signed(W downto 0):=(others=>'0');
	signal avRec_reg,avInc_reg: signed(W+1 downto 0):=(others=>'0');
	signal avRec_next,avInc_next:signed(W+1 downto 0); 
	signal av_reg,av_next: signed(W+1 downto 0):=(others=>'0'); 
	signal suma: signed(W+1 downto 0):=(others=>'0');
	signal dMen_s,dMay_s: signed(W downto 0):=(others=>'0');

--*********************************--
begin

	registros : process( clk,rst )
	begin
		if rst='1' then
			avRec_reg <= (others=>'0');
			avInc_reg <= (others=>'0');
			av_reg    <= (others=>'0');
		elsif rising_edge(clk) then
			avRec_reg <= avRec_next;
			avInc_reg <= avInc_next;
			av_reg    <= av_next;		
		end if ;
	end process ; -- registros
	--*********************************--
	dMay_s <= signed('0'& dMayor);
	dMen_s <= signed('0'& dMenor);

	avRec_next <= avRec_reg when start='0' else
				  dMen_s & '0'; -- multiplica x2 por ser dMenor>0

	resta <= (dMen_s-dMay_s); --ambos >0
	avInc_next <= avInc_reg when start='0' else
				  resta & '0'; -- multiplica x2, resta <= min representable

	suma <= av_reg + avInc_reg when (av_reg>=0) else
		     av_reg + avRec_reg;

	av_next <= 	(dMen_s & '0') - ('0'&dMay_s) when start='1' else
			   	suma;
	--*********************************--

	--logica de salida
	av <= STD_LOGIC_VECTOR(av_reg);
			   
end architecture ; -- arch