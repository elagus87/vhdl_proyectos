
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--##################################--

entity coor is
	 generic(W:natural:=11);
    Port ( av : in  STD_LOGIC_VECTOR (W+1 downto 0);
           ci,cf : in  STD_LOGIC_VECTOR (W-1 downto 0);
           inc_I : in  STD_LOGIC_VECTOR (1 downto 0);
           inc_R : in  STD_LOGIC_VECTOR (1 downto 0);
           dMenor: in STD_LOGIC_VECTOR(W-1 downto 0);
           start : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           done: out STD_LOGIC;
           coor : out  STD_LOGIC_VECTOR (W-1 downto 0));
end coor;

--##################################--

architecture Behavioral of coor is

  signal av_s:signed(W+1 downto 0);
  signal coor_reg,coor_next:signed(W-1 downto 0);
  signal dMenor_s:signed(W downto 0);
  signal incR,incI:signed(W-1 downto 0);
  signal done_reg,done_next:std_logic;
  --constant ext_zero:signed(W-1 downto 0):=(others =>'0');

--*********************************--
begin

	av_s <=signed(av);
	dMenor_s<=signed('0' & dMenor);
	incR <= TO_SIGNED(1,W) when inc_R="01" else
			  TO_SIGNED(-1,W) when inc_R="11" else
			  TO_SIGNED(0,W);
	incI <= TO_SIGNED(1,W)  when inc_I="01" else
			  TO_SIGNED(-1,W) when inc_I="11" else
			  TO_SIGNED(0,W);

  --*********************************--
  registro : process( clk,rst )
    begin
      if rst='1' then
        coor_reg <= (others=>'0');
		  done_reg<='0';
      elsif rising_edge(clk) then
        coor_reg <= coor_next;
		  done_reg<=done_next;
      end if ;
    end process ; -- registro

  --*********************************--
  coor_next <= signed(ci) when start='1' else
               coor_reg + incR when dMenor_s=0 else  
               coor_reg + incI when av_s>=0 else
               coor_reg + incR;
               
	done_next <= done_reg when start='1' else
               '1' when coor_reg=signed(cf) else
               '0';
  --*********************************--
	coor <= std_logic_vector(coor_reg);
  done <= done_reg;
end Behavioral;

