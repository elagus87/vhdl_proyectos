ibrary IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--##################################--

entity bresenham_comp is
	generic(W:natural:=11);
    Port ( x0 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           x1 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           y0 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           y1 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           start : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
       	   X : out STD_LOGIC_VECTOR(W-1 downto 0);
      	   Y : out STD_LOGIC_VECTOR(W-1 downto 0);
			     load : out STD_LOGIC;
    	     done : out STD_LOGIC);
end bresenham_comp;

--##################################--

architecture Behavioral of bresenham_comp is
  -- deltas
  signal dc_X,dc_Y:          signed(W-1 downto 0):=(others=>'0');
  signal coor_X0,coor_X1,coor_Y0,coor_Y1:   signed(W-1 downto 0):=(others=>'0');
  signal menos_dc_X,menos_dc_Y :   STD_LOGIC_VECTOR (W-1 downto 0):=(others=>'0');
  signal inc_X_next,inc_X_reg,inc_Y_next,inc_Y_reg:STD_LOGIC_VECTOR (1 downto 0):="00";
  signal d_X_reg,d_X_next,d_Y_reg,d_Y_next : STD_LOGIC_VECTOR (W-1 downto 0):=(others=>'0');
  -- delta mayor
  signal dx_gt_dy:        STD_LOGIC;
  signal Inc_Xr_reg,Inc_Yr_reg:   STD_LOGIC_VECTOR (1 downto 0);
  signal Inc_Xr_next,Inc_Yr_next: STD_LOGIC_VECTOR (1 downto 0);
  signal Mayor_reg,Menor_reg:   STD_LOGIC_VECTOR(W-1 downto 0);
  signal Mayor_next,Menor_next: STD_LOGIC_VECTOR(W-1 downto 0);

begin

  registros : process( clk,rst )
    begin
      if rst = '1' then
      --deltas
        d_X_reg   <= (others=>'0');
        inc_X_reg <= (others=>'0');
        d_Y_reg   <= (others=>'0');
        inc_Y_reg <= (others=>'0');
      -- delta mayor
        Inc_Xr_reg <= (others=>'0');
        Inc_Yr_reg <= (others=>'0');
        Mayor_reg  <= (others=>'0');
        Menor_reg  <= (others=>'0');
      elsif rising_edge(clk) then
      --deltas
        d_X_reg   <= d_X_next;
        inc_X_reg <= inc_X_next;
        d_Y_reg   <= d_X_next;
        inc_Y_reg <= inc_X_next;   
      -- delta mayor     
        Inc_Xr_reg <= Inc_Xr_next;
        Inc_Yr_reg <= Inc_Yr_next;
        Mayor_reg  <= Mayor_next;
        Menor_reg  <= Menor_next;
      end if ;
    end process ; -- registros

  --*************DELTAS*************--
      coor_X0 <= signed(x0);
      coor_X1 <= signed(x1);
      coor_Y0 <= signed(y0);
      coor_Y1 <= signed(y1);
      
      dc_X  <= signed(coor_X1-coor_X0); --siempre arranca de 0, no habrá OV
      menos_dc_X <= STD_LOGIC_VECTOR(not dc_X +1); --cambio de signo
      dc_Y  <= signed(coor_Y1-coor_Y0); --siempre arranca de 0, no habrá OV
      menos_dc_Y <= STD_LOGIC_VECTOR(not dc_Y +1); --cambio de signo
      
      --*********************************--
      inc_X_next <= inc_X_reg when start='0' else
            "01" when (dc_X>0) else --(+1)
            "11";         --(-1)
      d_X_next <= d_X_reg when start ='0' else
            STD_LOGIC_VECTOR(dc_X(W-1 downto 0)) when inc_X_next="01" else
              menos_dc_X(W-1 downto 0);

      inc_Y_next <= inc_Y_reg when start='0' else
            "01" when (dc_Y>0) else --(+1)
            "11";         --(-1)
      d_Y_next <= d_Y_reg when start ='0' else
            STD_LOGIC_VECTOR(dc_Y(W-1 downto 0)) when inc_Y_next="01" else
              menos_dc_Y(W-1 downto 0);
      --*********************************--
      -- logica de interconexion: 
      -- d_X_reg, inc_X_reg, d_Y_reg, inc_Y_reg;
      -- señales intermedias rumbo al calculo del 
      -- delta mayor

  --*************DELTA MAYOR***************--
      dx_gt_dy <= '1' when d_X_reg>=d_Y_reg else
              '0';      

      Inc_Xr_next <= Inc_Xr_reg when start='0' else
               inc_X_reg when dx_gt_dy='1' else
               "00";
      Inc_Yr_next <= Inc_Yr_reg when start='0' else
               inc_Y_reg when dx_gt_dy='0' else
               "00";
      Mayor_next  <= Mayor_reg when start='0' else
               d_X_reg when dx_gt_dy='1' else
               d_Y_reg;
      Menor_next  <= Menor_reg when start='0' else
               d_Y_reg when dx_gt_dy='1' else
               d_X_reg;
      --*********************************--
      -- logica de interconexion
      Inc_Xr <= Inc_Xr_reg;
      Inc_Yr <= Inc_Yr_reg;
      dMayor <= Mayor_reg;
      dMenor <= Menor_reg;

end Behavioral ;