library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--##################################--

entity size_gen is
   port(
        clk, rst: std_logic;
        video_on: in std_logic;
        pixel_x,pixel_y: in std_logic_vector(9 downto 0);--de barrido
        bres_x,bres_y: in std_logic_vector(9 downto 0);--de bresenham
        graph_rgb: out std_logic_vector(2 downto 0)
      );
end size_gen;

--##################################--

architecture arch of size_gen is
   signal refr_tick: std_logic;
   -- coordenadas x, y  (0,0) a (639,479)
   signal pix_x, pix_y: unsigned(9 downto 0);
   constant MAX_X: integer:=640;
   constant MAX_Y: integer:=480;

   ----------------------------------------------
   -- Pelota Cuadrada
   ----------------------------------------------
   constant BALL_SIZE: integer:=9; -- 8
   -- limites laterales
   signal ball_x_l, ball_x_r: unsigned(9 downto 0);
   -- limites superior e inferior
   signal ball_y_t, ball_y_b: unsigned(9 downto 0);
   -- registros de seguimiento a izquiera y arriba
   signal ball_x_reg, ball_x_next: unsigned(9 downto 0);
   signal ball_y_reg, ball_y_next: unsigned(9 downto 0);
				
   ----------------------------------------------
   -- ROM para pelota redondeada
   ----------------------------------------------
   type rom_type is array (0 to 8)
        of std_logic_vector(0 to 8);
   -- definicion de la ROM
   constant BALL_ROM: rom_type :=
   (
      "000111000",
		"001111100", --   ****
      "011111110", --  ******
      "111111111", -- ********
      "111111111", -- ********
      "111111111", -- ********
      "111111111", -- ********
      "011111110", --  ******
      "001111100"  --   ****

   );
   signal rom_addr, rom_col: unsigned(3 downto 0);
   signal rom_data: std_logic_vector(8 downto 0);
   signal rom_bit: std_logic;	
	
   ----------------------------------------------
   -- Salidas del objeto
   ----------------------------------------------
   signal sq_ball_on, rd_ball_on: std_logic;
   signal ball_rgb:std_logic_vector(2 downto 0);
			 
--*********************************--

begin
   -- registros
   process (clk,rst)
   begin
      if rst='1' then
         ball_x_reg <= (others=>'0');
         ball_y_reg <= (others=>'0');
      elsif (clk'event and clk='1') then
         ball_x_reg <= ball_x_next;
         ball_y_reg <= ball_y_next;
      end if;
   end process;
   pix_x <= unsigned(pixel_x);
   pix_y <= unsigned(pixel_y);
   -- refr_tick: 1-clock tick asserted at start of v-sync
   --       i.e., when the screen is refreshed (60 Hz)
   refr_tick <= '1' when (pix_y=481) and (pix_x=0) else
                '0';
   
   ----------------------------------------------
   -- Pelota cuadrada
   ----------------------------------------------
   -- limites
   ball_x_l <= ball_x_reg;--posicion actual a izquierda
   ball_y_t <= ball_y_reg;--posicion actual arriba
   ball_x_r <= ball_x_l + BALL_SIZE - 1;--posicion a derecha en funcion de ball size
   ball_y_b <= ball_y_t + BALL_SIZE - 1;--posicion abajo en funcion de ball size
   -- pixeles donde est� la pelota cuadrada
   sq_ball_on <= --cuando las coordenadas estan barriendo por donde esta la pelota cuadrada
      '1' when (ball_x_l<=pix_x) and (pix_x<=ball_x_r) and
               (ball_y_t<=pix_y) and (pix_y<=ball_y_b) else
      '0';
	
	-- mapeo de la localizacion actual a ROM addr/col	
   rom_addr <= pix_y(3 downto 0) - ball_y_t(3 downto 0);
   rom_col <= pix_x(3 downto 0) - ball_x_l(3 downto 0);
   rom_data <= BALL_ROM(to_integer(rom_addr));
   rom_bit <= rom_data(to_integer(rom_col));
   -- pixeles donde est� la pelota redondeada
   rd_ball_on <=
      '1' when (sq_ball_on='1') and (rom_bit='1') else
      '0';
   -- rgb output
   ball_rgb <= "100";   -- red
	
   -- nueva posicion de la pelota
   ball_x_next <= bres_x ;--posiciones calculada por bresenham
   ball_y_next <= bres_y ;--en otro modulo
						
-------------------------------
   -- Multiplexado RGB
   ----------------------------------------------
   process(video_on,rd_ball_on,ball_rgb)
   begin
      if video_on='0' then
          graph_rgb <= "000"; --negro
      else
         if rd_ball_on='1' then
            graph_rgb <= ball_rgb;
         else
            graph_rgb <= "110"; -- fondo amarillo
         end if;
      end if;
   end process;
end arch;
