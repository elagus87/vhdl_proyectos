library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--##################################--


entity delta_mayor is
	generic(W:natural:=11);
    Port ( dx,dy : 			in  STD_LOGIC_VECTOR (W-1 downto 0);
		   Inc_Yi,Inc_Xi :	in  STD_LOGIC_VECTOR (1 downto 0);
           start: 			in 	STD_LOGIC;
           clk,rst :		in 	STD_LOGIC;
           Inc_Yr,Inc_Xr : 	out STD_LOGIC_VECTOR (1 downto 0);
           dMayor,dMenor : 	out STD_LOGIC_VECTOR (W-1 downto 0));
end delta_mayor;

--##################################--

architecture arch of delta_mayor is

	signal dx_gt_dy:				STD_LOGIC;
	signal Inc_Xr_reg,Inc_Yr_reg:  	STD_LOGIC_VECTOR (1 downto 0);
	signal Inc_Xr_next,Inc_Yr_next: STD_LOGIC_VECTOR (1 downto 0);
	signal Mayor_reg,Menor_reg:		STD_LOGIC_VECTOR(W-1 downto 0);
	signal Mayor_next,Menor_next:	STD_LOGIC_VECTOR(W-1 downto 0);

--*********************************--
begin

	registro : process( clk,rst )
	begin
		if rst = '1' then
			Inc_Xr_reg <= (others=>'0');
			Inc_Yr_reg <= (others=>'0');
			Mayor_reg  <= (others=>'0');
			Menor_reg  <= (others=>'0');
		elsif rising_edge(clk) then
			Inc_Xr_reg <= Inc_Xr_next;
			Inc_Yr_reg <= Inc_Yr_next;
			Mayor_reg  <= Mayor_next;
			Menor_reg  <= Menor_next;			
		end if ;
	end process ; -- registro
	--*********************************--
	dx_gt_dy <= '1' when dx>=dy else
					'0';
	

	Inc_Xr_next <= Inc_Xr_reg when start='0' else
				   Inc_Xi when dx_gt_dy='1' else
				   "00";
	Inc_Yr_next <= Inc_Yr_reg when start='0' else
				   Inc_Yi when dx_gt_dy='0' else
				   "00";
	Mayor_next  <= Mayor_reg when start='0' else
				   dx when dx_gt_dy='1' else
				   dy;
	Menor_next  <= Menor_reg when start='0' else
				   dy when dx_gt_dy='1' else
				   dx;
	--*********************************--
	-- logica de salida
	Inc_Xr <= Inc_Xr_reg;
	Inc_Yr <= Inc_Yr_reg;
	dMayor <= Mayor_reg;
	dMenor <= Menor_reg;

end architecture ; -- arch
