## Algoritmo de Bresenham
Se basa en el siguiente codigo el cual contempla los casos en donde el algoritmo falla (lineas horizontales y verticales)
```m
%% Implementación de algoritmo de trazado de lineas

clc;
close all;
clear all;

X1=0;
Y1=0;
X2=-8;
Y2=10;
dY = (Y2 - Y1)
dX = (X2 - X1)

%% 
% Incrementos inclinados, es decir en la direccion de menor avance

if dY >= 0
    IncYi=1;
else
    dY = -dY;   %los delta son positivos, cambia el incremento
    IncYi = -1; %indico cambio de signo
end

if dX >= 0
    IncXi=1;
else
    dX = -dX;
    IncXi = -1;%indico cambio de signo
end
    
%% 
% Incrementos rectos, es decir en la direccion de mayor avance

if dX >= dY  
    IncYr = 0;    % comenzar� avanzando en X
    IncXr = IncXi;
    dMayor=dX;
    dMenor=dY;
else
    IncXr = 0;    % comenzar� avanzando en Y
    IncYr = IncYi; 
    %% cambio los delta para usar el mismo loop
    dMayor=dY;
    dMenor=dX;
end

X = X1;
Y = Y1;

avRecto = (2 * dMenor)
av = (avRecto - dMayor)
avInclinado = (av - dMayor)


xcoor=[];
ycoor=[];
avance=[];
i=0;
bk_exit=0;
%% 
% Modificado
% figure('name', 'avance del loop'),hold on;

if dMenor==0 %solo hay avance horizontal o vertical
    while(1)
        X = (X + IncXr)
        Y = (Y + IncYr)
        xcoor=[xcoor X];
        ycoor=[ycoor Y];
        if bk_exit==1
            break;
        elseif (X == X2 && Y == Y2)%una vuelta mas y termina
            bk_exit=1;
            disp('ya termina');
        end
    end
else
    while (1)
        xcoor=[xcoor X];
        ycoor=[ycoor Y];
        i=i+1;
         avance=[avance av];
        if av >= 0
            legend('inclinado');
            X = (X + IncXi);
            Y = (Y + IncYi);
            av=av+avInclinado;
        else
            legend('recto');
            X = (X + IncXr);
            Y = (Y + IncYr);
            av=av+avRecto;
        end
        
        if bk_exit==1
                break;
        elseif (X == X2 && Y == Y2)%una vuelta mas y termina
            bk_exit=1;
            disp('ya termina');
        end

    end
end

disp('X=');
disp(xcoor);
disp('Y=');
disp(ycoor);
figure;
subplot(211),stem(xcoor,ycoor); title(['origen=(' num2str(X1) ',' num2str(Y1) ') ,destino=(' num2str(X2) ',' num2str(Y2) ')']);
xlim([-10 10]);ylim([-10 10]);
subplot(212),stem([X1 X2],[Y1 Y2]);title(['ideal, I= ' num2str(i)]);
xlim([-10 10]);ylim([-10 10]);
```