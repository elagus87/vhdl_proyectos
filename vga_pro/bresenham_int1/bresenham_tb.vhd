LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY bresenham_tb IS
END bresenham_tb;
 
ARCHITECTURE behavior OF bresenham_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT bresenham
    generic(W:natural:=11);
    PORT(
         x1 : IN  std_logic_vector(W-1 downto 0);
         x2 : IN  std_logic_vector(W-1 downto 0);
         y1 : IN  std_logic_vector(W-1 downto 0);
         y2 : IN  std_logic_vector(W-1 downto 0);
         start : IN  std_logic;
         rst : IN  std_logic;
         clk : IN  std_logic;
         X : OUT  std_logic_vector(W-1 downto 0);
         Y : OUT  std_logic_vector(W-1 downto 0);
			load : out STD_LOGIC;
         done : OUT  std_logic
        );
    END COMPONENT;
    
   constant Tclk : time := 10 ns;
   constant Wt : natural:= 11;
   --Inputs
   signal x1 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal x2 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal y1 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal y2 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal start : std_logic := '0';
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal X : std_logic_vector(Wt-1 downto 0);
   signal Y : std_logic_vector(Wt-1 downto 0);
	signal load : STD_LOGIC;
   signal done : std_logic;

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bresenham PORT MAP (
          x1 => x1,
          x2 => x2,
          y1 => y1,
          y2 => y2,
          start => start,
          rst => rst,
          clk => clk,
          X => X,
          Y => Y,
			 load=>load,
          done => done
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for Tclk/2;
		clk <= '0';
		wait for Tclk/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst     <= '1';
      wait for Tclk*2;--2 
      rst<='0';

      x1      <= (others =>'0');
      y1      <= (others =>'0');
      x2      <= std_logic_vector(TO_SIGNED(3,Wt));
      y2      <= std_logic_vector(TO_SIGNED(10,Wt));
      wait for Tclk; 
      start<='1';
      wait for Tclk; 
      start<='0';
      wait on done;
		wait for Tclk; 
		report "...fin de ciclo"; 
      x1      <= (others =>'0');
      y1      <= (others =>'0');
      x2      <= std_logic_vector(TO_SIGNED(-10,Wt));
      y2      <= std_logic_vector(TO_SIGNED(-8,Wt));
      wait for Tclk; 
      start<='1';
      wait for Tclk; 
      start<='0';
      wait on done;
      wait for Tclk*3; 

      assert false
      report "...fin de la simulacion" 
      severity failure;
   end process;

END;
