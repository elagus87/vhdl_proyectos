library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity data_path is
	generic(W:natural:=11);
    Port ( x1 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           x2 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           y1 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           y2 : in  STD_LOGIC_VECTOR (W-1 downto 0);
           start : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
			  done : out  STD_LOGIC;
           cx,cy: out STD_LOGIC_VECTOR (W-1 downto 0));
end data_path;

architecture Behavioral of data_path is
	signal dx,dy : STD_LOGIC_VECTOR(W-1 downto 0);
	signal dMayor,dMenor : STD_LOGIC_VECTOR(W-1 downto 0);
	signal donex,doney:STD_LOGIC;
	signal av : STD_LOGIC_VECTOR(W+1 downto 0);
	signal inc_Xi,inc_Yi,inc_Xr,inc_Yr:STD_LOGIC_VECTOR(1 downto 0);
	signal start_reg1,start_reg2,start_next1,start_next2:STD_LOGIC;

begin


	registros : process( clk,rst )
	begin
		if rst='1' then
			start_reg1 <= '0';
			start_reg2 <= '0';
		elsif rising_edge(clk) then
			start_reg1 <= start_next1;
			start_reg2 <= start_next2;
		end if ;
	end process ; -- registros

	start_next1 <= start;
	start_next2 <= start_reg1;

   dx_unit: entity work.delta
      port map(c0=>x1, 
      		   c1=>x2,
               rst=>rst,                
               start=>start_next1,
               clk=>clk,
               d=>dx,
               inc=>inc_Xi);

   dy_unit: entity work.delta
      port map(c0=>y1, 
      		   c1=>y2,
               rst=>rst,                
               start=>start_next1,
               clk=>clk,
               d=>dy,
               inc=>inc_Yi);


   delta_may: entity work.delta_mayor
      port map(dx=>dx, 
      		   dy=>dy,
               rst=>rst,                
               start=>start_reg1,
               clk=>clk,
			   Inc_Yi=>inc_Yi,
			   Inc_Xi=>inc_Xi,
               dMayor=>dMayor,
               dMenor=>dMenor,
               Inc_Xr=>inc_Xr,
               Inc_Yr=>inc_Yr);

   avance: entity work.av_signal
      port map(rst=>rst,                
               start=>start_reg2,
               clk=>clk,
               dMayor=>dMayor,
               dMenor=>dMenor,
               av=>av);
					
	coorx: entity work.coor
      port map(rst=>rst,                
               start=>start_reg2,
               clk=>clk,
               dMenor=>dMenor,
               av=>av,
					ci=>x1,
					cf=>x2,
					inc_I=>inc_Xi,
					inc_R=>inc_Xr,
					done=>donex,
					coor=>cx);
					
	coory: entity work.coor
      port map(rst=>rst,                
               start=>start_reg2,
               clk=>clk,
               dMenor=>dMenor,
               av=>av,
					ci=>y1,
					cf=>y2,
					inc_I=>inc_Yi,
					inc_R=>inc_Yr,
					done=>doney,
					coor=>cy);
	done <= donex and doney;
end Behavioral;


