LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY data_path_tb IS
END data_path_tb;
 
ARCHITECTURE behavior OF data_path_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT data_path
	 generic(W:natural:=11);
    PORT(
         x1 : IN  std_logic_vector(W-1 downto 0);
         x2 : IN  std_logic_vector(W-1 downto 0);
         y1 : IN  std_logic_vector(W-1 downto 0);
         y2 : IN  std_logic_vector(W-1 downto 0);
         start : IN  std_logic;
         rst : IN  std_logic;
         clk : IN  std_logic;
			done: OUT std_logic;
         cx : OUT  std_logic_vector(W-1 downto 0);
         cy : OUT  std_logic_vector(W-1 downto 0)
        );
    END COMPONENT;
    
	constant Tclk : time := 10 ns;
   constant Wt : natural:= 11;
   --Inputs
   signal x1 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal x2 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal y1 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal y2 : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal start : std_logic := '0';
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal cx : std_logic_vector(Wt-1 downto 0);
   signal cy : std_logic_vector(Wt-1 downto 0);
	signal done: std_logic;

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: data_path PORT MAP (
          x1 => x1,
          x2 => x2,
          y1 => y1,
          y2 => y2,
          start => start,
          rst => rst,
          clk => clk,
          cx => cx,
          cy => cy,
			 done => done
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for Tclk/2;
		clk <= '1';
		wait for Tclk/2;
   end process;
 

   start_proc : process
   begin
      wait for Tclk*3;--3
      start<='1';
      wait for Tclk;--4
      start<='0';
      wait for Tclk*15;--20
      start<='1';
      wait for Tclk;--21
      start<='0';
      wait for Tclk*14;--35
   end process ; -- start_proc
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst     <= '1';
      wait for Tclk*2;--2 
      rst<='0';

      x1      <= (others =>'0');
      y1      <= (others =>'0');
      x2      <= std_logic_vector(TO_UNSIGNED(10,Wt));
      y2      <= std_logic_vector(TO_UNSIGNED(3,Wt));

      wait for Tclk*16;--17

      x1      <= (others =>'0');
      y1      <= (others =>'0');
      x2      <= std_logic_vector(TO_UNSIGNED(10,Wt));
      y2      <= std_logic_vector(TO_UNSIGNED(10,Wt));

      wait for Tclk*17;--34
      assert false
      report "fin de la simulacion" 
      severity failure;
   end process;

END;
