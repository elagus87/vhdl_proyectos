library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--##################################--

entity delta is
	generic(W:natural:=11);
    Port ( c0 : 	in  STD_LOGIC_VECTOR (W-1 downto 0);
           c1 :		in  STD_LOGIC_VECTOR (W-1 downto 0);
           clk :	in 	STD_LOGIC;
           start: 	in 	STD_LOGIC;
           rst : 	in 	STD_LOGIC;
           inc : 	out STD_LOGIC_VECTOR (1 downto 0);
           d  : 	out STD_LOGIC_VECTOR (W-1 downto 0));
end delta;

--##################################--

architecture Behavioral of delta is

	signal dc: 			    signed(W-1 downto 0):=(others=>'0');
	signal coor0,coor1: 	signed(W-1 downto 0):=(others=>'0');
	signal menos_dc :		STD_LOGIC_VECTOR (W-1 downto 0):=(others=>'0');
	signal inc_next,inc_reg:STD_LOGIC_VECTOR (1 downto 0):="00";
	signal d_reg,d_next :	STD_LOGIC_VECTOR (W-1 downto 0):=(others=>'0');

--*********************************--
begin

	registro : process( clk,rst )
	begin
		if rst = '1' then
			d_reg 	<= (others=>'0');
			inc_reg <= (others=>'0');
		elsif rising_edge(clk) then
			d_reg 	<= d_next;
			inc_reg <= inc_next;				
		end if ;
	end process ; -- registro

	--*********************************--
	coor0 <= signed(c0);
	coor1 <= signed(c1);
	
	dc 	<= signed(coor1-coor0); --siempre arranca de 0, no habrá OV
	menos_dc <= STD_LOGIC_VECTOR(not dc +1); --cambio de signo
	
	--*********************************--
	inc_next <= inc_reg when start='0' else
				"01" when (dc>0) else --(+1)
				"11";				  --(-1)
	

	d_next <= d_reg when start ='0' else
			  STD_LOGIC_VECTOR(dc(W-1 downto 0)) when inc_next="01" else
		      menos_dc(W-1 downto 0);
	--*********************************--
	-- logica de salida
	d 	<= d_reg;
	inc <= inc_reg;

end Behavioral;

