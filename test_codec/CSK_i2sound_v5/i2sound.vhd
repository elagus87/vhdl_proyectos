----------------------------------------------------------------------------------
-- University: 	 UTN-FRBA
-- Author:			 Equipo docente 
-- 
-- Create Date:    08:35:52 09/24/2018 
-- Design Name: 
-- Module Name:    i2sound - Behavioral 
-- Project Name:   tpo - Tecnicas Digitales 1
-- Target Devices: Altera/Intel DE1 - EP2C20F484C7
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;



entity i2sound is
	port(
				CLOCK_24:in std_logic_vector(1 downto 0); --24 MHz
				CLOCK_27:in std_logic_vector(1 downto 0); --27 MHz
				CLOCK_50:in std_logic;							--50 MHz
				EXT_CLOCK:in std_logic;							--clock externo
				
				KEY:in std_logic_vector(3 downto 0);		--pulsadores KEY(3)<=RST
				
				SW:in std_logic_vector(9 downto 0);			--tira de switches
				
				HEX0:out std_logic_vector(6 downto 0);		--7 segmentos 0
				HEX1:out std_logic_vector(6 downto 0);		--7 segmentos 1
				HEX2:out std_logic_vector(6 downto 0);		--7 segmentos 2
				HEX3:out std_logic_vector(6 downto 0);		--7 segmentos 3
				
				LEDG:out std_logic_vector(7 downto 0);		--tira de leds verdes
				LEDR:out std_logic_vector(9 downto 0);		--tira de leds rojos
				
				I2C_SDAT:inout std_logic;						--datos i2c 
				I2C_SCLK:out std_logic;							--reloj de comunicacion
				
				AUD_ADCLRCK:inout std_logic;					--clock de canales LR del adc IN
				AUD_ADCDAT:in std_logic;						--datos ADC							IN
				AUD_DACLRCK:inout std_logic;					--clock de canales LR del dac IN
				AUD_DACDAT:out std_logic;						--datos DAC						
				AUD_BCLK:inout std_logic;						--clock de flujo de bits		IN
				AUD_XCK:out std_logic 							--clock del chip
		 );	
		 
end i2sound;
----------------------------------------------------------------------------
--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%--
--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%--
----------------------------------------------------------------------------
architecture Behavioral of i2sound is
	--signals
	signal VOL: unsigned(6 downto 0);
	signal CLK_18_4:std_logic;
	
	signal reset,reset_N:std_logic;																		 -- resets sincronizados
	
	signal adc_paralel,dac_paralel:std_logic_vector(31 downto 0):=(others=>'0');			 -- del paralelizador
	signal adc_L_reg,adc_L_next,adc_R_reg,adc_R_next:signed(15 downto 0):=(others=>'0'); -- buffers de entrada
	signal dac_L_reg,dac_L_next,dac_R_reg,dac_R_next:signed(15 downto 0):=(others=>'0'); -- buffers de entrada
	signal salida:std_logic:='0';
	
	signal AUD_ADCLRCK_sync,AUD_BCLK_sync,AUD_DACLRCK_sync:std_logic;							 -- clks sincronizados
	signal LR_TICK_ADC,LR_TICK_DAC,BIT_TICK:std_logic;												 -- ticks
----------------------------------------------------------------------------	
-- componentes 
----------------------------------------------------------------------------
		component PLL  
					port(
							inclk0: in std_logic;
							c0:out std_logic
						 ); 
						end component PLL;

		component I2C_AV_Config  
					port(
							iCLK: in std_logic;
							iRST_N: in std_logic;
							iVOL: std_logic_vector(6 downto 0);
							I2C_SCLK:out std_logic;
							I2C_SDAT:inout std_logic
						 ); 
						end component I2C_AV_Config;

		component edge_detect  
				    Port ( 	sig : in  STD_LOGIC;
								clk : in STD_LOGIC;
								rst : in STD_LOGIC;
				           	tick : out  STD_LOGIC);
						end component edge_detect;			
		component sipo_reg is
					generic(W: natural);
					port(
							clk,rst,enable,serial_in:in std_logic;
							paralel_out:out std_logic_vector(W-1 downto 0)
						 );
					end component sipo_reg;		
		component piso_reg is 
					generic(W:natural);
					port(
							clk,rst,load,enable :in std_logic;
							paralel_input:in std_logic_vector(W-1 downto 0);
							serial_output:out std_logic
						 );
					end component piso_reg;	

----------------------------------------------------------------------------
begin
----------------------------------------------------------------------------
-- seteos iniciales accesorios
----------------------------------------------------------------------------
	HEX0		<=	"0001000";
	HEX1		<=	"1000111";
	HEX2		<=	"1000000";
	HEX3		<=	"0001001";
	LEDG		<=	'0'& std_logic_vector(VOL);
	--LEDR		<=	"0000000000";
	
	I2C_SDAT <= 'Z'; 				--seteo en alta impedancia


----------------------------------------------------------------------------
-- instancias	
----------------------------------------------------------------------------
-- clocks para el codec
	pll_conf: PLL port map(
								 inclk0 => CLOCK_27(0),
								 c0  => CLK_18_4);
					 
	AUD_XCK		<=	CLK_18_4;
						
--configuracion del i2c						
	i2c_audio_config: I2C_AV_Config
					  port map(
								 iCLK => CLOCK_50 ,
								 iRST_N => reset_N ,
								 iVOL => std_logic_vector(VOL) ,
								 I2C_SCLK => I2C_SCLK ,
								 I2C_SDAT => I2C_SDAT);
								 
-- detectores de flanco								 
	lrclk_ADC_tick: edge_detect
					  port map(
					  			sig =>AUD_ADCLRCK_sync,
					  			clk => CLOCK_50,
					  			rst => reset,
					  			tick => LR_TICK_ADC
					  		  );		
	bclkadc_tick: edge_detect
					  port map(
					  			sig =>AUD_BCLK_sync,
					  			clk => CLOCK_50,
					  			rst => reset,
					  			tick => BIT_TICK
					  		  );	
	lrclk_DAC_tick: edge_detect
					  port map(
					  			sig =>AUD_DACLRCK_sync,
					  			clk => CLOCK_50,
					  			rst => reset,
					  			tick => LR_TICK_DAC
					  		  );		 

-- registro sipo de entrada
	sipo_reg_adc: sipo_reg
						generic map(W=>32)
						port map(
									clk => CLOCK_50,
									rst => reset,
									enable => BIT_TICK,
									serial_in => AUD_ADCDAT,
									paralel_out => adc_paralel
								);
-- registro piso de salida
	piso_reg_dac: piso_reg
						generic map(W=>32)
						port map(
									clk=>CLOCK_50,
									rst=>reset,
									load=>LR_TICK_DAC,
									enable=>BIT_TICK,
									paralel_input=>dac_paralel,
									serial_output=>salida
								);
								
----------------------------------------------------------------------------
-- sincronizadores
----------------------------------------------------------------------------
	sync_rst:process(CLOCK_50)
	begin
		if rising_edge(CLOCK_50) then
			reset_N<= KEY(3);
			reset <= not(KEY(3));
		end if;
	end process sync_rst;
	
	sync_temp:process(CLOCK_50)
	begin
		if rising_edge(CLOCK_50) then
			AUD_ADCLRCK_sync <= AUD_ADCLRCK;
			AUD_DACLRCK_sync <= AUD_DACLRCK;
			AUD_BCLK_sync <= AUD_BCLK;
		end if;
	end process sync_temp;
----------------------------------------------------------------------------
-- logica de datos de entrada
----------------------------------------------------------------------------

-- control de datos de entrada
--registro_de_entrada:process(CLOCK_50,reset)
--begin
--	if reset='1' then 
--		ADC_L_reg <= (others=>'0');
--		ADC_R_reg <= (others=>'0');
--	elsif rising_edge(CLOCK_50) then
--		ADC_L_reg <= ADC_L_next;
--		ADC_R_reg <= ADC_R_next;
--	end if;
--end process registro_de_entrada;

-- ADC_L_next <= signed(adc_paralel(63 downto 32)) when LR_TICK_ADC='1' else
--									 ADC_L_reg;
-- ADC_R_next <= signed(adc_paralel(31 downto 0)) when LR_TICK_ADC='1' else
--									 ADC_R_reg;
 ADC_L_reg <= signed(adc_paralel(31 downto 16));
 ADC_R_reg <= signed(adc_paralel(15 downto 0));
 
----------------------------------------------------------------------------
-- logica de datos de salida
----------------------------------------------------------------------------

	contador_datos_de_salida:process(CLOCK_50,reset)
	-- son 16 bits por canal justificado a izquierda
	begin
		if reset='1' then
			dac_L_reg <= (others => '0');
			dac_R_reg <= (others => '0');
		elsif rising_edge(CLOCK_50)  then
			dac_L_reg <= dac_L_next;
			dac_R_reg <= dac_R_next;
		end if;
	end process;

-- logica de estado futuro	
	dac_L_next <= adc_L_reg when LR_TICK_ADC='1' else dac_L_reg;
	dac_R_next <= adc_R_reg when LR_TICK_ADC='1' else dac_R_reg;
	
-- salida al registro piso
	dac_paralel <= std_logic_vector(dac_L_reg & dac_R_reg);
--	dac_paralel <= std_logic_vector(adc_L_reg & adc_R_reg);
	
	AUD_DACDAT<=salida;

	
----------------------------------------------------------------------------
-- control de volumen
----------------------------------------------------------------------------
   VOL<=unsigned(SW(9 downto 3));


end Behavioral;

