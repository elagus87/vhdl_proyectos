library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity prueba_ac97 is
    Port ( reset 				: in 	std_logic;
			  clk 				: in 	std_logic;
			  sw 					: in	std_logic_vector(7 downto 0);
			  AUDIO_SDI 		: in  STD_LOGIC;
           AUDIO_BIT_CLK 	: in  STD_LOGIC;
           AUDIO_RESET 		: out STD_LOGIC;
           AUDIO_SDO 		: out STD_LOGIC;
           AUDIO_SYNC 		: out STD_LOGIC;
			  testigos			: out std_logic_vector(7 downto 0));
end prueba_ac97;

architecture Behavioral of prueba_ac97 is

signal datos_entrada,datos_salida 	: std_logic_vector(35 downto 0);
signal datos_listos 						: std_logic;
signal volumen_izq, volumen_der 		: std_logic_vector(3 downto 0);

component AC97
port
 (			  reset 			: in 	std_logic;
			  clock 			: in 	std_logic;
			  audsdi  		: in 	STD_LOGIC;				
           bitclk  		: in 	STD_LOGIC;				
			  data_in 		: out STD_LOGIC_VECTOR(35 DOWNTO 0);
			  data_out 		: in 	STD_LOGIC_VECTOR(35 DOWNTO 0);
			  data_ready 	: out	std_logic;
			  vol_ADC_izq	: in	std_logic_vector(3 downto 0);		
			  vol_ADC_der	: in	std_logic_vector(3 downto 0);		
           audrst  		: out STD_LOGIC;				
           audsdo 		: out STD_LOGIC;					
           audsync    	: out STD_LOGIC
 );
end component;

begin
controlador_AC97: AC97
  port map
   (	reset 		=> reset,
		clock 		=> clk,
		audsdi  		=> AUDIO_SDI,
		bitclk  		=> AUDIO_BIT_CLK,
	   data_in 		=> datos_entrada,
	   data_out		=> datos_salida,
	   data_ready	=> datos_listos,
		vol_ADC_izq	=> volumen_izq,
		vol_ADC_der	=> volumen_der,
		audrst		=> AUDIO_RESET,
		audsdo		=> AUDIO_SDO,
      audsync		=> AUDIO_SYNC);

process(datos_listos)
begin
if rising_edge(datos_listos) then
	case sw(1 downto 0) is
		when "00"=> 
			datos_salida<=datos_entrada;
		when "01"=> 
			datos_salida<=datos_entrada and X"ffffc0000";
		when "10"=> 
			datos_salida<=datos_entrada and X"00003ffff";
		when others=> 
			datos_salida<=X"000000000";
	end case;
end if;
end process;

testigos <= datos_entrada(35 downto 28);

volumen_izq<='1'&sw(7 downto 5);
volumen_der<='1'&sw(4 downto 2);
end Behavioral;
