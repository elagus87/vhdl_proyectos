library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
 
entity AC97 is
    Port ( 
			  reset 			:	in 	std_logic;
			  clock 			: 	in 	std_logic;
			  audsdi  		:	in   	STD_LOGIC;								--	Entrada del audio ADC
           bitclk  		: 	in  	STD_LOGIC;								--	Reloj del circuito del audio (12 MHz)
			  data_in 		:	out 	STD_LOGIC_VECTOR(35 DOWNTO 0);	-- Datos que llegan del ADC
			  data_out 		:	in	  	STD_LOGIC_VECTOR(35 DOWNTO 0);  	-- Datos que se enviaran al DAC
			  data_ready	:	out	std_logic;								-- Bandera de datos listos
			  vol_ADC_izq	:	in		std_logic_vector(3 downto 0);		-- Volumen del ADC izquierdo
			  vol_ADC_der	:	in		std_logic_vector(3 downto 0);		-- Volumen del ADC derecho
           audrst  		: 	out 	STD_LOGIC;								-- reset interno del CODEC
           audsdo 		:	out  	STD_LOGIC;								-- Salida de audio DAC
           audsync    	: 	out  	STD_LOGIC);								-- Sincrozador
end AC97;

architecture Arch of AC97 is

signal dato_entrada_left 	: std_logic_vector(17 downto 0);		--Dato de entrada canal izquierdo
signal dato_entrada_right 	: std_logic_vector(17 downto 0);	   --Dato de entrada canal derecho
signal dato_salida_left 	: std_logic_vector(17 downto 0);		--Dato de salida canal izquierdo
signal dato_salida_right 	: std_logic_vector(17 downto 0);	   --Dato de salida canal derecho
signal dato_listo				: std_logic:='0';

signal bit_count				: integer range 0 to 255;  			--Contador de cada uno de los elementos del frame
signal frame_count			: integer range 0 to 15;				--Contador de frames (tareas programada)
signal rst_count 				: integer range 0 to 1027;				--Permite aplicar un audrst al final de cada frame
signal control 				: STD_LOGIC_VECTOR (23 DOWNTO 0);	--Recibe la tarea a realizar
signal control_data 			: STD_LOGIC_VECTOR (19 DOWNTO 0);	--Contiene configuraciones
signal control_address 		: STD_LOGIC_VECTOR (19 DOWNTO 0);	--Contiene la direccion del registro

begin

	data_in<=dato_entrada_left&dato_entrada_right;			-- Se concatenan los dos canales para comunicacion con el exterior
	dato_salida_left<=data_out(35 downto 18);					-- Se dividen en dos canales la informacion que viene del mundo exterior
	dato_salida_right<=data_out(17 downto 0);
	data_ready<=dato_listo;											-- Se conecta la segnal a la bandera de datos listos

	control_address <= control(23 DOWNTO 16) & x"000";  	--Se asigna la parte del control que contiene los datos del registro a usar
	control_data <= control(15 DOWNTO 0) & x"0";				--Se asigna la parte del control que contiene la configuracion


--Proceso que controla los incrementos en una contador que va controlando un reset al final de la lectura de cada frame
	process(clock,reset)is							-- Se activa el proceso cunado hay un cambio en el reloj interno del circuito
	begin		
		if rising_edge(clock) then 				-- Solo cuando hay un flanco de subida
			if reset='0' then							-- valido para la ATLYS, reset activo bajo
				audrst<='0';
				rst_count<=0;
			elsif rst_count = 1027 then			-- Si ya se ha llegado al final del frame 
				audrst <= '1';							-- actival el reset
			else 
				rst_count <= rst_count + 1;		-- Si aun no llega continua incrementando el contador 
			end if;
		end if;
	end process;										--Fin del proceso que manipula el contador de elementos del frame


-- En este proceso se asignan los datos que contendran los primeros 5 slots de del frame.
-- Para esta pr�ctica solo son ocupados estos primeros.	
	process(bitclk) is
	begin
		if rising_edge(bitclk) then 				-- S�lo habra accesos a modificar variables en cada flanco de subida
		
			if bit_count = 255 then					-- Si se encuentra al final del frame, 
				audsync <= '1';						-- activa el sincronizador
				bit_count <= 0;						-- e inicializa el pivote, que indica en que posici�n del frame se encuentra
			end if;
			
			if frame_count = 15 then 				-- Para este programa solo se han colocado 5 rutinas a seguir, es decir la varible solo tiene 3 bits
				frame_count <= 0;						-- Solo se reinicia la varible
			end if;
			
			if bit_count = 15 then					-- el sincronizador solo esta activo en el tag, es por ello que al finalizar su lectura se deactiva
				audsync <= '0';
			end if;
					
			-- Asignacon de valores para cada Slot, es decir se configura
			--Slot 0 
			if (bit_count >= 0)and(bit_count <= 15) then	-- El tag se encuentra en las primeras 16 posiciones del frame
				case bit_count is
					when 0 => audsdo <= '1';					-- Indica que el frame es v�lido
					when 1 => audsdo <= '1';					-- Indica que el slot 1 (control address) es v�lido
					when 2 => audsdo <= '1';					-- indica qie el slot 2 (control data) es v�lido
					when 3 => audsdo <= '1';					-- Salida de audio izquierda valida
					when 4 => audsdo <= '1';					-- Salida de audio derecha valida
					when others	=> audsdo <= '0';				-- para cualquier otro caso las salidas son invalidas
				end case;
			
			--Slot 1: tag
			elsif (bit_count >= 16)and(bit_count <= 35)then		-- Se asigna a la salida el contenido del slot del control de direccion
				audsdo <= control_address(35-bit_count);			-- Son 20 bits
				
			--Slot 2: control address
			elsif (bit_count >= 36)and(bit_count <= 55) then	-- Se asigna a la salida el contenido del slot del control de dato
				audsdo <= control_data(55-bit_count);				-- Son 20 bits
								
			-- Slot 3: PCM audio data left
			elsif (bit_count >= 56)and(bit_count <= 73) then	-- Se asignan lo valores de entrada a las salidas izq
				audsdo<=dato_salida_left(73-bit_count);
				dato_listo<='0';
				
			elsif (bit_count >= 74)and(bit_count <= 75) then	-- Se mantiene la bandera baja hasta que se envie el canal derecho
				dato_listo<='0';
				
			-- Slot 4: PCM audio data right
			elsif (bit_count >= 76)and(bit_count <= 93) then	-- Se asignan lo valores de entrada a las salidas der
				audsdo<=dato_salida_right(93-bit_count);
				dato_listo<='0';
				
			-- Resto de slots
			else																-- Para cualquier otro valor mayor a 96 y hasta 255 que es el tama�o del frame
				audsdo <= '0';												-- se asignan ceros, ya que no son validados ya que no se estan ocupando
				dato_listo<='1';
			end if;
			
			if bit_count = 255 then 									-- Se lleva el conteo del n�mero de frames 
				frame_count <= frame_count + 1;
			end if;
			
			bit_count <= bit_count + 1;								-- Se lleva el conteo de la posici�n en el frame actual
			
		end if;
	end process;															
	
-- Entrada desde el Codec, tiene que ser en el flanco de bajada y las condicionales son retrasadas con respecto a la salida	
process(bitclk) is
begin
	if falling_edge(bitclk) then
		-- Slot 3: PCM audio data left
		if (bit_count >= 57)and(bit_count <= 74) then	
			dato_entrada_left<=dato_entrada_left(16 downto 0)&audsdi;		
		-- Slot 4: PCM audio data right
		elsif (bit_count >= 77)and(bit_count <= 94) then	
			dato_entrada_right<=dato_entrada_right(16 downto 0)&audsdi;			
		end if;
	end if;
end process;


-- Proceso en el que son asignadas las tareas, como activar el audio, asignar salidas izq y der, etc.
process (frame_count,vol_ADC_izq,vol_ADC_der) is 
	begin
			case frame_count is 
				when 1 =>
					control <= x"800000";		
				when 2 =>
					control <= x"800000";
				when 3 =>
					control <= x"020000";  										-- Activa salida line out x"020000" para que no haya atenuacion (solo atenuacion es posible)
				when 4 =>
					control <= x"180000";  										-- PCM out volume x"180000" para que haya ganancia de 12 dB al PCM
				when 5 =>
					control <= x"1A0404";  										-- Seleccion de entrada X"1A0404" es linea, X"1a0000" es mic		
				when 6 =>
					control <= x"1C0"&vol_ADC_izq&x"0"&vol_ADC_der;  	-- Record Gain, x"1C0000" para que sea 0 dB de ganancia en las entradas analogicas
				when 7 =>
					control <= x"2a0001";  										-- Activa Variable sample rate
				when 8 =>
					control <= x"2cbb80";  										-- Sample rate para DAC, 1F40 = 8Khz, 2B11 = 11.025KHz, 3E80 = 16KHz,											 
																						-- 5622 = 22.05KHz, AC44 = 44.1KHz, BB80 = 48KHz
				when 9 =>
					control <= x"32bb80";  										-- Sample rate para ADC
				when others =>
					control <= x"800000";		
			end case;
			
	end process;
end Arch;

