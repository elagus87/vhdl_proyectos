----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:33:30 09/28/2018 
-- Design Name: 
-- Module Name:    edge_detect - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;


entity edge_detect is
    Port ( sig : in  STD_LOGIC;
			  clk : in STD_LOGIC;
			  rst : in STD_LOGIC;
           tick : out  STD_LOGIC);
end edge_detect;

architecture Behavioral of edge_detect is

	type state_type is (s0,s1,s2);
	signal state_reg,state_next:state_type;
	
begin
	
registro_estados:process(sig,clk,rst)
begin
	if rst='1' then state_reg <= s0;
	elsif rising_edge(clk) then state_reg <= state_next;
	end if;
end process registro_estados;

estado_futuro:process(sig,clk,state_reg)

begin
	tick<='0';
	case state_reg is
		when s0 => if sig='1' then state_next<=s1;
					  else state_next<=s0;
					  end if;
		when s1 => tick<='1';
					  state_next<=s2;
		when s2 => if sig='0' then state_next<=s0;
					  else state_next<=s2;
					  end if;
	end case;			
end process estado_futuro;

end Behavioral;

