# Copyright (C) 1991-2013 Altera Corporation
# Your use of Altera Corporation's design tools, logic functions 
# and other software and tools, and its AMPP partner logic 
# functions, and any output files from any of the foregoing 
# (including device programming or simulation files), and any 
# associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License 
# Subscription Agreement, Altera MegaCore Function License 
# Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by 
# Altera or its authorized distributors.  Please refer to the 
# applicable agreement for further details.

# Quartus II 64-Bit Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition
# File: C:\Users\elagu\OneDrive\Documentos\vhdl_proyectos\CSK_i2sound_v2\CSK_i2sound.tcl
# Generated on: Sun Oct 07 23:18:12 2018

package require ::quartus::project

set_location_assignment PIN_L22 -to SW[0]
set_location_assignment PIN_L21 -to SW[1]
set_location_assignment PIN_M22 -to SW[2]
set_location_assignment PIN_V12 -to SW[3]
set_location_assignment PIN_W12 -to SW[4]
set_location_assignment PIN_U12 -to SW[5]
set_location_assignment PIN_U11 -to SW[6]
set_location_assignment PIN_M2 -to SW[7]
set_location_assignment PIN_M1 -to SW[8]
set_location_assignment PIN_L2 -to SW[9]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[0]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[1]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[2]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[3]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[4]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[5]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[6]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[7]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[8]
set_instance_assignment -name IO_STANDARD LVTTL -to SW[9]
set_location_assignment PIN_J2 -to HEX0[0]
set_location_assignment PIN_J1 -to HEX0[1]
set_location_assignment PIN_H2 -to HEX0[2]
set_location_assignment PIN_H1 -to HEX0[3]
set_location_assignment PIN_F2 -to HEX0[4]
set_location_assignment PIN_F1 -to HEX0[5]
set_location_assignment PIN_E2 -to HEX0[6]
set_location_assignment PIN_E1 -to HEX1[0]
set_location_assignment PIN_H6 -to HEX1[1]
set_location_assignment PIN_H5 -to HEX1[2]
set_location_assignment PIN_H4 -to HEX1[3]
set_location_assignment PIN_G3 -to HEX1[4]
set_location_assignment PIN_D2 -to HEX1[5]
set_location_assignment PIN_D1 -to HEX1[6]
set_location_assignment PIN_G5 -to HEX2[0]
set_location_assignment PIN_G6 -to HEX2[1]
set_location_assignment PIN_C2 -to HEX2[2]
set_location_assignment PIN_C1 -to HEX2[3]
set_location_assignment PIN_E3 -to HEX2[4]
set_location_assignment PIN_E4 -to HEX2[5]
set_location_assignment PIN_D3 -to HEX2[6]
set_location_assignment PIN_F4 -to HEX3[0]
set_location_assignment PIN_D5 -to HEX3[1]
set_location_assignment PIN_D6 -to HEX3[2]
set_location_assignment PIN_J4 -to HEX3[3]
set_location_assignment PIN_L8 -to HEX3[4]
set_location_assignment PIN_F3 -to HEX3[5]
set_location_assignment PIN_D4 -to HEX3[6]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX0[0]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX0[1]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX0[2]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX0[3]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX0[4]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX0[5]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX0[6]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX1[0]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX1[1]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX1[2]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX1[3]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX1[4]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX1[5]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX1[6]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX2[0]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX2[1]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX2[2]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX2[3]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX2[4]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX2[5]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX2[6]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX3[0]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX3[1]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX3[2]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX3[3]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX3[4]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX3[5]
set_instance_assignment -name IO_STANDARD LVTTL -to HEX3[6]
set_location_assignment PIN_R22 -to KEY[0]
set_location_assignment PIN_R21 -to KEY[1]
set_location_assignment PIN_T22 -to KEY[2]
set_location_assignment PIN_T21 -to KEY[3]
set_location_assignment PIN_R20 -to LEDR[0]
set_location_assignment PIN_R19 -to LEDR[1]
set_location_assignment PIN_U19 -to LEDR[2]
set_location_assignment PIN_Y19 -to LEDR[3]
set_location_assignment PIN_T18 -to LEDR[4]
set_location_assignment PIN_V19 -to LEDR[5]
set_location_assignment PIN_Y18 -to LEDR[6]
set_location_assignment PIN_U18 -to LEDR[7]
set_location_assignment PIN_R18 -to LEDR[8]
set_location_assignment PIN_R17 -to LEDR[9]
set_location_assignment PIN_U22 -to LEDG[0]
set_location_assignment PIN_U21 -to LEDG[1]
set_location_assignment PIN_V22 -to LEDG[2]
set_location_assignment PIN_V21 -to LEDG[3]
set_location_assignment PIN_W22 -to LEDG[4]
set_location_assignment PIN_W21 -to LEDG[5]
set_location_assignment PIN_Y22 -to LEDG[6]
set_location_assignment PIN_Y21 -to LEDG[7]
set_instance_assignment -name IO_STANDARD LVTTL -to KEY[0]
set_instance_assignment -name IO_STANDARD LVTTL -to KEY[1]
set_instance_assignment -name IO_STANDARD LVTTL -to KEY[2]
set_instance_assignment -name IO_STANDARD LVTTL -to KEY[3]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[0]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[1]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[2]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[3]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[4]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[5]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[6]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[7]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[8]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDR[9]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDG[0]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDG[1]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDG[2]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDG[3]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDG[4]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDG[5]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDG[6]
set_instance_assignment -name IO_STANDARD LVTTL -to LEDG[7]
set_location_assignment PIN_D12 -to CLOCK_27[0]
set_location_assignment PIN_E12 -to CLOCK_27[1]
set_location_assignment PIN_B12 -to CLOCK_24[0]
set_location_assignment PIN_A12 -to CLOCK_24[1]
set_location_assignment PIN_L1 -to CLOCK_50
set_location_assignment PIN_M21 -to EXT_CLOCK
set_instance_assignment -name IO_STANDARD LVTTL -to CLOCK_27[1]
set_instance_assignment -name IO_STANDARD LVTTL -to CLOCK_24[0]
set_instance_assignment -name IO_STANDARD LVTTL -to CLOCK_24[1]
set_instance_assignment -name IO_STANDARD LVTTL -to CLOCK_50
set_instance_assignment -name IO_STANDARD LVTTL -to EXT_CLOCK
set_location_assignment PIN_A3 -to I2C_SCLK
set_location_assignment PIN_B3 -to I2C_SDAT
set_location_assignment PIN_A6 -to AUD_ADCLRCK
set_location_assignment PIN_B6 -to AUD_ADCDAT
set_location_assignment PIN_A5 -to AUD_DACLRCK
set_location_assignment PIN_B5 -to AUD_DACDAT
set_location_assignment PIN_B4 -to AUD_XCK
set_location_assignment PIN_A4 -to AUD_BCLK
set_instance_assignment -name IO_STANDARD LVTTL -to I2C_SCLK
set_instance_assignment -name IO_STANDARD LVTTL -to I2C_SDAT
set_instance_assignment -name IO_STANDARD LVTTL -to AUD_ADCLRCK
set_instance_assignment -name IO_STANDARD LVTTL -to AUD_ADCDAT
set_instance_assignment -name IO_STANDARD LVTTL -to AUD_DACLRCK
set_instance_assignment -name IO_STANDARD LVTTL -to AUD_DACDAT
set_instance_assignment -name IO_STANDARD LVTTL -to AUD_XCK
set_instance_assignment -name IO_STANDARD LVTTL -to AUD_BCLK
set_location_assignment PIN_AA3 -to SRAM_ADDR[0]
set_location_assignment PIN_AB3 -to SRAM_ADDR[1]
set_location_assignment PIN_AA4 -to SRAM_ADDR[2]
set_location_assignment PIN_AB4 -to SRAM_ADDR[3]
set_location_assignment PIN_AA5 -to SRAM_ADDR[4]
set_location_assignment PIN_AB10 -to SRAM_ADDR[5]
set_location_assignment PIN_AA11 -to SRAM_ADDR[6]
set_location_assignment PIN_AB11 -to SRAM_ADDR[7]
set_location_assignment PIN_V11 -to SRAM_ADDR[8]
set_location_assignment PIN_W11 -to SRAM_ADDR[9]
set_location_assignment PIN_R11 -to SRAM_ADDR[10]
set_location_assignment PIN_T11 -to SRAM_ADDR[11]
set_location_assignment PIN_Y10 -to SRAM_ADDR[12]
set_location_assignment PIN_U10 -to SRAM_ADDR[13]
set_location_assignment PIN_R10 -to SRAM_ADDR[14]
set_location_assignment PIN_T7 -to SRAM_ADDR[15]
set_location_assignment PIN_Y6 -to SRAM_ADDR[16]
set_location_assignment PIN_Y5 -to SRAM_ADDR[17]
set_location_assignment PIN_AB5 -to SRAM_CE_N
set_location_assignment PIN_AA6 -to SRAM_DQ[0]
set_location_assignment PIN_AB6 -to SRAM_DQ[1]
set_location_assignment PIN_AA7 -to SRAM_DQ[2]
set_location_assignment PIN_AB7 -to SRAM_DQ[3]
set_location_assignment PIN_AA8 -to SRAM_DQ[4]
set_location_assignment PIN_AB8 -to SRAM_DQ[5]
set_location_assignment PIN_AA9 -to SRAM_DQ[6]
set_location_assignment PIN_AB9 -to SRAM_DQ[7]
set_location_assignment PIN_Y9 -to SRAM_DQ[8]
set_location_assignment PIN_W9 -to SRAM_DQ[9]
set_location_assignment PIN_V9 -to SRAM_DQ[10]
set_location_assignment PIN_U9 -to SRAM_DQ[11]
set_location_assignment PIN_R9 -to SRAM_DQ[12]
set_location_assignment PIN_W8 -to SRAM_DQ[13]
set_location_assignment PIN_V8 -to SRAM_DQ[14]
set_location_assignment PIN_U8 -to SRAM_DQ[15]
set_location_assignment PIN_Y7 -to SRAM_LB_N
set_location_assignment PIN_T8 -to SRAM_OE_N
set_location_assignment PIN_W7 -to SRAM_UB_N
set_location_assignment PIN_AA10 -to SRAM_WE_N
