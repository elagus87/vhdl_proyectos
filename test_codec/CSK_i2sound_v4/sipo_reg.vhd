library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sipo_reg is
	generic(W:natural :=64);
	port(
			clk,rst,enable,serial_in:in std_logic;
			paralel_out:out std_logic_vector(W-1 downto 0)
		 );
end entity sipo_reg;

architecture behavioral of sipo_reg is 
	signal s_reg,s_next: std_logic_vector(W-1 downto 0);
begin
	reg_estado:process(clk,rst)
	begin
		if rst='1' then s_reg<=(others=>'0');
		elsif rising_edge(clk) then
			s_reg<=s_next;
		end if;
	end process reg_estado;
	
-- logica de estado futuro
	s_next <= s_reg(W-2 downto 0) & serial_in when enable='1' else s_reg;
	
-- logica de salida

	paralel_out <= s_reg;
end architecture behavioral;