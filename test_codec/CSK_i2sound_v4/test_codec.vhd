
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY test_codec IS
END test_codec;
 
ARCHITECTURE behavior OF test_codec IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT i2sound
    PORT(
         CLOCK_24 : IN  std_logic_vector(1 downto 0);
         CLOCK_27 : IN  std_logic_vector(1 downto 0);
         CLOCK_50 : IN  std_logic;
         EXT_CLOCK : IN  std_logic;
         KEY : IN  std_logic_vector(3 downto 0);
         SW : IN  std_logic_vector(9 downto 0);
         HEX0 : OUT  std_logic_vector(6 downto 0);
         HEX1 : OUT  std_logic_vector(6 downto 0);
         HEX2 : OUT  std_logic_vector(6 downto 0);
         HEX3 : OUT  std_logic_vector(6 downto 0);
         LEDG : OUT  std_logic_vector(7 downto 0);
         LEDR : OUT  std_logic_vector(9 downto 0);
         I2C_SDAT : INOUT  std_logic;
         I2C_SCLK : OUT  std_logic;
         AUD_ADCLRCK : INOUT  std_logic;
         AUD_ADCDAT : IN  std_logic;
         AUD_DACLRCK : INOUT  std_logic;
         AUD_DACDAT : OUT  std_logic;
         AUD_BCLK : INOUT  std_logic;
         AUD_XCK : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal CLOCK_24_test : std_logic_vector(1 downto 0) := (others => '0');
   signal CLOCK_27_test : std_logic_vector(1 downto 0) := (others => '0');
   signal CLOCK_50_test : std_logic := '0';
   signal EXT_CLOCK_test : std_logic := '0';
   signal KEY_test : std_logic_vector(3 downto 0) := (others => '0');
   signal SW_test : std_logic_vector(9 downto 0) := (others => '0');
   signal AUD_ADCDAT_test : std_logic := '0';

	--BiDirs
   signal I2C_SDAT_test : std_logic;
   signal AUD_ADCLRCK_test : std_logic;
   signal AUD_DACLRCK_test : std_logic;
   signal AUD_BCLK_test : std_logic;

 	--Outputs
   signal HEX0_test : std_logic_vector(6 downto 0);
   signal HEX1_test : std_logic_vector(6 downto 0);
   signal HEX2_test : std_logic_vector(6 downto 0);
   signal HEX3_test : std_logic_vector(6 downto 0);
   signal LEDG_test : std_logic_vector(7 downto 0);
   signal LEDR_test : std_logic_vector(9 downto 0);
   signal I2C_SCLK_test : std_logic;
   signal AUD_DACDAT_test : std_logic;
   signal AUD_XCK_test : std_logic;

   -- Clock period definitions
   constant CLOCK_24_period : time := 41.666 ns;
   constant CLOCK_27_period : time := 37.037 ns;
   constant CLOCK_50_period : time := 20 ps;
   constant AUD_BCLK_period : time := 325.521 ns;
   constant AUD_ADCLRCK_period: time := 20.83 us; 
   constant AUD_DACLRCK_period: time := 20.83 us;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: i2sound PORT MAP (
          CLOCK_24 => CLOCK_24_test,
          CLOCK_27 => CLOCK_27_test,
          CLOCK_50 => CLOCK_50_test,
          EXT_CLOCK => EXT_CLOCK_test,
          KEY => KEY_test,
          SW => SW_test,
          HEX0 => HEX0_test,
          HEX1 => HEX1_test,
          HEX2 => HEX2_test,
          HEX3 => HEX3_test,
          LEDG => LEDG_test,
          LEDR => LEDR_test,
          I2C_SDAT => I2C_SDAT_test,
          I2C_SCLK => I2C_SCLK_test,
          AUD_ADCLRCK => AUD_ADCLRCK_test,
          AUD_ADCDAT => AUD_ADCDAT_test,
          AUD_DACLRCK => AUD_DACLRCK_test,
          AUD_DACDAT => AUD_DACDAT_test,
          AUD_BCLK => AUD_BCLK_test,
          AUD_XCK => AUD_XCK_test
        );

   -- Clock process definitions
   CLOCK_24_process :process
   begin
		CLOCK_24_test <= "00";
		wait for CLOCK_24_period/2;
		CLOCK_24_test <= "11";
		wait for CLOCK_24_period/2;
   end process;
 
   CLOCK_27_process :process
   begin
		CLOCK_27_test <= "00";
		wait for CLOCK_27_period/2;
		CLOCK_27_test <= "11";
		wait for CLOCK_27_period/2;
   end process;
 
   CLOCK_50_process :process
   begin
		CLOCK_50_test <= '0';
		wait for CLOCK_50_period/2;
		CLOCK_50_test <= '1';
		wait for CLOCK_50_period/2;
   end process;
 
 
   AUD_BCLK_process :process
   begin
		AUD_BCLK_test <= '0';
		wait for AUD_BCLK_period/2;
		AUD_BCLK_test <= '1';
		wait for AUD_BCLK_period/2;
   end process;
  
   AUD_DACLRCK_process :process
   begin
    AUD_DACLRCK_test <= '0';
    wait for AUD_DACLRCK_period/2;
    AUD_DACLRCK_test <= '1';
    wait for AUD_DACLRCK_period/2;
   end process;

   AUD_ADCLRCK_process :process
   begin
    AUD_ADCLRCK_test <= '0';
    wait for AUD_ADCLRCK_period/2;
    AUD_ADCLRCK_test <= '1';
    wait for AUD_ADCLRCK_period/2;
   end process;


   -- Stimulus process
   stim_proc: process
   begin		
      KEY_test(3)<='0';
      wait for 100 ns;
      KEY_test(3)<='1';

      if rising_edge(AUD_ADCLRCK_test) then 
        AUD_ADCDAT_test<='1';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='1';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='1';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='1';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='0';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='0';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='0';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='0';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='1';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='1';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='1';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='1';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='0';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='0';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='0';
        wait for AUD_BCLK_period;
        AUD_ADCDAT_test<='0';
        wait for AUD_BCLK_period;
      end if;
   end process;

END;
