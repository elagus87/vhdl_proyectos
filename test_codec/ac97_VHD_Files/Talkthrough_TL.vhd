--//////////Top Level for AC97 Talkthrough /////////////////////////////--
-- ***********************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.Spar6_Parts.all;			-- include your library here with added components ac97, ac97cmd


-- THESE SIGNALS NEED TO BE MAPPED IN A UCF FILE TO ROUTE THEM EXTERNALLY
-- THIS CAN BE DONE MANUALLY OR VIA PLAN AHEAD OR A SIMILAR PIN MAPPING GUI 
entity Spar6_Talkthrough_TL is
    Port ( clk : in  STD_LOGIC;
           n_reset : in  STD_LOGIC;
			  SDATA_IN : in STD_LOGIC;
			  BIT_CLK : in STD_LOGIC;
			  SOURCE : in STD_LOGIC_VECTOR(2 downto 0);
			  VOLUME : in STD_LOGIC_VECTOR(4 downto 0);
			  SYNC : out STD_LOGIC;
			  SDATA_OUT : out STD_LOGIC;
			  AC97_n_RESET : out STD_LOGIC;
--			  test_dac : out STD_LOGIC_VECTOR(17 downto 0);
--			  test_sig : out STD_LOGIC_VECTOR(17 downto 0);
			  vel_up: in  STD_LOGIC;
			  vel_dw: in  STD_LOGIC
			  );
end Spar6_Talkthrough_TL;

-----------------------------------------------------------------
--						Inicio de la arquitectura							--
-----------------------------------------------------------------

architecture arch of Spar6_Talkthrough_TL is


	signal L_bus, R_bus, L_bus_out, R_bus_out : std_logic_vector(17 downto 0);	
	signal cmd_addr : std_logic_vector(7 downto 0);
	signal cmd_data : std_logic_vector(15 downto 0);
	signal ready : std_logic;
	signal latching_cmd : std_logic;

	signal 	   vel : std_logic_vector(31 downto 0);
	signal 	 phase : std_logic_vector(31 downto 0);
	signal 	  addr : std_logic_vector(9 downto 0); --la rom es de 16 bits
	signal dac_next : std_logic_vector(15 downto 0); --el DAC es de 18 bits
	signal  vel_reg : unsigned(31 downto 0);
	signal vel_next : unsigned(31 downto 0);
	signal 		dac : STD_LOGIC_VECTOR (17 downto 0);
	signal 	 audio : signed(17 downto 0);
	-- asigna velocidad angular para 1 KHz
	-- 32 bits de fase
	-- addr de la rom 10 bits
	-- 32 - 10 bits = 22 bits
	-- 2^10 = 1024 ciclos de reloj
	-- fclk = 50 MHz
	-- Tclk = 20 ns
	-- Tmin = 1024 x 20ns x 1024 = 0.02097152s
	-- N = 0.02097152s / 1ms = 21
begin
-----------------------------------------------------------------	
	--instancias de los componentes
	dds:entity work.dds
		port map(arst	=> n_reset,
					clk 	=> clk,
					vel 	=> vel,
					phase => phase);
					
	rom:entity work.rom
		port map(addres	=> addr,
					data 	=> dac_next);
-----------------------------------------------------------------					
	--indexacion de la tabla a traves de los MSB de phase
	addr <= phase(31 downto 22);
	
	registro_de_salida:process(clk,n_reset)
		begin
		if (n_reset = '1') then
			dac <= (others=>'0');
			vel_reg<="00000000000000000000000000010101"; -- arranca en 1kHz
		elsif(rising_edge(clk)) then
			dac <= dac_next & "00";
			vel_reg <= vel_next;
		end if;
	end process;
-----------------------------------------------------------------	
	--cambios de frecuencia
	vel_next <= (vel_reg + 1) when vel_up='1' else
					(vel_reg - 1) when vel_dw='1' else
					vel_reg;
	vel <= std_logic_vector(vel_reg);

---------------------------------------------------------------------------
-- 	Maquinas de estados para ambos drivers de configuración del ac97	 --	
---------------------------------------------------------------------------
	ac97_cont0 : entity work.ac97(arch)
		port map(
						n_reset => n_reset,
						clk => clk,
						ac97_sdata_out => SDATA_OUT,
						ac97_sdata_in => SDATA_IN,
						latching_cmd => latching_cmd ,
						ac97_sync => SYNC,
						ac97_bitclk => BIT_CLK,
						ac97_n_reset => AC97_n_RESET,
						ac97_ready_sig => ready,
						L_out => L_bus,
						R_out => R_bus,
						L_in => L_bus_out,
						R_in => R_bus_out,
						cmd_addr => cmd_addr,
						cmd_data => cmd_data
					);
 
   ac97cmd_cont0 : entity work.ac97cmd(arch)
		port map (
						clk => clk,
						ac97_ready_sig => ready,
						cmd_addr => cmd_addr,
						cmd_data => cmd_data,
						volume => VOLUME, 
						source => SOURCE,
						latching_cmd => latching_cmd
					);  
	 

--------------------------------------------------------------------
--		loopback de audio a ser reemplazado por el procesamiento		-- 
--------------------------------------------------------------------
	process ( clk, n_reset, L_bus_out, R_bus_out,dac)
  
	begin
		audio <=signed(R_bus_out);
		if (clk'event and clk = '1') then
			if n_reset = '0' then
				L_bus <= (others => '0');
				R_bus <= (others => '0');
			elsif(ready = '1') then
				L_bus <= L_bus_out;
				R_bus <= std_logic_vector(audio);
			end if;
		end if;
	end process;
--	test_dac<=dac;
--	test_sig<=L_bus_out;
end arch;


