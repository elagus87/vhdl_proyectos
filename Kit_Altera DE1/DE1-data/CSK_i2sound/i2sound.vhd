----------------------------------------------------------------------------------
-- University: 	 UTN-FRBA
-- Author:			 Equipo docente 
-- 
-- Create Date:    08:35:52 09/24/2018 
-- Design Name: 
-- Module Name:    i2sound - Behavioral 
-- Project Name:   tpo - Tecnicas Digitales 1
-- Target Devices: Altera/Intel DE1 - EP2C20F484C7
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;



entity i2sound is
	port(
				CLOCK_24:in std_logic_vector(1 downto 0); --24 MHz
				CLOCK_27:in std_logic_vector(1 downto 0); --27 MHz
				CLOCK_50:in std_logic;							--50 MHz
				EXT_CLOCK:in std_logic;							--clock externo
				
				KEY:in std_logic_vector(3 downto 0);		--pulsadores
				
				SW:in std_logic_vector(9 downto 0);			--tira de switches
				
				HEX0:out std_logic_vector(6 downto 0);		--7 segmentos n�0
				HEX1:out std_logic_vector(6 downto 0);		--7 segmentos n�1
				HEX2:out std_logic_vector(6 downto 0);		--7 segmentos n�2
				HEX3:out std_logic_vector(6 downto 0);		--7 segmentos n�3
				
				LEDG:out std_logic_vector(7 downto 0);		--tira de leds verdes
				LEDR:out std_logic_vector(9 downto 0);		--tira de leds rojos
				
				I2C_SDAT:inout std_logic;						--datos i2c 
				I2C_SCLK:out std_logic;							--reloj de comunicacion
				
				AUD_ADCLRCK:inout std_logic;					--clock de canales LR del adc
				AUD_ADCDAT:inout std_logic;					--datos ADC
				AUD_DACLRCK:inout std_logic;					--clock de canales LR del dac
				AUD_DACDAT:out std_logic;						--datos DAC
				AUD_BCLK:inout std_logic;						--clock de flujo de bits
				AUD_XCK:out std_logic							--clock del chip
		 );	
		 
end i2sound;

architecture Behavioral of i2sound is
	signal VOL: unsigned(6 downto 0);
	signal CLK_18_4:std_logic;
begin
	HEX0		<=	"0001000";
	HEX1		<=	"1000111";
	HEX2		<=	"1000000";
	HEX3		<=	"0001001";
	LEDG		<=	std_logic_vector(VOL);
	LEDR		<=	x"400";
	
	I2C_SDAT <= 'Z'; 				--seteo en alta impedancia
	AUD_DACDAT	<=	AUD_ADCDAT; --llegue como llegue del adc lo manda al dac, solia decir AUD_DACLRCK
	
	
	component PLL port(
									inclk0: in std_logic;
									c0:out std_logic
								); end component;
	pll_conf: PLL port map(
						CLOCK_27(0) => inclk0,
						CLK_18_4 => c0
					 );
					 
	AUD_XCK		<=	CLK_18_4;

					 
--	i2c_audio_config: entity work.I2C_AV_Config(data_flow)
--		   port map(
--						 CLOCK_50 => iCLK,
--						 KEY(0) => iRST_N,
--						 VOL => iVOL,
--						 I2C_SCLK => I2C_SCLK,
--						 I2C_SDAT => I2C_SDAT
					  );
					  
	volumen:process( KEY(0) )
	begin 
		if VOL<68 then VOL <= 98;
		else VOL	<=	VOL+3;
		end if;
	end process;
	
end Behavioral;

