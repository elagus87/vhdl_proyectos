----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:07:38 10/03/2017 
-- Design Name: 
-- Module Name:    contador_ce_pe - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;



entity contador_ce_pe is
    Port ( a_rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           ce : in  STD_LOGIC;
           pe : in  STD_LOGIC;
           load : in  STD_LOGIC_VECTOR (3 downto 0);
           count_out : out  STD_LOGIC_VECTOR (3 downto 0));
end contador_ce_pe;

architecture Behavioral of contador_ce_pe is
	signal cuenta_reg,cuenta_next:unsigned(3 downto 0);
begin
	
	contador:process(clk,a_rst)
	begin
		if (a_rst='1') then cuenta_reg<="0000";
		elsif rising_edge(clk) then
			cuenta_reg<=cuenta_next;
		end if;			
	end process contador;
	
	--estado futuro
	cuenta_next<=(cuenta_reg+1)  when ce='1' else
					  unsigned(load)when pe='1' else
					  cuenta_reg;

	--salida
	count_out<=std_logic_vector(cuenta_reg);

end Behavioral;

