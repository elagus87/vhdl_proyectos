
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY contadot_tb IS
END contadot_tb;
 
ARCHITECTURE behavior OF contadot_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT contador
    PORT(
         frec : IN  std_logic;
         load : IN  std_logic_vector(3 downto 0);
         pe : IN  std_logic;
         count : IN  std_logic;
         a_rst : IN  std_logic;
         clk : IN  std_logic;

         leds : OUT  std_logic_vector(3 downto 0);
         segmentos0 : OUT  std_logic_vector(6 downto 0);
         segmentos1 : OUT  std_logic_vector(6 downto 0);
         segmentos2 : OUT  std_logic_vector(6 downto 0);
         segmentos3 : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal frec_t : std_logic := '0';
   signal load_t : std_logic_vector(3 downto 0) := (others => '0');
   signal pe_t : std_logic := '0';
   signal count_t : std_logic := '0';
   signal a_rst_t : std_logic := '0';
   signal clk_t : std_logic := '0';

 	--Outputs

   signal leds_t : std_logic_vector(3 downto 0);
   signal segmentos0_t : std_logic_vector(6 downto 0);
   signal segmentos1_t : std_logic_vector(6 downto 0);
   signal segmentos2_t : std_logic_vector(6 downto 0);
   signal segmentos3_t : std_logic_vector(6 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: contador PORT MAP (
          frec => frec_t,
          load => load_t,
          pe => pe_t,
          count => count_t,
          a_rst => a_rst_t,
          clk => clk_t,
          leds => leds_t,
          segmentos0 => segmentos0_t,
          segmentos1 => segmentos1_t,
          segmentos2 => segmentos2_t,
          segmentos3 => segmentos3_t
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk_t <= '0';
		wait for clk_period/2;
		clk_t <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin
		a_rst_t<='1';
		wait for 20 ns;	
		
		a_rst_t<='0';
		frec_t<='0';
		pe_t<='1';
		load_t<="1000";
		count_t<='1';		
      wait for 20 ns;	
		pe_t<='0';
		wait for clk_period*10;
		frec_t<='1';
		wait for clk_period*20;
		count_t<='1';		
      wait for 50 ns;
		count_t<='0';		
      wait for 10 ns;
		count_t<='1';		
      wait for 10 ns;
		count_t<='0';	
		wait for 50 ns;
		count_t<='1';	
		wait for 40 ms;
		
      assert false
		report "fin"
		severity failure;
		
   end process;

END;
