	
	# Clock source
	set_location_assignment PIN_L1 -to clk
	#-----------------------------------------------#
	
	# Slide switches
	set_location_assignment PIN_V12 -to medio_uno
	set_location_assignment PIN_M22 -to pl
	set_location_assignment PIN_L21 -to ce
	set_location_assignment PIN_L22 -to a_rst
	
	set_location_assignment PIN_L2 -to carga[3]
	set_location_assignment PIN_M1 -to carga[2]
	set_location_assignment PIN_M2 -to carga[1]
	set_location_assignment PIN_U11 -to carga[0]
	#-----------------------------------------------#
	
	# Display 0
	set_location_assignment PIN_E2 -to segmentos0[6]
	set_location_assignment PIN_F1 -to segmentos0[5]
	set_location_assignment PIN_F2 -to segmentos0[4]
	set_location_assignment PIN_H1 -to segmentos0[3]
	set_location_assignment PIN_H2 -to segmentos0[2]
	set_location_assignment PIN_J1 -to segmentos0[1]
	set_location_assignment PIN_J2 -to segmentos0[0]
	
	# Display 1
	set_location_assignment PIN_D1 -to segmentos1[6]
	set_location_assignment PIN_D2 -to segmentos1[5]
	set_location_assignment PIN_G3 -to segmentos1[4]
	set_location_assignment PIN_H4 -to segmentos1[3]
	set_location_assignment PIN_H5 -to segmentos1[2]
	set_location_assignment PIN_H6 -to segmentos1[1]
	set_location_assignment PIN_E1 -to segmentos1[0]
	
	# Display 2
	set_location_assignment PIN_D3 -to segmentos2[6]
	set_location_assignment PIN_E4 -to segmentos2[5]
	set_location_assignment PIN_E3 -to segmentos2[4]
	set_location_assignment PIN_C1 -to segmentos2[3]
	set_location_assignment PIN_C2 -to segmentos2[2]
	set_location_assignment PIN_G6 -to segmentos2[1]
	set_location_assignment PIN_G5 -to segmentos2[0]
	
	# Display 3
	set_location_assignment PIN_D4 -to segmentos3[6]
	set_location_assignment PIN_F3 -to segmentos3[5]
	set_location_assignment PIN_L8 -to segmentos3[4]
	set_location_assignment PIN_J4 -to segmentos3[3]
	set_location_assignment PIN_D6 -to segmentos3[2]
	set_location_assignment PIN_D5 -to segmentos3[1]
	set_location_assignment PIN_F4 -to segmentos3[0]
	
	#-----------------------------------------------#