
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity contador is
    Port ( frec : in  STD_LOGIC;								       --elige cuenta de a medio o un seg
			  load : in  STD_LOGIC_VECTOR (3 downto 0);			 --carga paralela
			  pe : in  STD_LOGIC;										 --hab carga paralela
			  count : in  STD_LOGIC;									 --hab contador
			  a_rst : in  STD_LOGIC;									 --rst asincrónico
			  clk : in  STD_LOGIC;
                 
			  leds : out  STD_LOGIC_VECTOR (3 downto 0);			 --auxiliar para corroborar la cuenta
			  segmentos0 : out  STD_LOGIC_VECTOR (6 downto 0);  --a solo efectos de apagar el resto
			  segmentos1 : out  STD_LOGIC_VECTOR (6 downto 0);
			  segmentos2 : out  STD_LOGIC_VECTOR (6 downto 0);
           segmentos3 : out  STD_LOGIC_VECTOR (6 downto 0));
end contador;

architecture estructural of contador is
	signal divisor:std_logic_vector(24 downto 0);
	signal clk_en:std_logic;
	signal push_db:std_logic;
	signal habilitar:std_logic;
	signal cuenta:std_logic_vector(3 downto 0);
	signal contador_ce:std_logic;
begin
	
	divisor<=std_logic_vector(to_unsigned(4,25)) when frec='1' else
				std_logic_vector(to_unsigned(2,25)) when frec='0';
	
	-- contador_ce<=(count and clk_en);		-- comentar para parte 2
	 contador_ce<=habilitar;
	-- En la parte 1 se comentan las siguientes 2 instancias dado que count es una llave
	debouncer:entity work.debounce
					port map(	
						clk=>clk,
						reset=>a_rst,
						b=>count,		--boton
						db=>push_db
					);
	tick_gen:entity work.tick_gen
					port map(	
						clk=>clk,
						reset=>a_rst,
						p=>push_db,		--boton
						ceo=>habilitar
					);

	clk_divider:entity work.clockS
					port map(	
						clk=>clk,
						a_rst=>a_rst,
						div=>divisor,
						clk_div=>clk_en
					);
	segmentos:entity work.conv_bin_7seg
					port map(	
						bin=>std_logic_vector(cuenta),
						sseg0_L=>segmentos0,
						sseg1_L=>segmentos1,
						sseg2_L=>segmentos2,
						sseg3_L=>segmentos3
					);
					
	contador:entity work.contador_ce_pe
					port map(
						a_rst => a_rst,
						clk => clk,
						ce => contador_ce, 		--dependiente de la llave y el divisor de clk
						pe => pe,
						load => load,
						count_out => cuenta		--salida
					);
					
	leds<=cuenta;
end estructural;

