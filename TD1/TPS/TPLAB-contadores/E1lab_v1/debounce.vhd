	library ieee;
	use ieee.std_logic_1164.all;
	use ieee.numeric_std.all;

	entity debounce is
		port (clk, reset: in std_logic;
					b: in std_logic;
					db: out std_logic);
	end entity debounce;

	architecture behavioral of debounce is
		constant N: integer:= 19; -- 2^N * 20ns = 10ms tick
		signal q_reg, q_next: unsigned(N-1 downto 0);
		signal tc: std_logic;
		type state_type is (zero, wait1_1, wait1_2, wait1_3,
											one, wait0_1, wait0_2, wait0_3);
		signal s_reg, s_next: state_type;
	begin
		--************************
		-- counter to generate 10 ms tick
		-- (2^19 * 20ns)
		--************************
		process(clk, reset)
		begin
			if reset= '1' then 
				q_reg <= (others => '0');
			elsif rising_edge(clk) then
				q_reg <= q_next;
			end if;
		end process;
		
		-- next-state logic
		q_next <= q_reg + 1;
		
		--output tick
		tc <= '1' when q_reg=0 else '0';
		
		--*************
		-- debouncing fsm
		--*************
		-- state register
		process(clk,reset)
		begin
			if (reset='1') then
				s_reg <= zero;
			elsif rising_edge(clk) then
				s_reg <= s_next;
			end if;
		end process;
		
		-- next-state/output logic
		process(s_reg, b, tc)
		begin
			s_next <= s_reg; -- default: back to same state
			db <= '0';   -- default 0
			case s_reg is
				when zero =>
					if b='1' then
						s_next <= wait1_1;
					end if;
					
				when wait1_1 =>
					if b='0' then
						s_next <= zero;
					else
						if tc='1' then
							s_next <= wait1_2;
						end if;
					end if;
					
				when wait1_2 =>
					if b='0' then
						s_next <= zero;
					else
						if tc='1' then
							s_next <= wait1_3;
						end if;
					end if;
					
				when wait1_3 =>
					if b='0' then
						s_next <= zero;
					else
						if tc='1' then
							s_next <= one;
						end if;
					end if;
					
				when one =>
					db <='1';
					if b='0' then
						s_next <= wait0_1;
					end if;
					
				when wait0_1 =>
					db <='1';
					if b='1' then
						s_next <= one;
					else
						if tc='1' then
							s_next <= wait0_2;
						end if;
					end if;
					
				when wait0_2 =>
					db <='1';
					if b='1' then
						s_next <= one;
					else
						if tc='1' then
							s_next <= wait0_3;
						end if;
					end if;
					
				when wait0_3 =>
					db <='1';
					if b='1' then
						s_next <= one;
					else
						if tc='1' then
							s_next <= zero;
						end if;
					end if;
			end case;
		end process;
		
	end architecture behavioral;

	