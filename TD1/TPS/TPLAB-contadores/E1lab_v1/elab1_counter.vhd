
	library ieee;
	use ieee.std_logic_1164.all;

	entity elab1_counter is
		 port (clk, reset, btn : in  std_logic;
					led0, led1, led2, led3 : out  std_logic);
	end elab1_counter;

	architecture struct of elab1_counter is
	signal link: std_logic; 
	begin

	deb: entity work.debounce
					port map(b => btn, reset => reset, clk => clk, db => link);


	fsm1: entity work.elab1_fsm1
					port map(p => link, reset => reset, clk => clk, 
										o(0) => led0, o(1) => led1, o(2) => led2, o(3) => led3);
	end architecture struct;
