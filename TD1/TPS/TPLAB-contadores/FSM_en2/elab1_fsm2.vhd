	library ieee;
	use ieee.std_logic_1164.all;

	entity elab1_fsm2 is
		 port (clk, reset, p : in  std_logic;
					ceo : out  std_logic);
	end entity elab1_fsm2;

	architecture behavioral of elab1_fsm2 is
	type state_type is (s0, s1, s2);
	signal s_reg, s_next: state_type;
	begin

	state_reg:
	process(clk, reset)
	begin
		if reset= '1' then
			s_reg <= s0;
		elsif rising_edge(clk) then
			s_reg <= s_next;
		end if;
	end process state_reg;

	next_state_logic:
	process(s_reg, p)
	begin
		case s_reg is
			when s0 =>
				if p= '1' then 	s_next <= s1;
				else 					s_next <= s0;
				end if;
			when s1 =>
				if p= '1' then 	s_next <= s2;
				else 					s_next <= s0;
				end if;
			when s2 =>
				if p= '1' then 	s_next <= s2;
				else 					s_next <= s0;
				end if;
		end case;
	end process next_state_logic;
				
	-- output logic

			ceo <= '1' when s_reg= s1 else '0';

	end architecture behavioral;

