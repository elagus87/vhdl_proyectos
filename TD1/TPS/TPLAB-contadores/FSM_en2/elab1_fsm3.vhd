	library ieee;
	use ieee.std_logic_1164.all;

	entity elab1_fsm3 is
		 port (clk, reset, ce : in  std_logic;
					o : out  std_logic_vector(3 downto 0));
	end entity elab1_fsm3;

	architecture behavioral of elab1_fsm3 is
	type state_type is (s0, s1, s2, s3);
	signal s_reg, s_next: state_type;
	begin

	state_reg:
	process(clk, reset)
	begin
		if reset= '1' then
			s_reg <= s0;
		elsif rising_edge(clk) then
			s_reg <= s_next;
		end if;
	end process state_reg;

	next_state_logic:
	process(s_reg, ce)
	begin
		case s_reg is
			when s0 =>
				if ce= '1' then 	s_next <= s1;
				else 						s_next <= s0;
				end if;
			when s1 =>
				if ce= '1' then 	s_next <= s2;
				else 						s_next <= s1;
				end if;
			when s2 =>
				if ce= '1' then 	s_next <= s3;
				else 						s_next <= s2;
				end if;
			when s3 =>
				if ce= '1' then 	s_next <= s0;
				else 						s_next <= s3;
				end if;
		end case;
	end process next_state_logic;
				
	-- output logic

		with s_reg select
			o <= "0001" when s0,
						"0010" when s1,
						"0100" when s2,
						"1000" when s3;
	end architecture behavioral;

