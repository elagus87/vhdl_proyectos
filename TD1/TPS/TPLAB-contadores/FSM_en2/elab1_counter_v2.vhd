
	library ieee;
	use ieee.std_logic_1164.all;

	entity elab1_counter_v2 is
		 port (clk, reset, btn : in  std_logic;
					led0, led1, led2, led3 : out  std_logic);
	end elab1_counter_v2;

	architecture struct of elab1_counter_v2 is
	signal link1, link2: std_logic; 
	begin

	deb: entity work.debounce
					port map(b => btn, reset => reset, clk => clk, db => link1);


	fsm2: entity work.elab1_fsm2
					port map(p => link1, reset => reset, clk => clk, 
										ceo => link2);
	
	fsm3: entity work.elab1_fsm3
					port map(ce => link2, reset => reset, clk => clk, 
										o(0) => led0, o(1) => led1, o(2) => led2, o(3) => led3);
										
	end architecture struct;
	