--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:08:54 09/28/2017
-- Design Name:   
-- Module Name:   C:/Users/elagu/OneDrive/Documentos/vhdl_proyectos/TPLAB2/divisor_clk/divisor_clk/clockS_tb.vhd
-- Project Name:  divisor_clk
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: clockS
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL; 
 
ENTITY clockS_tb IS
END clockS_tb;
 
ARCHITECTURE behavior OF clockS_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT clockS
    PORT(
         clk : IN  std_logic;
         div : IN  STD_LOGIC_VECTOR(24 downto 0);
         clk_div : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk_t : std_logic := '0';
   signal div_t : STD_LOGIC_VECTOR(24 downto 0) := (others=>'0');

 	--Outputs
   signal clk_div_t : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: clockS PORT MAP (
          clk => clk_t,
          div => div_t,
          clk_div => clk_div_t
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk_t <= '0';
		wait for clk_period/2;
		clk_t <= '1';
		wait for clk_period/2;
   end process;
 
   -- Stimulus process
   stim_proc: process
   begin		
      div_t<=std_logic_vector(to_unsigned(25000000,25)); --25mil
		wait for 3000 ms;
		div_t<=std_logic_vector( to_unsigned(12500000,25) ); --12.5mil
		wait for 3000 ms;
		assert false
		report "Simulación Completa"
		severity failure;
   end process;

END;
