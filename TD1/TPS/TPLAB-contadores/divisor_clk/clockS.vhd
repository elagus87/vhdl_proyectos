
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity clockS is
    Port ( clk : in  STD_LOGIC;
           div : in  STD_LOGIC_VECTOR(24 downto 0);
			  a_rst : in  STD_LOGIC;
           clk_div : out  STD_LOGIC);
end clockS;

architecture Behavioral of clockS is
	signal bitCK : std_logic :='0';
begin

	 clk_divisor:process(clk,div,a_rst)
		variable count : unsigned(24 downto 0) :=(others=>'0');
		begin
		if (a_rst='1') then count:=(others=>'0');
		elsif rising_edge(clk) then count := count + 1;
				if (count=unsigned(div)) then 
					bitCK<='1';
					count := (others=>'0');
				else bitCK<='0';
				end if;
		end if;	
	 end process clk_divisor;
	
	clk_div<= bitCK;

end Behavioral;

