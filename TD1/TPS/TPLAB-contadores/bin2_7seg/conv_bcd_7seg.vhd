	
	library	ieee;
	use		ieee.std_logic_1164.all;
	
	entity conv_bin_7seg is
		port(	bin : in		std_logic_vector(3 downto 0);
				sseg0_L : out	std_logic_vector(6 downto 0);
				sseg1_L : out	std_logic_vector(6 downto 0);
				sseg2_L : out	std_logic_vector(6 downto 0);
				sseg3_L : out	std_logic_vector(6 downto 0));
		
	end entity conv_bin_7seg;
	
	architecture data_flow of conv_bin_7seg is
	signal sseg: std_logic_vector(6 downto 0); -- logica positiva
	begin
	
		sseg <= "0111111" when bin = "0000" else	-- (0)
						"0000110" when bin = "0001" else	-- (1)
						"1011011" when bin = "0010" else	-- (2)
						"1001111" when bin = "0011" else	-- (3)
						"1100110" when bin = "0100" else	-- (4)
						"1101101" when bin = "0101" else	-- (5)
						"1111100" when bin = "0110" else	-- (6)
						"0000111" when bin = "0111" else	-- (7)
						"1111111" when bin = "1000" else	-- (8)
						"1100111" when bin = "1001" else	-- (9)
						"1111101" when bin = "1010" else	-- (a)
						"0011111" when bin = "1011" else	-- (b)
						"1001110" when bin = "1100" else	-- (c)
						"0111101" when bin = "1101" else	-- (d)
						"1101111" when bin = "1110" else	-- (e)
						"1000111" when bin = "1111" else	-- (f)
						"1111001"; 								-- (E)rror
	
	-- Los segmentos del display se activan con nivel bajo (L)
	
		sseg0_L <= not sseg; 	-- Display Hex0
		sseg1_L <= "1111111"; 	-- Display Hex1 desactivado
		sseg2_L <= "1111111"; 	-- Display Hex2 desactivado
		sseg3_L <= "1111111"; 	-- Display Hex3 desactivado
	
	end architecture data_flow;


	