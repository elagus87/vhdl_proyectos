	
	library	ieee;
	use		ieee.std_logic_1164.all;
	
	entity tb_conv_bcd_7seg is
	end entity	tb_conv_bcd_7seg;
	
	architecture behavior of tb_conv_bcd_7seg is
	-- inputs
	signal		test_bcd : std_logic_vector(3 downto 0);
	-- outputs
	signal	test_sseg0_L, test_sseg1_L, test_sseg2_L, test_sseg3_L : std_logic_vector(6 downto 0);
	-- constants
	constant INTERVAL: time := 20 ns;
	
	begin
	
	uut:	entity work.conv_bcd_7seg
					port map(
					bcd => test_bcd,		
					sseg0_L => test_sseg0_L,
					sseg1_L => test_sseg1_L,
					sseg2_L => test_sseg2_L,
					sseg3_L => test_sseg3_L
					);
					
	bcd_proc: process
	begin
		test_bcd <= "0000";
		wait for INTERVAL;
		test_bcd <= "0001";
		wait for INTERVAL;
		test_bcd <= "0010";
		wait for INTERVAL;
		test_bcd <= "0011";
		wait for INTERVAL;
		test_bcd <= "0100";
		wait for INTERVAL;
		test_bcd <= "0101";
		wait for INTERVAL;
		test_bcd <= "0110";
		wait for INTERVAL;
		test_bcd <= "0111";
		wait for INTERVAL;
		test_bcd <= "1000";
		wait for INTERVAL;
		test_bcd <= "1001";
		wait for INTERVAL;
		test_bcd <= "1010";
		wait for INTERVAL;
		test_bcd <= "1011";
		wait for INTERVAL;
		test_bcd <= "1100";
		wait for INTERVAL;
		test_bcd <= "1101";
		wait for INTERVAL;
		test_bcd <= "1110";
		wait for INTERVAL;
		test_bcd <= "1111";
		wait for INTERVAL;
		assert false
			report "Simulation Completed"
			severity failure;
	end process bcd_proc;
		
	end architecture behavior;
	
	