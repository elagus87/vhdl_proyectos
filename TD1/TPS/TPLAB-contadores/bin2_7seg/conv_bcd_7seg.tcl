	
	# BCD-TO-SEVEN SEGMENT CONVERTER: PIN ASSIGNMENT
	
	#package require ::quartus::project
	
	# Slide switches
	set_location_assignment PIN_V12 -to bcd[3]
	set_location_assignment PIN_M22 -to bcd[2]
	set_location_assignment PIN_L21 -to bcd[1]
	set_location_assignment PIN_L22 -to bcd[0]
	
	# Display 0
	set_location_assignment PIN_E2 -to sseg0_L[6]
	set_location_assignment PIN_F1 -to sseg0_L[5]
	set_location_assignment PIN_F2 -to sseg0_L[4]
	set_location_assignment PIN_H1 -to sseg0_L[3]
	set_location_assignment PIN_H2 -to sseg0_L[2]
	set_location_assignment PIN_J1 -to sseg0_L[1]
	set_location_assignment PIN_J2 -to sseg0_L[0]
	
	# Display 1
	set_location_assignment PIN_D1 -to sseg1_L[6]
	set_location_assignment PIN_D2 -to sseg1_L[5]
	set_location_assignment PIN_G3 -to sseg1_L[4]
	set_location_assignment PIN_H4 -to sseg1_L[3]
	set_location_assignment PIN_H5 -to sseg1_L[2]
	set_location_assignment PIN_H6 -to sseg1_L[1]
	set_location_assignment PIN_E1 -to sseg1_L[0]
	
	# Display 2
	set_location_assignment PIN_D3 -to sseg2_L[6]
	set_location_assignment PIN_E4 -to sseg2_L[5]
	set_location_assignment PIN_E3 -to sseg2_L[4]
	set_location_assignment PIN_C1 -to sseg2_L[3]
	set_location_assignment PIN_C2 -to sseg2_L[2]
	set_location_assignment PIN_G6 -to sseg2_L[1]
	set_location_assignment PIN_G5 -to sseg2_L[0]
	
	# Display 3
	set_location_assignment PIN_D4 -to sseg3_L[6]
	set_location_assignment PIN_F3 -to sseg3_L[5]
	set_location_assignment PIN_L8 -to sseg3_L[4]
	set_location_assignment PIN_J4 -to sseg3_L[3]
	set_location_assignment PIN_D6 -to sseg3_L[2]
	set_location_assignment PIN_D5 -to sseg3_L[1]
	set_location_assignment PIN_F4 -to sseg3_L[0]