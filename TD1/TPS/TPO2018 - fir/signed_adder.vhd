library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity signed_adder is
  generic(
    W : natural := 16
  );
  port(
    a   : in  std_logic_vector(W-1 downto 0);
    b   : in  std_logic_vector(W-1 downto 0);
    r   : out std_logic_vector(W-1 downto 0)
  );
end signed_adder;

architecture Behavioral of signed_adder is
  signal s_a     : signed(W-1 downto 0);
  signal s_b     : signed(W-1 downto 0);
  signal s_r     : signed(W-1 downto 0);
  signal s_r_sat : signed(W-1 downto 0);
  signal v_ctrl  : std_logic_vector(2 downto 0);
  constant s_max : integer := +2**(W-1) - 1;
  constant s_min : integer := -2**(W-1);
begin
  
  s_a <= signed(a);
  s_b <= signed(b);
 
  s_r    <= s_a + s_b;
  
	v_ctrl <= s_r(W-1) & s_b(W-1) & s_a(W-1);   
	
  with v_ctrl select
    s_r_sat <=  to_signed(s_max,W) when "100",
                to_signed(s_min,W) when "011",          
                s_r                when others;                
  r <= std_logic_vector(s_r_sat);
	
end Behavioral;
