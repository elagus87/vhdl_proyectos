library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fir_transpose is
  generic(
    W : natural := 16;
    L : natural := 32
  );
  port(
    x   : in  std_logic_vector(W-1 downto 0);
    clk : in  std_logic;
    rst : in  std_logic;
    en  : in  std_logic;
    y   : out std_logic_vector(W-1 downto 0)
  );
end fir_transpose;

architecture Behavioral of fir_transpose is
  type std_logic_array is array(L-1 downto 0) of std_logic_vector(W-1 downto 0);
  signal qreg  : std_logic_array;
  signal prod  : std_logic_array;
  signal sum   : std_logic_array;
  constant coeff : std_logic_array := ( "0000000001100100",
                                        "0000000000000110",
                                        "1111111101001000",
                                        "1111111101011001",
                                        "0000000011001000",
                                        "0000000110011011",
                                        "1111111110110010",
                                        "1111110100011011",
                                        "1111111010010000",
                                        "0000001110110111",
                                        "0000010011001011",
                                        "1111110011111010",
                                        "1111010101011001",
                                        "1111110111110000",
                                        "0001100010110111",
                                        "0011000110001000",
                                        "0011000110001000",
                                        "0001100010110111",
                                        "1111110111110000",
                                        "1111010101011001",
                                        "1111110011111010",
                                        "0000010011001011",
                                        "0000001110110111",
                                        "1111111010010000",
                                        "1111110100011011",
                                        "1111111110110010",
                                        "0000000110011011",
                                        "0000000011001000",
                                        "1111111101011001",
                                        "1111111101001000",
                                        "0000000000000110",
                                        "0000000001100100");

begin

  ffs: for k in L-1 downto 0 generate
    ffs_k: entity work.ffd_en
      generic map(
        W => W
      )
      port map(
        d   => sum(k),
        clk => clk,
        rst => rst,
        en  => en,
        q   => qreg(k)
      );
  end generate ffs;
  
  mul: for k in L-1 downto 0 generate
    mull_k: entity work.signed_multiplier
      generic map(
        W => W
      )
      port map(
        a => x,
        b => coeff(k),
        p => prod(k)
      );
  end generate mul;
  
  sum(L-1) <= prod(L-1);
  
  add: for k in L-2 downto 0 generate
    add_k: entity work.signed_adder
     generic map(
       W => W
     )
     port map(
       a => prod(k),
       b => qreg(k+1),
       r => sum(k)
     );
  end generate add;
  
  y <= qreg(0);
end Behavioral;
