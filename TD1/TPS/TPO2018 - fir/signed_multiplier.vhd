library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity signed_multiplier is
  generic(
    W : natural := 16
  );
  port(
    a   : in  std_logic_vector(W-1 downto 0);
    b   : in  std_logic_vector(W-1 downto 0);
    p   : out std_logic_vector(W-1 downto 0)
  );
end signed_multiplier;

architecture Behavioral of signed_multiplier is
  signal s_a      : signed(  W-1 downto 0);
  signal s_b      : signed(  W-1 downto 0);
  signal s_p      : signed(2*W-1 downto 0);
  signal s_p_trun : signed(  W-1 downto 0);
begin
  s_a      <= signed(a);
  s_b      <= signed(b);
  s_p      <= s_a * s_b;
  s_p_trun <= s_p(2*W-2 downto W-1);  
  p        <= std_logic_vector(s_p_trun);
end Behavioral;
