library ieee;
use ieee.std_logic_1164.all;

entity piso_reg is 
	generic(W:natural :=64);
	port(
			clk,rst,load,enable:in std_logic;
			paralel_input:in std_logic_vector(W-1 downto 0);
			serial_output:out std_logic
		 );
end entity piso_reg;

architecture behavioral of piso_reg is
	signal s_reg,s_next:std_logic_vector(W-1 downto 0);
begin
	registro:process(clk,rst)
	begin
		if rst='1' then s_reg<=(others=>'0');
		elsif rising_edge(clk) then
			s_reg <= s_next;
		end if;
	end process registro;
	
--logica de estado futuro
	s_next <= paralel_input when load='1' else
				 s_reg(W-2 downto 0) &  '0' when enable='1' else s_reg;
	
--logica de salida
	serial_output <= s_reg(W-1);
end architecture behavioral;