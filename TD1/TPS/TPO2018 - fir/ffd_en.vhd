library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ffd_en is
  generic(
    W : natural := 16
  );
  port(
    d   : in  std_logic_vector(W-1 downto 0);
    clk : in  std_logic;
    rst : in  std_logic;
    en  : in  std_logic;
    q   : out std_logic_vector(W-1 downto 0)
  );
end ffd_en;

architecture Behavioral of ffd_en is
begin

  process(clk, rst) begin
    if (rst = '1') then
      q <= (others => '0');
    elsif (rising_edge(clk)) then
      if (en = '1') then
        q <= d;
      end if;
    end if;
  end process;
  
end Behavioral;
