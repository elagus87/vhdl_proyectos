library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fir_dual_ch is
  generic(
    W : natural := 32
  );
  port(
    x   : in  std_logic_vector(W-1 downto 0);
    clk : in  std_logic;
    rst : in  std_logic;
    en  : in  std_logic;
    y   : out std_logic_vector(W-1 downto 0)
  );
end fir_dual_ch;

architecture Behavioral of fir_dual_ch is
  signal y_left  : std_logic_vector((W/2)-1 downto 0);
  signal y_right : std_logic_vector((W/2)-1 downto 0);
begin

  left_ch: entity work.fir_transpose
    generic map(
      W => W/2
    )
    port map(
      x   => x(W-1 downto W/2),
      clk => clk,
      rst => rst,
      en  => en,
      y   => y_left
    );
  
  right_ch: entity work.fir_transpose
    generic map(
      W => W/2
    )
    port map(
      x   => x((W/2)-1 downto 0),
      clk => clk,
      rst => rst,
      en  => en,
      y   => y_right
    );
    
    y <= y_left & y_right;

end Behavioral;

