library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity route_src is
generic(DATA_WIDTH:natural:=11);
  port (
			  i_r0 :in  std_logic_vector(DATA_WIDTH-1 downto 0);
			  i_r1 :in  std_logic_vector(DATA_WIDTH-1 downto 0);
		 	  i_r2 :in  std_logic_vector(DATA_WIDTH-1 downto 0);
		 	  i_r3 :in  std_logic_vector(DATA_WIDTH-1 downto 0);
		   i_const :in  std_logic_vector(DATA_WIDTH-1 downto 0);
		i_sel_src1 :in  std_logic_vector(1 downto 0);
		i_sel_src2 :in  std_logic_vector(1 downto 0);
		i_const_en :in  std_logic;
		     o_op1 :out std_logic_vector(DATA_WIDTH-1 downto 0);
		     o_op2 :out std_logic_vector(DATA_WIDTH-1 downto 0)
  		) ;
end entity ;

architecture data_flow of route_src is
begin
	with i_sel_src1 select
	o_op1 <= i_r0 when "00",
			 i_r1 when "01",
			 i_r2 when "10",
			 i_r3 when others;

	with i_const_en & i_sel_src2 select
	o_op2 <= i_r0 when "000",
			 i_r1 when "001",
			 i_r2 when "010",
			 i_r3 when "011",
			 i_const when others;

end data_flow ;