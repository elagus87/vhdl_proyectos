LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
use work.random_stim_pkg.all;
use work.report_pkg.all;
use work.gldn_mdl_pkg.all;
 
ENTITY tb_route_src IS
END tb_route_src;
 
ARCHITECTURE behavior OF tb_route_src IS 

   shared variable   var_i_r0        : std_logic_vector(15 downto 0)  := (others => '0');
   shared variable   var_i_r1        : std_logic_vector(15 downto 0)  := (others => '0');
   shared variable   var_i_r2        : std_logic_vector(15 downto 0)  := (others => '0');
   shared variable   var_i_r3        : std_logic_vector(15 downto 0)  := (others => '0');
   shared variable   var_i_const     : std_logic_vector(15 downto 0)  := (others => '0');
   shared variable   var_i_sel_src1  : std_logic_vector (1 downto 0)  := (others => '0');
   shared variable   var_i_sel_src2  : std_logic_vector (1 downto 0)  := (others => '0');
   shared variable   var_i_const_en  : std_logic                      := '0';

   signal            t_i_r0          : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_i_r1          : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_i_r2          : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_i_r3          : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_i_const       : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_i_sel_src1    : std_logic_vector (1 downto 0)  := (others => '0');
   signal            t_i_sel_src2    : std_logic_vector (1 downto 0)  := (others => '0');
   signal            t_i_const_en    : std_logic                      := '0';

   signal            t_o_op1         : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_o_op2         : std_logic_vector(15 downto 0)  := (others => '0');
   signal            exp_op1         : std_logic_vector(15 downto 0)  := (others => '0');
   signal            exp_op2         : std_logic_vector(15 downto 0)  := (others => '0');

   shared variable   errors          : integer := 0;
 
BEGIN

   -- Instantiate the Unit Under Test (UUT)
   uut: entity work.route_src
   generic map (
      DATA_WIDTH => 16
   )
   port map (
          i_r0       => t_i_r0      ,
          i_r1       => t_i_r1      ,
          i_r2       => t_i_r2      ,
          i_r3       => t_i_r3      ,
          i_const    => t_i_const   ,
          i_sel_src1 => t_i_sel_src1,
          i_sel_src2 => t_i_sel_src2,
          i_const_en => t_i_const_en,
          o_op1      => t_o_op1     ,
          o_op2      => t_o_op2
        );

   -- Stimulus process
   stim_proc: process
   begin
      report_begin;
      for k in 0 to 99 loop
         var_i_r0       := random_vector(16);
         var_i_r1       := random_vector(16);
         var_i_r2       := random_vector(16);
         var_i_r3       := random_vector(16);
         var_i_const    := random_vector(16);
         var_i_sel_src1 := random_vector(2) ;
         var_i_sel_src2 := random_vector(2) ;
         var_i_const_en := random_bit       ;
         gldn_route_src(
            var_i_r0      ,
            var_i_r1      ,
            var_i_r2      ,
            var_i_r3      ,
            var_i_const   ,
            var_i_sel_src1,
            var_i_sel_src2,
            var_i_const_en,
            exp_op1       ,
            exp_op2
         );
         t_i_r0         <= var_i_r0         ;
         t_i_r1         <= var_i_r1         ;
         t_i_r2         <= var_i_r2         ;
         t_i_r3         <= var_i_r3         ;
         t_i_const      <= var_i_const      ;
         t_i_sel_src1   <= var_i_sel_src1   ;
         t_i_sel_src2   <= var_i_sel_src2   ;
         t_i_const_en   <= var_i_const_en   ;
         wait for 5 ns;
         if (report_error(exp_op1, t_o_op1, "o_op1")) then
            errors := errors + 1;
         end if;
         if (report_error(exp_op2, t_o_op2, "o_op2")) then
            errors := errors + 1;
         end if;
         wait for 5 ns;
      end loop;
      report_pass_fail(errors);
      report_end;
   end process;

END;
