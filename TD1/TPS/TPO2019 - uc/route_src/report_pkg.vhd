LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.math_real.all;
use std.env.stop; --Posibilita el stop sin recurrir al failure

package report_pkg is

   procedure report_begin;

   procedure report_end;

   function  report_error(
      constant ev      : in std_logic_vector;
      constant cv      : in std_logic_vector;
      constant cv_name : in string
   ) return boolean;

   procedure  report_pass_fail(
      constant errors  : in integer
   );


end report_pkg;

-------------------------------------------------------------------------------
-- begin of package
package body report_pkg is

   procedure report_begin is
   begin
      report LF &
             "----------------------------------------" & LF &
             "COMIENZO DE LA SIMULACION"                & LF &
             "----------------------------------------" & LF ;
   end procedure report_begin;

   procedure report_end is
   begin
      report LF &
             "---------------------------------" & LF &
             "FIN  DE  LA  SIMULACION"           & LF &
             "---------------------------------" & LF
      severity note;
      stop;--fin sin falla
   end procedure report_end;

   function report_error(
      constant ev      : in std_logic_vector;
      constant cv      : in std_logic_vector;
      constant cv_name : in string
   ) return boolean is
   begin
      if (ev /= cv) then
         report LF &
                "************************************************" & LF &
                "    ERROR al evaluar " & cv_name & "."            & LF &
                "************************************************" & LF &
                "        Valor esperado de "    & cv_name & " = " & INTEGER'IMAGE(to_integer(unsigned(ev))) & LF &
                "        Valor actual      de " & cv_name & " = " & INTEGER'IMAGE(to_integer(unsigned(cv))) & LF & LF;
         return TRUE;
      else
         return FALSE;
      end if;
   end function report_error;

   procedure report_pass_fail(
      constant errors  : in integer
   ) is
   begin
      if (errors /= 0) then
         report LF &
                "-------------------------------------------------" & LF &
                " LA SIMULACION NO FUE EXITOSA"                     & LF &
                " Se han detectado:"                                & LF &
                "     " & INTEGER'IMAGE(errors) & " errores."       & LF &
                " Revise los mensajes previos para     "            & LF &
                " poder corregir los errores cometidos."            & LF &
                "-------------------------------------------------" & LF;
      else
         report LF &
                "------------------------------------------------" & LF &
                "LA SIMULACION FUE SATISFACTORIA!"                 & LF &
                "------------------------------------------------" & LF;
      end if;
   end procedure report_pass_fail;

end package body report_pkg;
