LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
use work.random_stim_pkg.all;
use work.report_pkg.all;
use work.gldn_mdl_pkg.all;
 
ENTITY tb_arith_unit IS
END tb_arith_unit;
 
ARCHITECTURE behavior OF tb_arith_unit IS

   shared variable   var_i_op1       : std_logic_vector(15 downto 0)  := (others => '0');
   shared variable   var_i_op2       : std_logic_vector(15 downto 0)  := (others => '0');
   shared variable   var_i_opcode    : std_logic_vector (3 downto 0)  := (others => '0');

   signal            t_i_op1         : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_i_op2         : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_i_opcode      : std_logic_vector (3 downto 0)  := (others => '0');

   signal            t_o_result      : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_o_flags       : std_logic_vector (3 downto 0)  := (others => '0');
   signal            exp_o_result    : std_logic_vector(15 downto 0)  := (others => '0');
   signal            exp_o_flags     : std_logic_vector (3 downto 0)  := (others => '0');

   shared variable   errors          : integer := 0;
 
BEGIN

   -- Instantiate the Unit Under Test (UUT)
   uut: entity work.arith_unit
   generic map (
      DATA_WIDTH => 16
   )
   port map (
          i_op1    => t_i_op1   ,
          i_op2    => t_i_op2   ,
          i_opcode => t_i_opcode,
          o_result => t_o_result,
          o_flags  => t_o_flags
   );

   -- Stimulus process
   stim_proc: process
   begin
      report_begin;
      for k in 0 to 99 loop
         var_i_op1      := random_vector(16);
         var_i_op2      := random_vector(16);
         var_i_opcode   := random_vector (4);
         gldn_arith_unit(
            var_i_op1   ,
            var_i_op2   ,
            var_i_opcode,
            exp_o_result,
            exp_o_flags
         );
         t_i_op1        <= var_i_op1        ;
         t_i_op2        <= var_i_op2        ;
         t_i_opcode     <= var_i_opcode     ;
         wait for 5 ns;
         if report_error(exp_o_result, t_o_result, "o_result") then
            errors := errors + 1;
         end if;
         if report_error(exp_o_flags , t_o_flags , "o_flags" ) then
            errors := errors + 1;
         end if;
         wait for 5 ns;
      end loop;
      report_pass_fail(errors);
      report_end;
   end process;

END;
