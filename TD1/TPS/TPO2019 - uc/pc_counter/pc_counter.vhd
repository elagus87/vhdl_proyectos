library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pc_counter is
generic(N_COND:natural:=4;
	    PC_WIDTH:natural:=8);
  port (
		i_clk :in std_logic;
		i_rst :in std_logic;
		i_en :in std_logic;
		i_srst :in std_logic;
		i_jump_e :in std_logic;
		i_cond_e :in std_logic;
		i_jump_sel :in std_logic_vector(1 downto 0);
		i_cond_vec :in std_logic_vector(N_COND-1 downto 0);
		i_const_addr :in std_logic_vector(PC_WIDTH-1 downto 0);
		o_pc_addr :out std_logic_vector(PC_WIDTH-1 downto 0)
  		);
end entity ; -- pc_counter

architecture arch of pc_counter is
	signal o_pc_addr_reg,o_pc_addr_next:unsigned(PC_WIDTH-1 downto 0);
	signal to_cond_addr,i_cont_addr_u:unsigned(PC_WIDTH-1 downto 0);

	constant zero : unsigned(PC_WIDTH-1 downto 0):=(others => '0');
begin

	i_cont_addr_u <= unsigned(i_const_addr);
	
	--**********************************
	registro : process(  i_clk,i_rst  )
	begin
		if i_rst='1' then
			o_pc_addr_reg <= (others => '0');
		elsif rising_edge(i_clk) then
			o_pc_addr_reg <= o_pc_addr_next;			
		end if ;
	end process ; -- registro

	--**********************************
	o_pc_addr_next <= o_pc_addr_reg when i_en='0' else
					  zero when i_srst='1' else 
					  i_cont_addr_u when i_jump_e='1' else
					  to_cond_addr when i_cond_e='0' else
					  i_cont_addr_u;

	--**********************************
	to_cond_addr <= i_cont_addr_u when (i_jump_sel&i_cond_vec(0))="001" else
					i_cont_addr_u when (i_jump_sel&i_cond_vec(1))="011" else
					i_cont_addr_u when (i_jump_sel&i_cond_vec(2))="101" else
					i_cont_addr_u when (i_jump_sel&i_cond_vec(3))="111" else
					o_pc_addr_reg +1;

	--**********************************
	o_pc_addr <= std_logic_vector(o_pc_addr_reg);

end arch ; -- arch