LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE ieee.math_real.all;

package gldn_mdl_pkg is

   procedure gldn_route_src (
      constant i_r0        : in  std_logic_vector(15 downto 0);
      constant i_r1        : in  std_logic_vector(15 downto 0);
      constant i_r2        : in  std_logic_vector(15 downto 0);
      constant i_r3        : in  std_logic_vector(15 downto 0);
      constant i_const     : in  std_logic_vector(15 downto 0);
      constant i_sel_src1  : in  std_logic_vector (1 downto 0);
      constant i_sel_src2  : in  std_logic_vector (1 downto 0);
      constant i_const_en  : in  std_logic                    ;
      signal   o_op1       : out std_logic_vector(15 downto 0);
      signal   o_op2       : out std_logic_vector(15 downto 0)
   );

   procedure gldn_route_dest (
      constant i_data      : in  std_logic_vector(15 downto 0);
      constant i_const     : in  std_logic_vector(15 downto 0);
      constant i_sel_dest  : in  std_logic_vector (1 downto 0);
      constant i_const_en  : in  std_logic                    ;
      signal   o_data      : out std_logic_vector(15 downto 0);
      signal   o_ld        : out std_logic_vector (3 downto 0)
   );

   procedure gldn_logic_unit (
      constant i_op1       : in  std_logic_vector(15 downto 0);
      constant i_op2       : in  std_logic_vector(15 downto 0);
      constant i_opcode    : in  std_logic_vector (3 downto 0);
      signal   o_result    : out std_logic_vector(15 downto 0)
   );

   procedure gldn_arith_unit (
      constant i_op1       : in  std_logic_vector(15 downto 0);
      constant i_op2       : in  std_logic_vector(15 downto 0);
      constant i_opcode    : in  std_logic_vector (3 downto 0);
      signal   o_result    : out std_logic_vector(15 downto 0);
      signal   o_flags     : out std_logic_vector (3 downto 0)
   );

   procedure gldn_reg_flags  (
      constant i_en         : in    std_logic;
      constant i_srst       : in    std_logic;
      constant i_flags      : in    std_logic_vector(3 downto 0);
      constant i_ld_en      : in    std_logic;
      signal   exp_o_flags  : inout std_logic_vector(3 downto 0)
   );

   procedure gldn_reg_map    (
      constant i_en         : in    std_logic;
      constant i_srst       : in    std_logic;
      constant i_data       : in    std_logic_vector(15 downto 0);
      constant i_ld_en      : in    std_logic_vector (3 downto 0);
      signal   exp_o_r0     : inout std_logic_vector(15 downto 0);
      signal   exp_o_r1     : inout std_logic_vector(15 downto 0);
      signal   exp_o_r2     : inout std_logic_vector(15 downto 0);
      signal   exp_o_r3     : inout std_logic_vector(15 downto 0)
   );

   procedure gldn_pc_counter (
      constant i_en          : in    std_logic;
      constant i_srst        : in    std_logic;
      constant i_jump_e      : in    std_logic;
      constant i_cond_e      : in    std_logic;
      constant i_jump_sel    : in    std_logic_vector(1 downto 0);
      constant i_cond_vec    : in    std_logic_vector(3 downto 0);
      constant i_const_addr  : in    std_logic_vector(7 downto 0);
      signal   exp_o_pc_addr : inout std_logic_vector(7 downto 0)
   );

end gldn_mdl_pkg;

-------------------------------------------------------------------------------
-- begin of package
package body gldn_mdl_pkg is

   ----------------------------------------------------------------------------
   -- procedure
   ----------------------------------------------------------------------------
   procedure gldn_route_src (
      constant i_r0        : in  std_logic_vector(15 downto 0);
      constant i_r1        : in  std_logic_vector(15 downto 0);
      constant i_r2        : in  std_logic_vector(15 downto 0);
      constant i_r3        : in  std_logic_vector(15 downto 0);
      constant i_const     : in  std_logic_vector(15 downto 0);
      constant i_sel_src1  : in  std_logic_vector (1 downto 0);
      constant i_sel_src2  : in  std_logic_vector (1 downto 0);
      constant i_const_en  : in  std_logic                    ;
      signal   o_op1       : out std_logic_vector(15 downto 0);
      signal   o_op2       : out std_logic_vector(15 downto 0)
   ) is
   begin
      if (i_sel_src1 = "00") then
         o_op1 <= i_r0;
      elsif (i_sel_src1 = "01") then
         o_op1 <= i_r1;
      elsif (i_sel_src1 = "10") then
         o_op1 <= i_r2;
      elsif (i_sel_src1 = "11") then
         o_op1 <= i_r3;
      else
         o_op1 <= (others => '0');
      end if;

      if (i_const_en = '1') then
         o_op2 <= i_const;
      elsif (i_sel_src2 = "00") then
         o_op2 <= i_r0;
      elsif (i_sel_src2 = "01") then
         o_op2 <= i_r1;
      elsif (i_sel_src2 = "10") then
         o_op2 <= i_r2;
      elsif (i_sel_src2 = "11") then
         o_op2 <= i_r3;
      else
         o_op1 <= (others => '0');
      end if;
   end procedure gldn_route_src;

   ----------------------------------------------------------------------------
   -- procedure
   ----------------------------------------------------------------------------
   procedure gldn_route_dest (
      constant i_data      : in  std_logic_vector(15 downto 0);
      constant i_const     : in  std_logic_vector(15 downto 0);
      constant i_sel_dest  : in  std_logic_vector (1 downto 0);
      constant i_const_en  : in  std_logic                    ;
      signal   o_data      : out std_logic_vector(15 downto 0);
      signal   o_ld        : out std_logic_vector (3 downto 0)
   ) is
   begin
      if (i_sel_dest = "00") then
         o_ld <= "0001";
      elsif (i_sel_dest = "01") then
         o_ld <= "0010";
      elsif (i_sel_dest = "10") then
         o_ld <= "0100";
      elsif (i_sel_dest = "11") then
         o_ld <= "1000";
      end if;
      if (i_const_en = '1') then
         o_data <= i_const;
      else
         o_data <= i_data;
      end if;
   end procedure gldn_route_dest;

   ----------------------------------------------------------------------------
   -- procedure
   ----------------------------------------------------------------------------
   procedure gldn_logic_unit (
      constant i_op1       : in  std_logic_vector(15 downto 0);
      constant i_op2       : in  std_logic_vector(15 downto 0);
      constant i_opcode    : in  std_logic_vector (3 downto 0);
      signal   o_result    : out std_logic_vector(15 downto 0)
   ) is
   begin
      if (i_opcode = "0000") then
         o_result <= i_op1;
      elsif (i_opcode = "0001") then
         o_result <= i_op1;
      elsif (i_opcode = "0010") then
         o_result <= (i_op1 and i_op2);
      elsif (i_opcode = "0011") then
         o_result <= (i_op1 or  i_op2);
      elsif (i_opcode = "0100") then
         o_result <= (i_op1 xor i_op2);
      elsif (i_opcode = "0101") then
         o_result <= (not i_op1);
      elsif (i_opcode = "0110") then
         o_result <= (i_op1(14 downto 0) & '0');
      elsif (i_opcode = "0111") then
         o_result <= ('0' & i_op1(15 downto 1));
      else
         o_result <= i_op1;
      end if;
   end procedure gldn_logic_unit;

   ----------------------------------------------------------------------------
   -- procedure
   ----------------------------------------------------------------------------
   procedure gldn_arith_unit (
      constant i_op1       : in  std_logic_vector(15 downto 0);
      constant i_op2       : in  std_logic_vector(15 downto 0);
      constant i_opcode    : in  std_logic_vector (3 downto 0);
      signal   o_result    : out std_logic_vector(15 downto 0);
      signal   o_flags     : out std_logic_vector (3 downto 0)
   ) is
      variable sum   : signed(15 downto 0);
      variable sub   : signed(15 downto 0);
      variable v_add : std_logic;
      variable v_sub : std_logic;
   begin
      sum   := signed(i_op1) + signed(i_op2);
      sub   := signed(i_op1) - signed(i_op2);
      v_add := (    sum(15)  and not(i_op2(15)) and not(i_op1(15))) or
               (not(sum(15)) and     i_op2(15)  and     i_op1(15) ) ;
      v_sub := (    sub(15)  and     i_op2(15)  and not(i_op1(15))) or
               (not(sub(15)) and not(i_op2(15)) and     i_op1(15) ) ;

      case (i_opcode) is
         when "1000"|"1001" =>
            o_result <= std_logic_vector(sum);
            if (sum = 0) then
               o_flags(0) <= '1';
            else
               o_flags(0) <= '0';
            end if;
            o_flags(1) <= sum(15);
            o_flags(2) <= '0';
            o_flags(3) <= v_add;
         when "1010"|"1011" =>
            o_result <= std_logic_vector(sub);
            if (sub = 0) then
               o_flags(0) <= '1';
            else
               o_flags(0) <= '0';
            end if;
            o_flags(1) <= sub(15);
            o_flags(2) <= '0';
            o_flags(3) <= v_sub;
         when "1100"|"1101" =>
            o_result   <= (others => '0');
            o_flags(0) <= '0';
            o_flags(1) <= '0';
            if (i_op1 = i_op2) then
               o_flags(2) <= '1';
            else
               o_flags(2) <= '0';
            end if;
            o_flags(3) <= '0';
         when "1110"|"1111" =>
            o_result   <= (others => '0');
            o_flags    <= (others => '0');
         when others        =>
            o_result   <= (others => '0');
            o_flags    <= (others => '0');
      end case;
   end procedure gldn_arith_unit;

   ----------------------------------------------------------------------------
   -- procedure
   ----------------------------------------------------------------------------
   procedure gldn_reg_flags  (
      constant i_en         : in    std_logic;
      constant i_srst       : in    std_logic;
      constant i_flags      : in    std_logic_vector(3 downto 0);
      constant i_ld_en      : in    std_logic;
      signal   exp_o_flags  : inout std_logic_vector(3 downto 0)
   ) is
   begin
      if (i_en = '1') then
         if (i_srst = '1') then
            exp_o_flags <= (others => '0');
         elsif (i_ld_en = '1') then
            exp_o_flags <= i_flags;
         end if;
      end if;
   end procedure  gldn_reg_flags;

   ----------------------------------------------------------------------------
   -- procedure
   ----------------------------------------------------------------------------
   procedure gldn_reg_map    (
      constant i_en         : in    std_logic;
      constant i_srst       : in    std_logic;
      constant i_data       : in    std_logic_vector(15 downto 0);
      constant i_ld_en      : in    std_logic_vector (3 downto 0);
      signal   exp_o_r0     : inout std_logic_vector(15 downto 0);
      signal   exp_o_r1     : inout std_logic_vector(15 downto 0);
      signal   exp_o_r2     : inout std_logic_vector(15 downto 0);
      signal   exp_o_r3     : inout std_logic_vector(15 downto 0)
   ) is
   begin
      if (i_en = '1') then
         if (i_srst = '1') then
            exp_o_r0 <= (others => '0');
            exp_o_r1 <= (others => '0');
            exp_o_r2 <= (others => '0');
            exp_o_r3 <= (others => '0');
         else
            if (i_ld_en(0) = '1') then
               exp_o_r0 <= i_data;
            end if;
            if (i_ld_en(1) = '1') then
               exp_o_r1 <= i_data;
            end if;
            if (i_ld_en(2) = '1') then
               exp_o_r2 <= i_data;
            end if;
            if (i_ld_en(3) = '1') then
               exp_o_r3 <= i_data;
            end if;
         end if;
      end if;
   end procedure gldn_reg_map;

   ----------------------------------------------------------------------------
   -- procedure
   ----------------------------------------------------------------------------
   procedure gldn_pc_counter (
      constant i_en          : in    std_logic;
      constant i_srst        : in    std_logic;
      constant i_jump_e      : in    std_logic;
      constant i_cond_e      : in    std_logic;
      constant i_jump_sel    : in    std_logic_vector(1 downto 0);
      constant i_cond_vec    : in    std_logic_vector(3 downto 0);
      constant i_const_addr  : in    std_logic_vector(7 downto 0);
      signal   exp_o_pc_addr : inout std_logic_vector(7 downto 0)
   ) is
      variable curr_addr : unsigned(7 downto 0);
      variable next_addr : unsigned(7 downto 0);
      variable cond_met  : std_logic;
      variable jump_vec  : std_logic_vector(2 downto 0);
   begin
      case (i_jump_sel) is
         when "00"   => 
            cond_met := i_cond_vec(0);
         when "01"   => 
            cond_met := i_cond_vec(1);
         when "10"   => 
            cond_met := i_cond_vec(2);
         when "11"   => 
            cond_met := i_cond_vec(3);
         when others => 
            cond_met := 'X';
      end case;
      curr_addr := unsigned(exp_o_pc_addr);
      jump_vec  := i_jump_e & i_cond_e & cond_met;
      case (jump_vec) is
         when "111"|"101"|"100" =>
            next_addr := unsigned(i_const_addr);
         when others =>
            next_addr := curr_addr + 1;
      end case;
      if (i_en = '1') then
         if (i_srst = '1') then
            exp_o_pc_addr <= (others => '0');
         else
            exp_o_pc_addr <= std_logic_vector(next_addr);
         end if;
      end if;
   end procedure gldn_pc_counter;

end package body gldn_mdl_pkg;
