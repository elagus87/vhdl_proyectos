library IEEE;
use ieee.std_logic_1164.all;

entity reg_map is
generic(DATA_WIDTH:natural:=11);
  port (
		i_clk :in std_logic;
		i_rst :in std_logic;
		i_en :in std_logic;
		i_srst :in std_logic;
		i_data :in std_logic_vector(DATA_WIDTH-1 downto 0);
		i_ld_en :in std_logic_vector(3 downto 0);
		o_r0 :out std_logic_vector(DATA_WIDTH-1 downto 0);
		o_r1 :out std_logic_vector(DATA_WIDTH-1 downto 0);
		o_r2 :out std_logic_vector(DATA_WIDTH-1 downto 0);
		o_r3 :out std_logic_vector(DATA_WIDTH-1 downto 0)
  		) ;
end entity ; -- reg_map

architecture arch of reg_map is
	signal o_r0_reg,o_r0_next: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal o_r1_reg,o_r1_next: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal o_r2_reg,o_r2_next: std_logic_vector(DATA_WIDTH-1 downto 0);
	signal o_r3_reg,o_r3_next: std_logic_vector(DATA_WIDTH-1 downto 0);

	constant zero : std_logic_vector(DATA_WIDTH-1 downto 0):=(others => '0');
begin

	registros : process( i_clk,i_rst )
	begin
		if i_rst='1' then
			o_r0_reg <= (others => '0');
			o_r1_reg <= (others => '0');
			o_r2_reg <= (others => '0');
			o_r3_reg <= (others => '0');
		elsif rising_edge(i_clk) then
			o_r0_reg <= o_r0_next;
			o_r1_reg <= o_r1_next;
			o_r2_reg <= o_r2_next;
			o_r3_reg <= o_r3_next;
		end if ;
	end process ; -- registros
	
	--**********************************
	o_r0_next <= o_r0_reg when i_en='1' else
				 zero when i_srst='1' else
				 i_data when i_ld_en(0)='1'else
				 o_r0_reg;

	o_r1_next <= o_r1_reg when i_en='1' else
				 zero when i_srst='1' else
				 i_data when i_ld_en(1)='1'else
				 o_r1_reg;

	o_r2_next <= o_r2_reg when i_en='1' else
				 zero when i_srst='1' else
				 i_data when i_ld_en(2)='1'else
				 o_r2_reg;

	o_r3_next <= o_r3_reg when i_en='1' else
				 zero when i_srst='1' else
				 i_data when i_ld_en(3)='1'else
				 o_r3_reg;

	--**********************************
	o_r0 <= o_r0_reg;
	o_r1 <= o_r1_reg;
	o_r2 <= o_r2_reg;
	o_r3 <= o_r3_reg;
				 
end arch ; -- arch