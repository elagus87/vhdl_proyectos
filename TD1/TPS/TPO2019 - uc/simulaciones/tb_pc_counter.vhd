LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
use work.random_stim_pkg.all;
use work.report_pkg.all;
use work.gldn_mdl_pkg.all;
 
ENTITY tb_pc_counter IS
   generic(
      N_COND   : natural := 4;
      PC_WIDTH : natural := 8
   );
END tb_pc_counter;
 
ARCHITECTURE behavior OF tb_pc_counter IS 

   shared variable   var_i_clk        :     std_logic;
   shared variable   var_i_rst        :     std_logic;
   shared variable   var_i_en         :     std_logic;
   shared variable   var_i_srst       :     std_logic;
   shared variable   var_i_jump_e     :     std_logic;
   shared variable   var_i_cond_e     :     std_logic;
   shared variable   var_i_jump_sel   :     std_logic_vector         (1 downto 0);
   shared variable   var_i_cond_vec   :     std_logic_vector  (N_COND-1 downto 0);
   shared variable   var_i_const_addr :     std_logic_vector(PC_WIDTH-1 downto 0);
   signal            t_i_clk          :     std_logic                             := '0';
   signal            t_i_rst          :     std_logic                             := '0';
   signal            t_i_en           :     std_logic                             := '0';
   signal            t_i_srst         :     std_logic                             := '0';
   signal            t_i_jump_e       :     std_logic                             := '0';
   signal            t_i_cond_e       :     std_logic                             := '0';
   signal            t_i_jump_sel     :     std_logic_vector         (1 downto 0) := (others => '0');
   signal            t_i_cond_vec     :     std_logic_vector  (N_COND-1 downto 0) := (others => '0');
   signal            t_i_const_addr   :     std_logic_vector(PC_WIDTH-1 downto 0) := (others => '0');
   signal            t_o_pc_addr      :     std_logic_vector(PC_WIDTH-1 downto 0) := (others => '0');
   signal            exp_o_pc_addr    :     std_logic_vector(PC_WIDTH-1 downto 0) := (others => '0');
   shared variable   errors           :     integer                               := 0;
 
BEGIN

   -- Instantiate the Unit Under Test (UUT)
   uut: entity work.pc_counter
   generic map (
      N_COND   => N_COND,
      PC_WIDTH => PC_WIDTH
   )
   port map (
      i_clk        => t_i_clk       ,
      i_rst        => t_i_rst       ,
      i_en         => t_i_en        ,
      i_srst       => t_i_srst      ,
      i_jump_e     => t_i_jump_e    ,
      i_cond_e     => t_i_cond_e    ,
      i_jump_sel   => t_i_jump_sel  ,
      i_cond_vec   => t_i_cond_vec  ,
      i_const_addr => t_i_const_addr,
      o_pc_addr    => t_o_pc_addr
   );

   clk_proc: process begin
      t_i_clk <= '0';
      wait for 10 ns;
      t_i_clk <= '1';
      wait for 10 ns;
   end process;
   
   rst_proc: process begin
      wait for 20 ns;
      wait for 20 ns;
      t_i_rst <= '1';
      wait for 25 ns;
      t_i_rst <= '0';
      wait;
   end process;
   
   driver_proc: process begin
      report_begin;
      wait until falling_edge(t_i_rst);
      for k in 0 to 99 loop
         var_i_en         := random_bit;
         var_i_srst       := random_bit;
         var_i_jump_e     := random_bit;
         var_i_cond_e     := random_bit;
         var_i_jump_sel   := random_vector(2);
         var_i_cond_vec   := random_vector(N_COND  );
         var_i_const_addr := random_vector(PC_WIDTH);
         wait until falling_edge(t_i_clk);
         t_i_en         <= var_i_en        ;
         t_i_srst       <= var_i_srst      ;
         t_i_jump_e     <= var_i_jump_e    ;
         t_i_cond_e     <= var_i_cond_e    ;
         t_i_jump_sel   <= var_i_jump_sel  ;
         t_i_cond_vec   <= var_i_cond_vec  ;
         t_i_const_addr <= var_i_const_addr;
         wait until rising_edge(t_i_clk);
         gldn_pc_counter(
            var_i_en        ,
            var_i_srst      ,
            var_i_jump_e    ,
            var_i_cond_e    ,
            var_i_jump_sel  ,
            var_i_cond_vec  ,
            var_i_const_addr,
            exp_o_pc_addr
         );
         if report_error(exp_o_pc_addr, t_o_pc_addr, "o_pc_addr") then
            errors := errors + 1;
         end if;
      end loop;
      report_pass_fail(errors);
      report_end;
   end process;

END;
