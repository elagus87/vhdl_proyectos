LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
use work.random_stim_pkg.all;
use work.report_pkg.all;
use work.gldn_mdl_pkg.all;
 
ENTITY tb_route_dest IS
END tb_route_dest;
 
ARCHITECTURE behavior OF tb_route_dest IS 

   shared variable   var_i_data      : std_logic_vector(15 downto 0)  := (others => '0');
   shared variable   var_i_const     : std_logic_vector(15 downto 0)  := (others => '0');
   shared variable   var_i_sel_dest  : std_logic_vector (1 downto 0)  := (others => '0');
   shared variable   var_i_const_en  : std_logic                      := '0'            ;

   signal            t_i_data        : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_i_const       : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_i_sel_dest    : std_logic_vector (1 downto 0)  := (others => '0');
   signal            t_i_const_en    : std_logic                      := '0'            ;

   signal            t_o_data        : std_logic_vector(15 downto 0)  := (others => '0');
   signal            t_o_ld          : std_logic_vector (3 downto 0)  := (others => '0');
   signal            exp_o_data      : std_logic_vector(15 downto 0)  := (others => '0');
   signal            exp_o_ld        : std_logic_vector (3 downto 0)  := (others => '0');

   shared variable   errors          : integer := 0;

BEGIN

   -- Instantiate the Unit Under Test (UUT)
   uut: entity work.route_dest 
   generic map (
      DATA_WIDTH => 16
   )
   port map (
          i_data     => t_i_data    ,
          i_const    => t_i_const   ,
          i_sel_dest => t_i_sel_dest,
          i_const_en => t_i_const_en,
          o_data     => t_o_data    ,
          o_ld       => t_o_ld
   );

   -- Stimulus process
   stim_proc: process
   begin
      report_begin;
      for k in 0 to 99 loop
         var_i_data     := random_vector(16);
         var_i_const    := random_vector(16);
         var_i_sel_dest := random_vector(2) ;
         var_i_const_en := random_bit       ;
         gldn_route_dest(
            var_i_data    ,
            var_i_const   ,
            var_i_sel_dest,
            var_i_const_en,
            exp_o_data    ,
            exp_o_ld
         );
         t_i_data       <= var_i_data       ;
         t_i_const      <= var_i_const      ;
         t_i_sel_dest   <= var_i_sel_dest   ;
         t_i_const_en   <= var_i_const_en   ;
         wait for 5 ns;
         if (report_error(exp_o_data, t_o_data, "o_data")) then
            errors := errors + 1;
         end if;
         if (report_error(exp_o_ld  , t_o_ld  , "o_ld  ")) then
            errors := errors + 1;
         end if;
         wait for 5 ns;
      end loop;
      report_pass_fail(errors);
      report_end;
   end process;

END;
