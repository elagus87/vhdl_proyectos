LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
use work.random_stim_pkg.all;
use work.report_pkg.all;
use work.gldn_mdl_pkg.all;
 
ENTITY tb_reg_map IS
   generic(
      DATA_WIDTH : natural := 16
   );
END tb_reg_map;
 
ARCHITECTURE behavior OF tb_reg_map IS 

   shared variable   var_i_en      : std_logic                                                 ;
   shared variable   var_i_srst    : std_logic                                                 ;
   shared variable   var_i_data    : std_logic_vector(DATA_WIDTH-1 downto 0)                   ;
   shared variable   var_i_ld_en   : std_logic_vector(3 downto 0)                              ;

   signal            t_i_clk       : std_logic                               := '0'            ;
   signal            t_i_rst       : std_logic                               := '0'            ;
   signal            t_i_en        : std_logic                               := '0'            ;
   signal            t_i_srst      : std_logic                               := '0'            ;
   signal            t_i_data      : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
   signal            t_i_ld_en     : std_logic_vector           (3 downto 0) := (others => '0');
   signal            t_o_r0        : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
   signal            t_o_r1        : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
   signal            t_o_r2        : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
   signal            t_o_r3        : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');

   signal            exp_o_r0      : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
   signal            exp_o_r1      : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
   signal            exp_o_r2      : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
   signal            exp_o_r3      : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');

   shared variable   errors        : integer := 0;

BEGIN

   -- Instantiate the Unit Under Test (UUT)
   uut: entity work.reg_map
   generic map (
      DATA_WIDTH => 16
   )
   port map (
      i_clk   => t_i_clk  ,
      i_rst   => t_i_rst  ,
      i_en    => t_i_en   ,
      i_srst  => t_i_srst ,
      i_data  => t_i_data ,
      i_ld_en => t_i_ld_en,
      o_r0    => t_o_r0   ,
      o_r1    => t_o_r1   ,
      o_r2    => t_o_r2   ,
      o_r3    => t_o_r3
   );

   clk_proc: process begin
      t_i_clk <= '0';
      wait for 10 ns;
      t_i_clk <= '1';
      wait for 10 ns;
   end process;
   
   rst_proc: process begin
      wait for 20 ns;
      wait for 20 ns;
      t_i_rst <= '1';
      wait for 25 ns;
      t_i_rst <= '0';
      wait;
   end process;
   
   driver_proc: process begin
      report_begin;
      wait until falling_edge(t_i_rst);
      for k in 0 to 99 loop
         var_i_en    := random_bit;
         var_i_srst  := random_bit;
         var_i_data  := random_vector(16);
         var_i_ld_en := random_vector(4);
         wait until falling_edge(t_i_clk);
         t_i_en      <= var_i_en   ;
         t_i_srst    <= var_i_srst ;
         t_i_data    <= var_i_data ;
         t_i_ld_en   <= var_i_ld_en;
         wait until rising_edge(t_i_clk);
         gldn_reg_map(
            var_i_en   ,
            var_i_srst ,
            var_i_data ,
            var_i_ld_en,
            exp_o_r0   ,
            exp_o_r1   ,
            exp_o_r2   ,
            exp_o_r3
         );
         if report_error(exp_o_r0, t_o_r0, "o_r0") then
            errors := errors + 1;
         end if;
         if report_error(exp_o_r1, t_o_r1, "o_r1") then
            errors := errors + 1;
         end if;
         if report_error(exp_o_r2, t_o_r2, "o_r2") then
            errors := errors + 1;
         end if;
         if report_error(exp_o_r3, t_o_r3, "o_r3") then
            errors := errors + 1;
         end if;
      end loop;
      report_pass_fail(errors);
      report_end;
   end process;

END;
