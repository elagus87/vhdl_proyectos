library IEEE;
use ieee.std_logic_1164.all;

entity route_dest is
generic(DATA_WIDTH:natural:=11);
  port (
		i_data :in std_logic_vector(DATA_WIDTH-1 downto 0);
		i_const :in std_logic_vector(DATA_WIDTH-1 downto 0);
		i_sel_dest :in std_logic_vector(1 downto 0);
		i_const_en :in std_logic;
		o_data :out std_logic_vector(DATA_WIDTH-1 downto 0);
		o_ld :out std_logic_vector(3 downto 0)
  ) ;
end entity ; -- route_dest

architecture data_flow of route_dest is
begin

	with i_sel_dest select
	o_ld <= "0001" when "00",
			"0010" when "01",
			"0100" when "10",
			"1000" when others;

	with i_const_en select
	o_data <= i_data when '0',
			  i_const when others;
end data_flow ; -- arch