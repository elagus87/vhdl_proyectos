library IEEE;
use ieee.std_logic_1164.all;

entity reg_flags is
  port (
		i_clk :in std_logic;
		i_rst :in std_logic;
		i_en :in std_logic;
		i_srst :in std_logic;
		i_flags :in std_logic_vector(3 downto 0);
		i_ld_en :in std_logic;
		o_flags :out std_logic_vector(3 downto 0)
  		) ;
end entity ; -- reg_flags

architecture arch of reg_flags is
	signal o_flags_reg,o_flags_next: std_logic_vector(3 downto 0);

	constant zero : std_logic_vector(3 downto 0):=(others => '0');
begin

	registros : process( i_clk,i_rst )
	begin
		if i_rst='1' then
			o_flags_reg <= (others => '0');
		elsif rising_edge(i_clk) then
			o_flags_reg <= o_flags_next;
		end if ;
	end process ; -- registros
	
	--**********************************
	o_flags_next <= o_flags_reg when i_en='1' else
				 zero when i_srst='1' else
				 i_flags when i_ld_en='1'else
				 o_flags_reg;

	--**********************************
	o_flags <= o_flags_reg;

end arch ; -- arch