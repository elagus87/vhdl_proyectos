library IEEE;
use ieee.std_logic_1164.all;
-- testeo ok
entity logic_unit is
generic(W:natural:=16);
  port (
		op1 :in std_logic_vector(W-1 downto 0);
		op2 :in std_logic_vector(W-1 downto 0);
		opcode :in std_logic_vector(3 downto 0);
		result :out std_logic_vector(W-1 downto 0)
		) ;
end entity ; -- logic_unit

architecture data_flow of logic_unit is
begin

	with opcode select
	result <= op1 when "0000",
				op2 when "0001",
				op1 and op2 when "0010",
				op1 or op2 when "0011",
				op1 xor op2 when "0100",
				not(op1) when "0101",
				op1(W-2 downto 0) & '0' when "0110",
				'0' & op1(W-1 downto 1) when "0111",
				op1 when others;
end data_flow ; -- arch