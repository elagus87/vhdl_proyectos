library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
 
entity inc_dec is
generic(W:natural:=4);
  port (
	b : in  std_logic_vector(W-1 downto 0);
	m : in  std_logic;
 	o : out std_logic_vector(W-1 downto 0)
  	) ;
end inc_dec ; -- inc_dec
 
architecture data_flow of inc_dec is
	signal u_b,u_o: unsigned(W-1 downto 0);
begin
--para poder operar aritmeticamente
	u_b <= unsigned(b);
	u_o <= (u_b + 1) when m='0' else
		   (u_b - 1);
	o <= std_logic_vector(u_o);
end architecture data_flow;