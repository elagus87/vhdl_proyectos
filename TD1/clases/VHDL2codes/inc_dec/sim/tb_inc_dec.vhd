library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
-- paquetes creados por Fernando Orge
use work.random_stim_pkg.all;
use work.report_pkg.all;

entity tb_inc_dec is
end tb_inc_dec;

architecture tb of tb_inc_dec is
    constant Wt : natural:= 8;
    component inc_dec
    generic(w:natural:=Wt);
        port (b : in std_logic_vector (w-1 downto 0);
              m : in std_logic;
              o : out std_logic_vector (w-1 downto 0));
    end component;

    signal b : std_logic_vector (Wt-1 downto 0);
    signal m : std_logic;
    signal o : std_logic_vector (Wt-1 downto 0);
    -- variables necesarias
    shared variable     var_b      : std_logic_vector (Wt-1 downto 0);
    shared variable     var_m      : std_logic;
    shared variable     errors     : integer := 0;

begin

    dut : inc_dec
    port map (b => b,
              m => m,
              o => o);

    stimuli : process
    begin
        report_begin;
        -- nincrementa
        for k in 0 to 7 loop
            var_b := random_vector(Wt);
            var_m  := '0';
            b <= var_b;
            m <= var_m;
            wait for 5 ns;
        end loop;
        -- decrementa
        for k in 0 to 7 loop
            var_b := random_vector(Wt);
            var_m  := '1';
            b <= var_b;
            m <= var_m;
            wait for 5 ns;
        end loop;
        report_pass_fail(errors);
        report_end;
        wait;
    end process;

end tb;