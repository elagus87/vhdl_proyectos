LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
 
entity comp_sel is
generic(W:natural:=8);
  port (
	a :in std_logic_vector(W-1 downto 0);
    b :in std_logic_vector(W-1 downto 0);
    m: in std_logic;
	o :out std_logic_vector(W-1 downto 0)
  	) ;
end comp_sel ; 
 
architecture arch of comp_sel is
 
signal sa,sb,s_o: signed(W-1 downto 0);
signal ua,ub,u_o: unsigned(W-1 downto 0);
     
begin
    -- interpreto los datos
    sa <= signed(a); 
    sb <= signed(b); 
    ua <= unsigned(a); 
    ub <= unsigned(b); 
    -- comparo segun corresponde
    u_o <= ua when (ua>=ub) else ub;
    s_o <= sa when (sa>=sb) else sb;
    -- vuelvo a niveles logicos para asignar la salida
    o <= std_logic_vector(u_o) when m='0' else
         std_logic_vector(s_o);
    
end architecture ;