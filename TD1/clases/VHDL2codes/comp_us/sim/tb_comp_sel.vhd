library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
-- paquetes creados por Fernando Orge
use work.random_stim_pkg.all;
use work.report_pkg.all;

entity tb_comp_sel is
end tb_comp_sel;

architecture tb of tb_comp_sel is
    constant Wt : natural:= 8;

    component comp_sel
    generic(w:natural:=Wt);
        port (a : in std_logic_vector (w-1 downto 0);
              b : in std_logic_vector (w-1 downto 0);
              m : in std_logic;
              o : out std_logic_vector (w-1 downto 0));
    end component;

    signal a : std_logic_vector (Wt-1 downto 0);
    signal b : std_logic_vector (Wt-1 downto 0);
    signal m : std_logic;
    signal o : std_logic_vector (Wt-1 downto 0);
    -- variables necesarias
    shared variable     var_a      : std_logic_vector (Wt-1 downto 0);
    shared variable     var_b      : std_logic_vector (Wt-1 downto 0);
    shared variable     var_m      : std_logic;
    shared variable     errors     : integer := 0;

begin

    dut : comp_sel
    port map (a => a,
              b => b,
              m => m,
              o => o);

    random : process
    begin
        report_begin;
        -- no signados
        for k in 0 to 15 loop
            var_b := random_vector(Wt);
            var_a := random_vector(Wt);
            var_m  := '0';
            b <= var_b;
            a <= var_a;
            m <= var_m;
            wait for 5 ns;
        end loop;
        -- signados
        for k in 0 to 15 loop
            var_b := random_vector(Wt);
            var_a := random_vector(Wt);
            var_m  := '1';
            b <= var_b;
            a <= var_a;
            m <= var_m;
            wait for 5 ns;
        end loop;
        report_pass_fail(errors);
        report_end;
        wait;
    end process;

end tb;