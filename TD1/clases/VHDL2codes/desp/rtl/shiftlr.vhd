library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
 
entity shiftlr is
  port (
        a : in STD_LOGIC_VECTOR (7 downto 0) ;
     mode : in std_logic;
        o: out STD_LOGIC_VECTOR (7 downto 0)
 	   ) ;
end shiftlr ;
 
architecture data_flow of shiftlr is
  begin
    with mode select
    o <= a(6 downto 0) & '0' when '0',
        '0' & a(7 downto 1) when others;
end data_flow ;