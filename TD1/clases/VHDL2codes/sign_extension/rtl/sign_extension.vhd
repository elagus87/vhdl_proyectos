library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sign_extension is
  generic (Win: natural := 4;
           Wout: natural := 8) ;
  port (
        x : in std_logic_vector (Win-1 downto 0) ;
        y : out std_logic_vector (Wout-1 downto 0)
  	   ) ;
end sign_extension ;

architecture data_flow of sign_extension is
  constant zeros:
  std_logic_vector (Wout-Win-1 downto 0) := ( others => '0') ;
  constant ones:
  std_logic_vector (Wout-Win-1 downto 0) := ( others => '1') ;
  signal sign_x : std_logic ;
begin
  sign_x <= x (Win-1) ; --miro MSB para decidir
  y <= ( zeros & x ) when sign_x = '0' else
       ( ones & x ) ;
end data_flow ;