
library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
-- paquetes creados por Fernando Orge
use work.random_stim_pkg.all;
use work.report_pkg.all;

entity tb_sign_extension is
end tb_sign_extension;

architecture tb of tb_sign_extension is
    constant Wt : natural:= 8;
    component sign_extension
    generic(Win:natural:=Wt;
            Wout:natural:=2*Wt);
        port (x : in std_logic_vector (win-1 downto 0);
              y : out std_logic_vector (wout-1 downto 0));
    end component;

    signal x : std_logic_vector (Wt-1 downto 0);
    signal y : std_logic_vector (2*Wt-1 downto 0);
    -- variables necesarias
    shared variable     var_x      : std_logic_vector (Wt-1 downto 0);
    shared variable     errors     : integer := 0;

begin

    dut : sign_extension
    port map (x => x,
              y => y);

    random : process
    begin
        report_begin;
        for k in 0 to 15 loop
            var_x := random_vector(Wt);
            x <= var_x;
            wait for 5 ns;
        end loop;
        report_pass_fail(errors);
        report_end;
        wait;
    end process;

end tb;