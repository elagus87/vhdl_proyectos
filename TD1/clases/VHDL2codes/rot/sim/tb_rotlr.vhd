library ieee;
use ieee.std_logic_1164.all;

entity tb_rotlr is
end tb_rotlr;

architecture tb of tb_rotlr is
    component rotlr
        port (a    : in std_logic_vector (7 downto 0);
              mode : in std_logic;
              o    : out std_logic_vector (7 downto 0));
    end component;

    signal a    : std_logic_vector (7 downto 0);
    signal mode : std_logic;
    signal o    : std_logic_vector (7 downto 0);

begin

    dut : rotlr
    port map (a    => a,
              mode => mode,
              o    => o);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        a <= "01001011";
        mode <= '0';
        wait for 5 ns;
        mode <= '1';
        wait for 5 ns;
        wait;
    end process;

end tb;