library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity add_sub_sig is
generic(W:natural:=3);
  port (
  	a : in  std_logic_vector(W-1 downto 0);
	b : in  std_logic_vector(W-1 downto 0);
	m : in  std_logic;
	r : out std_logic_vector(W-1 downto 0);
	ov: out std_logic
  	) ;
end add_sub_sig ;

architecture data_flow of add_sub_sig is
	signal s_a: signed(W-1 downto 0);
	signal s_b: signed(W-1 downto 0);
	signal s_r: signed(W-1 downto 0);

	signal v_ctrl : std_logic_vector(2 downto 0);
	signal v_add : std_logic;
	signal v_sub : std_logic;

begin
    -- interpreto
	s_a <= signed(a);
	s_b <= signed(b);
    -- opero
	s_r <= (s_b + s_a) when m='0' else
		   (s_b - s_a);
    -- vuelvo a nivel logico hacia la salida
    r <= std_logic_vector(s_r);
    -- signal de signos
	v_ctrl <= s_r(W-1) & s_b(W-1) & s_a(W-1);
	--overflow por suma
	with v_ctrl select
		v_add <= '1' when "100"|"011",
			     '0' when others;
	--overflow por resta
	with v_ctrl select
		v_sub <= '1' when "010"|"101",
                 '0' when others;
    -- selecciono con modo
	ov <= v_add when m='0' else v_sub;
 
end architecture data_flow;