library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity tb_add_sub_sig is
end tb_add_sub_sig;

architecture tb of tb_add_sub_sig is

    component add_sub_sig
    generic(w:natural:=3);
        port (a  : in std_logic_vector (w-1 downto 0);
              b  : in std_logic_vector (w-1 downto 0);
              m  : in std_logic;
              r  : out std_logic_vector (w-1 downto 0);
              ov : out std_logic);
    end component;

    constant Wt : natural:= 3;

    signal a  : std_logic_vector (Wt-1 downto 0);
    signal b  : std_logic_vector (Wt-1 downto 0);
    signal m  : std_logic;
    signal r  : std_logic_vector (Wt-1 downto 0);
    signal ov : std_logic;

begin

    dut : add_sub_sig
    port map (a  => a,
              b  => b,
              m  => m,
              r  => r,
              ov => ov);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        a <= (others => '0');
        b <= (others => '0');
        m <= '0';
        wait for 10 ns;
        a <= std_logic_vector(to_signed(2,Wt));
        b <= std_logic_vector(to_signed(2,Wt));
        wait for 10 ns;
        a <= std_logic_vector(to_signed(2,Wt));
        b <= std_logic_vector(to_signed(1,Wt));
        wait for 10 ns;
        a <= std_logic_vector(to_signed(-2,Wt));
        b <= std_logic_vector(to_signed(2,Wt));
        wait for 10 ns;
        a <= std_logic_vector(to_signed(-1,Wt));
        b <= std_logic_vector(to_signed(-4,Wt));
        wait for 10 ns;
        m <= '1';
        a <= std_logic_vector(to_signed(-2,Wt));
        b <= std_logic_vector(to_signed(3,Wt));
        wait for 10 ns;
        a <= std_logic_vector(to_signed(-3,Wt));
        b <= std_logic_vector(to_signed(3,Wt));
        wait for 10 ns;
        a <= std_logic_vector(to_signed(-2,Wt));
        b <= std_logic_vector(to_signed(-1,Wt));
        wait for 10 ns;
        a <= std_logic_vector(to_signed(-1,Wt));
        b <= std_logic_vector(to_signed(3,Wt));
        wait for 10 ns;
        -- EDIT Add stimuli here
        wait;
    end process;

end tb;


