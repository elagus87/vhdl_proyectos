library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mult is
generic(W:natural:=4);
  port (
  	a : in  std_logic_vector(W-1 downto 0);
	b : in  std_logic_vector(W-1 downto 0);
	r : out std_logic_vector(2*W-1 downto 0)
  	) ;
end mult ;

architecture data_flow of mult is
	signal u_a: unsigned(W-1 downto 0);
	signal u_b: unsigned(W-1 downto 0);
	signal u_r: unsigned(2*W-1 downto 0);
begin
--para poder operar aritmeticamente
	u_b <= unsigned(b);
	u_a <= unsigned(a);
 
	u_r <= u_a*u_b ;
	  r <= std_logic_vector(u_r);
 
end architecture data_flow;