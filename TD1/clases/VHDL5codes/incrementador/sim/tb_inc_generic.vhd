library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-- paquetes creados por ing. Fernando Orge
--use work.random_stim_pkg.all;
-- use work.report_pkg.all;
-- esquema general de test:
-- autor: ing. Pablo Caldirola

entity tb_inc_generic is
end entity tb_inc_generic;

architecture tb of tb_inc_generic is
  -- constants
  constant INTERVAL: time :=100 ns;
  constant Wt : natural:= 4;
 -- dut
    component inc_generic
    generic(W:natural:=Wt);
        port (bin: in std_logic_vector (W-1 downto 0);
            bin_inc: out std_logic_vector(W downto 0));
    end component; 

--inputs 
    signal bin_t : std_logic_vector(Wt-1 downto 0) := (others => '0');
 --outputs 
    signal bin_inc_t : std_logic_vector(Wt downto 0);

begin 
-- instantiate the unit under test (dut) 

    dut: inc_generic
    port map (bin => bin_t,
            bin_inc => bin_inc_t
            );

    -- stimulus process 
    bin_proc: process 
        variable count: unsigned(Wt downto 0) := (others => '0');
        begin 
            if count < 2**Wt then
                for k in 0 to Wt-1 loop bin_t(k) <= count(k);
                end loop;
                wait for INTERVAL;
                count:=count+1;
            else
            -- terminate simulation
--                report_end;
                wait;
            end if; 
        end process bin_proc; 
end architecture tb; 
