library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity arith_unit is
generic(W:natural:=16);
  port (
		i_op1:in std_logic_vector(W-1 downto 0);
		i_op2:in std_logic_vector(W-1 downto 0);
		i_opcode :in std_logic_vector(3 downto 0);
		o_result:out std_logic_vector(W-1 downto 0);
		o_flags :out std_logic_vector(3 downto 0)
  ) ;
end entity ; -- arith_unit

architecture arch of arith_unit is
	signal i_op1_s,i_op2_s: signed(W-1 downto 0);
	signal o_result_s: signed(W-1 downto 0);
	signal v_ctrl : std_logic_vector(2 downto 0);
	signal v_add : std_logic;
	signal v_sub : std_logic;
	signal m,ov,z,neg,eq : std_logic;
	signal add_sub,comp : std_logic;

	constant zero : signed(W-1 downto 0):=(others => '0');
begin
    -- Suma y resta --
	i_op1_s <= signed(i_op1);
	i_op2_s <= signed(i_op2);
	
	with i_opcode(3 downto 1) select
	o_result_s <= i_op1_s + i_op2_s when "100",
				i_op1_s - i_op2_s when "101",
				zero when others;

	--*********************************--
	-- signal de signos
	v_ctrl <= o_result_s(W-1) & i_op1_s(W-1) & i_op2_s(W-1);
	--overflow por suma
	with v_ctrl select
	v_add <= '1' when "100"|"011",
			 '0' when others;
	--overflow por resta
	with v_ctrl select
	v_sub <= '1' when "010"|"101",
			 '0' when others;

	m <= '1' when i_opcode(3 downto 1) = "101" else
         '0';
	ov <= v_add when m='0' else v_sub;

	--***********otros o_flags***********--
	  z <= '1' when (o_result_s = zero) else
	       '0';
	neg <= '1' when o_result_s(W-1)='1' else
		   '0';
	 eq <= '1' when i_op2_s=i_op1_s else
	 	   '0';

	--*********************************--
	 add_sub <= '1' when i_opcode(3 downto 2)="10" else '0';
	    comp <= '1' when i_opcode(3 downto 1)="110" else '0';

     o_flags <= (ov & '0' & neg & z) when add_sub='1' else
                ('0' & eq & '0' & '0') when comp='1' else
                "0000";    
	o_result <= std_logic_vector(o_result_s);
	 

end arch ; -- arch
