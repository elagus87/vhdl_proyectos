library IEEE;
use ieee.std_logic_1164.all;
-- testeo ok
entity logic_unit is
generic(W:natural:=16);
  port (
		i_op1 :in std_logic_vector(W-1 downto 0);
		i_op2 :in std_logic_vector(W-1 downto 0);
		i_opcode :in std_logic_vector(3 downto 0);
		o_result :out std_logic_vector(W-1 downto 0)
		) ;
end entity ; -- logic_unit

architecture data_flow of logic_unit is
begin

	with i_opcode select
	o_result <= i_op1 when "0000",
				i_op1 when "0001",
				i_op1 and i_op2 when "0010",
				i_op1 or i_op2 when "0011",
				i_op1 xor i_op2 when "0100",
				not(i_op1) when "0101",
				i_op1(W-2 downto 0) & '0' when "0110",
				'0' & i_op1(W-1 downto 1) when "0111",
				i_op1 when others;
end data_flow ; -- arch