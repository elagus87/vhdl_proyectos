library ieee;
use ieee.std_logic_1164.all;

entity piso_reg is
generic(K: natural:=3);
  port (
        clk: in std_logic;
        rst: in std_logic;
        ld : in std_logic;
        en : in std_logic;
        d  : in std_logic_vector(K-1 downto 0) ;
        q  : out std_logic 
  ) ;
end piso_reg;

architecture behavioral of piso_reg is
    signal s_k_reg,s_k_next: std_logic_vector(K-1 downto 0) ;
begin

    -- logica secuencial
    state_register:process(clk,rst)
    begin
        if (rst='1') then
            s_k_reg <= (others => '0');
        elsif (rising_edge(clk)) then
            s_k_reg <= s_k_next;
        end if;
    end process;

    -- logica de estado futuro
    s_k_next <= d when ld='1' else
                s_k_reg(K-2 downto 0) & s_k_reg(K-1) when en='1' else
                s_k_reg;
    -- logica de salida
    q <= s_k_reg(K-1);            
end behavioral ; -- behavioral