library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
-- paquetes creados por Fernando Orge
use work.random_stim_pkg.all;
use work.report_pkg.all;

entity tb_piso_reg is
end tb_piso_reg;

architecture tb of tb_piso_reg is
    constant Wt : natural:= 8;
    component piso_reg
    generic(K:natural:=Wt);
        port (clk : in std_logic;
              rst : in std_logic;
              ld  : in std_logic;
              en  : in std_logic;
              d   : in std_logic_vector (K-1 downto 0);
              q   : out std_logic);
    end component;

    signal clk : std_logic;
    signal rst : std_logic;
    signal ld  : std_logic;
    signal en  : std_logic;
    signal d   : std_logic_vector (Wt-1 downto 0);
    signal q   : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : piso_reg
    port map (clk => clk,
              rst => rst,
              ld  => ld,
              en  => en,
              d   => d,
              q   => q);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        ld <= '0';
        en <= '0';
        d <= (others => '0');
        rst <= '1';
        wait for 10 ns;
        rst <= '0';
        d <= "10101010";
        wait for 10 ns;
        en <='1';
        ld <='1';
        wait for 10 ns;
        ld <='0';
        wait for tbPeriod*(Wt+1);
        en <= '0';
        wait for tbPeriod*2;
        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;