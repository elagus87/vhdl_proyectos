library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
  port (
        i_clk : in std_logic;
        i_arst : in std_logic;
        i_srst : in std_logic;
        i_en : in std_logic;
        o_tc : out std_logic;
        o_cnt : out std_logic_vector(5 downto 0)
        ) ;
end counter ;

architecture arch of counter is
    signal count_reg,count_next:unsigned(5 downto 0) ;
    constant zeros:unsigned(5 downto 0):=(others=>'0');
    signal comp:std_logic;
begin
    registros : process( i_clk,i_arst )
    begin
        if i_arst='1' then
            count_reg<=(others=>'0');
        elsif rising_edge(i_clk) then
            count_reg<=count_next;  
        end if ;
    end process ; -- registros

    ---------------------------------------
    count_next <= count_reg when i_en='0' else
                  zeros when i_srst='1' else
                  count_reg+1;

    comp <= '1' when (count_reg="111111") else '0';
    --o_tc <= '1' when (comp and i_en)='1' else '0';
    o_tc <= comp and i_en;
    o_cnt <= std_logic_vector(count_reg) ;
                  
end architecture ; -- arch