library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity datapath is
  port (
        i_clk : in std_logic;
        i_arst : in std_logic;
        i_add : in std_logic;
        i_dest : in std_logic_vector(1 downto 0) ;
        i_source : in std_logic_vector(1 downto 0) ;
        i_data : in std_logic_vector(15 downto 0) ;
        o_r0,o_r1,o_r2,o_r3:out std_logic_vector(15 downto 0)
        ) ;
end datapath ;

-- r(i_dest)_reg <= r(i_source)_reg + i_data
-- | i_add = 0 | carry_next <= carry reg |
-- |           | r(W)_next <= r(W)_reg   |

architecture arch of datapath is
    signal s_carry:std_logic_vector(3 downto 0) ;
    signal s_idx:integer:=0;
    signal s_sum:unsigned(16 downto 0) ;
    signal s_data:unsigned(16 downto 0) ;
    signal sel_register:std_logic_vector(16 downto 0) ;
    signal carry_reg, carry_next:std_logic;
   -- registros
    type slv_array_t is array (3 downto 0)
    of std_logic_vector(15 downto 0);
    --signal del registro
    signal r_reg,r_next: slv_array_t; --4 reg de 16 bits en uno
begin
    registros : for k in 0 to 3 generate
        --a partir de aca pienso en cada reg de 16 bits
        individual : process( i_clk,i_arst )
        begin
            if i_arst='1' then
                r_reg(k)<=(others=>'0');
            elsif rising_edge(i_clk) then
                r_reg(k)<=r_next(k);  
            end if ;
        end process ; -- individual
    end generate ; -- registros

    carry_out : process( i_clk,i_arst )
    begin
        if i_arst='1' then
            carry_reg<='0';
        elsif rising_edge(i_clk) then
            carry_reg<=carry_next;  
        end if ;
    end process ; -- carry_out

    -----------------------------------------------
    with i_source select
    sel_register <= '0' & r_reg(0) when "00",
                    '0' & r_reg(1) when "01",
                    '0' & r_reg(2) when "10",
                    '0' & r_reg(3) when others;

    s_data <= '0' & unsigned(i_data);
    s_sum <= unsigned(sel_register) + s_data;

    carry_next <= s_sum(16) when i_add='1' else carry_reg;

    -----------------------------------------------

    s_idx <= to_integer( unsigned(i_dest));
    estado_futuro : for k in 0 to 3 generate
        r_next(k)<= r_reg(k) when i_add='0' else
                    std_logic_vector(s_sum(15 downto 0)) when s_idx=k else
                    r_reg(k);
    end generate ; -- estado_futuro
    -----------------------------------------------
    o_r0 <= r_reg ( 0 ) ;
    o_r1 <= r_reg ( 1 ) ;
    o_r2 <= r_reg ( 2 ) ;
    o_r3 <= r_reg ( 3 ) ;

end architecture ; -- arch