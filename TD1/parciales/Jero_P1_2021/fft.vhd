library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity myFft is
Port ( 
        t: in std_logic;
        ena: in std_logic;
        rst: in std_logic;
        clk: in std_logic;
        q: out std_logic);
end myFft;

architecture arch of myFft is
    signal q_reg,q_next:std_logic;
begin
    -- FFD basico
    process(clk,rst)
        begin
        if rst='1' then
            q_reg <= '0';
        elsif rising_edge(clk) then
            q_reg <=q_next;
        end if;
    end process;

    -- logica de estado futuro, descriir q_next
    with ena select
    q_next <= q_reg when '0',
              t xor q_reg when others;  

    -- salida
    q <= q_reg;

end architecture ; -- myFft