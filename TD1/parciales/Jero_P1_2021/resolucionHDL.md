# ej 4

## FFD
Explicación de funcionamiento
| D | Q_reg |
| --|--|
| 0 | 0 |
| 1 | 1 |

| D | Q_next |
| --|--|
| 0 | 0 |
| 1 | 1 |

Q_next = D

# FFT
| T | Q_reg | Q_next |
| --|--|--|
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 0 |

Q_next = T xor Q_reg

### pero, con ena
si ena='1' => funciono normalmente
caso contrario, dejo la salida igual => Q_next=Q_reg