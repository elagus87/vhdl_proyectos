library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity myCmp is
    Generic (N: integer := 4);
Port ( 
            a: in std_logic_vector (N-1 downto 0);
            b: in std_logic_vector (N-1 downto 0);
            c: in std_logic_vector (N-1 downto 0);
        mayor: out std_logic_vector (N-1 downto 0);
        medio: out std_logic_vector (N-1 downto 0);
        menor: out std_logic_vector (N-1 downto 0));
end myCmp;

architecture behavioral of myCmp is
    signal a_s:signed(N-1 downto 0);
    signal b_s:signed(N-1 downto 0);
    signal c_s:signed(N-1 downto 0);
    signal menor_s:signed(N-1 downto 0);

begin
    a_s <= signed(a);
    b_s <= signed(b);
    c_s <= signed(c);
    menor <= std_logic_vector(menor_s);
    -- mux con prioridad 
    menor_s <= a_s when a_s<b_s and a_s<c_s else
             b_s when b_s<a_s and b_s<c_s else
             c_s;


    amb <= '1' when a<b else '0'; 
    amc <= '1' when a<c else '0'; 
    cmb <= '1' when c<b else '0'; 

    with amb & amc & cmb select
    menor <= a when "11",
             b when "01",
             c when others;
    
             
	
end architecture behavioral;