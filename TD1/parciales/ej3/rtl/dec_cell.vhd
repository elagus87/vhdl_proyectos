library ieee;
use ieee.std_logic_1164.all;

entity dec_cell is
  port (
        i_a : in std_logic;
        i_nbw : in std_logic;
        o_nbw : out std_logic;
        o_r : out std_logic
        );
end dec_cell ;

architecture arch of dec_cell is
begin

    o_nbw <= i_a or i_nbw;
    o_r <= i_a xnor i_nbw;

end architecture ; -- arch