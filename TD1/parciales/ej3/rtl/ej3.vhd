library ieee;
use ieee.std_logic_1164.all;

entity dec is
  port (
        i_a : in std_logic_vector(3 downto 0) ;
        i_nbw : in std_logic;
        o_nbw : out std_logic;
        o_r :out std_logic_vector(3 downto 0)
        );
end dec ;

architecture arch of dec is
    signal bw:std_logic_vector(4 downto 0) ;

    component dec_cell 
    port (
        i_a : in std_logic;
        i_nbw : in std_logic;
        o_nbw : out std_logic;
        o_r : out std_logic);
    end component;
begin

    celdas : for k in 0 to 3 generate
        cells :dec_cell
        port map(   i_a =>i_a(k),
                    i_nbw =>bw(k),
                    o_nbw =>bw(k+1),
                    o_r =>o_r(k)
                );
    end generate ; -- celdas

    o_nbw <= bw(4);

end architecture ; -- arch