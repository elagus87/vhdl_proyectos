library ieee;
use ieee.std_logic_1164.all;

entity fsm is
  port (
        i_clk : in std_logic;
        i_arst : in std_logic;
        o_s : out std_logic_vector(3 downto 0)
        );
end fsm ;

architecture arch of fsm is
  type state_type is (S0,S1,S2,S3,S4,S5,S6,S7);
  signal s_reg, s_next : state_type;
  attribute FSM_ENCODING:string;
  attribute FSM_ENCODING of state_type: type is "auto";  
begin
    registro : process( i_clk,i_arst )
    begin
        if i_arst='1' then
            s_reg<=S0;
        elsif rising_edge(i_clk) then
            s_reg<=s_next;  
        end if ;
    end process ; -- registro

    next_st : process( s_reg )
    begin
        case( s_reg ) is
            when S0 => s_next<=S1;
                       o_s <= "1000"; 
            when S1 => s_next<=S2;
                       o_s <= "0100"; 
            when S2 => s_next<=S3;
                       o_s <= "0010"; 
            when S3 => s_next<=S4;
                       o_s <= "0001"; 
            when S4 => s_next<=S5;
                       o_s <= "0010"; 
            when S5 => s_next<=S6;
                       o_s <= "0100"; 
            when S6 => s_next<=S7;
                       o_s <= "0010";         
            when others => s_next<=S0;
                           o_s <= "0001";         
        end case ;
    end process ; -- next_st

end architecture ; -- arch