library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity comp_ca2 is
generic(W: natural := 4);
    Port ( a : in  STD_LOGIC_VECTOR (W-1 downto 0);
           b : in  STD_LOGIC_VECTOR (W-1 downto 0);
           o : out STD_LOGIC_VECTOR (W-1 downto 0));
end comp_ca2;

architecture data_flow of comp_ca2 is
	signal s_a: signed(W-1 downto 0);
	signal s_b: signed(W-1 downto 0);
	signal may: signed(W-1 downto 0);
begin

	s_a <= signed(a);
	s_b <= signed(b);

	may <= s_a when (s_a >= s_b) else s_b;

	o <= STD_LOGIC_VECTOR(may)
end data_flow;