LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity comp_biased is
generic(W:natural:=8);
  port (
		a :in std_logic_vector(W-1 downto 0);
		b :in std_logic_vector(W-1 downto 0);
		o :in std_logic_vector(W-1 downto 0)
  		) ;
end comp_biased ;

architecture data_flow of comp_biased is
	signal a_bias: std_logic_vector(W-1 downto 0);
	signal b_bias: std_logic_vector(W-1 downto 0);
begin
	a_bias <= not(a(W-1)) & a(W-2 downto 0);
	b_bias <= not(b(W-1)) & b(W-2 downto 0);

	o <= a when (a_bias>=b_bias) else b;
end architecture ; 