LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity comp_sel is
generic(W:natural:=8);
  port (
		a :in std_logic_vector(W-1 downto 0);
		b :in std_logic_vector(W-1 downto 0);
		o :in std_logic_vector(W-1 downto 0)
  		) ;
end comp_sel ; 

architecture data_flow of comp_sel is
begin
	o <= a when (a>=b) else b;
end architecture ;