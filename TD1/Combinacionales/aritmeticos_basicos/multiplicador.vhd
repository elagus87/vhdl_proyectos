library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity multiplicador is
generic(W:natural:=3);
  port (
	    b : in  std_logic_vector(W-1 downto 0);
		a : in  std_logic_vector(W-1 downto 0);
	    r : out std_logic_vector(2*W-1 downto 0)
  	) ;
end multiplicador ; 

architecture data_flow of multiplicador is
	signal u_a: unsigned(W-1 downto 0);
	signal u_b: unsigned(W-1 downto 0);
	signal u_r: unsigned(2*W-1 downto 0);
begin
--para poder operar aritmeticamente
	u_b <= unsigned(b);
	u_a <= unsigned(a);

	u_r <= u_a*u_b ;
	  r <= std_logic_vector(u_r);

end architecture data_flow;