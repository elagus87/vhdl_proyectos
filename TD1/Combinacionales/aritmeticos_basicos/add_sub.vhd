library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity add_sub is
generic(W:natural:=4);
  port (
  		a : in  std_logic_vector(W-1 downto 0);
	    b : in  std_logic_vector(W-1 downto 0);
		m : in  std_logic;
	    r : out std_logic_vector(W-1 downto 0);
	    co: out  std_logic
  	) ;
end add_sub ;

architecture data_flow of add_sub is
	signal u_a: unsigned(W downto 0);
	signal u_b: unsigned(W downto 0);
	signal u_r: unsigned(W downto 0);
begin
	u_a <= '0' & unsigned(a);
	u_b <= '0' & unsigned(b);

	u_r <= (u_b + u_a) when m='0' else
		   (u_b - u_a);

	  r <= std_logic_vector(u_r(W-1 downto 0));
	 co <= std_logic(u_r(W));
end architecture data_flow;