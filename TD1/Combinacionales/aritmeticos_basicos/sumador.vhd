library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sumador is
generic(W:natural:=3);
  port (
	    b : in  std_logic_vector(W-1 downto 0);
		a : in  std_logic_vector(W-1 downto 0);
	    r : out std_logic_vector(W-1 downto 0);
	    co: out std_logic
  	) ;
end sumador ; -- sumador

architecture data_flow of sumador is
	signal u_a: unsigned(W downto 0);
	signal u_b: unsigned(W downto 0);
	signal u_r: unsigned(W downto 0);
begin
--para poder operar aritmeticamente
	u_b <= '0' & unsigned(b);
	u_a <= '0' & unsigned(a);

	u_r <= (u_b + "0001");
	--u_r <= (u_b + "0101");
	--u_r <= (u_b + u_a);

	  r <= std_logic_vector(u_r(W-1 downto 0));
	  r <= std_logic(u_r(W));

end architecture data_flow;