LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity comp_basic is
generic(W:natural:=8);
  port (
		a :in std_logic_vector(W-1 downto 0);
		b :in std_logic_vector(W-1 downto 0);
		o :in std_logic_vector(W-1 downto 0)
  		) ;
end comp_basic ;

architecture data_flow of comp_basic is
begin
	o <= '1' when (a>=b) else '0';
end architecture ;