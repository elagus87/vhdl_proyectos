LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY comp_ca2_tb IS
END comp_ca2_tb;
 
ARCHITECTURE behavior OF comp_ca2_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT comp_ca2
    generic(W:natural:=4);
    PORT(
         a : IN  std_logic_vector(W-1 downto 0);
         b : IN  std_logic_vector(W-1 downto 0);
         m : IN  std_logic;
         o : OUT  std_logic_vector(W-1 downto 0)
        );
    END COMPONENT;
    
   constant Wt : natural:= 4;
   --Inputs
   signal a_t : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal b_t : std_logic_vector(Wt-1 downto 0) := (others => '0');
   signal m_t : std_logic := '0';

 	--Outputs
   signal o_t : std_logic_vector(Wt-1 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: comp_ca2 PORT MAP (
          a => a_t,
          b => b_t,
          m => m_t,
          o => o_t
        );

   -- Stimulus process
   stim_proc: process
   begin		
    a_t <= std_logic_vector(TO_SIGNED(7,Wt));
    b_t <= std_logic_vector(TO_SIGNED(3,Wt));
    m_t <= '1';
    wait for 20 ns;
    m_t <= '0';
    wait for 20 ns;
    ----------------------
    a_t <= std_logic_vector(TO_SIGNED(-7,Wt));
    b_t <= std_logic_vector(TO_SIGNED(-3,Wt));
    m_t <= '1';
    wait for 20 ns;
    m_t <= '0';
    wait for 20 ns;
    ----------------------
    a_t <= std_logic_vector(TO_SIGNED(5,Wt));
    b_t <= std_logic_vector(TO_SIGNED(-2,Wt));
    m_t <= '1';
    wait for 20 ns;
    m_t <= '0';
    wait for 20 ns;
        ----------------------
    a_t <= std_logic_vector(TO_SIGNED(-7,Wt));
    b_t <= std_logic_vector(TO_SIGNED(-7,Wt));
    m_t <= '1';
    wait for 20 ns;
    m_t <= '0';
    wait for 20 ns;
    assert false
    report "fin de la simulacion" 
    severity failure;
   end process;

END;
