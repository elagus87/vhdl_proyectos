----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:09:18 10/16/2019 
-- Design Name: 
-- Module Name:    sumador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sumador is
generic(W:natural:=3);
port(
      a:in std_logic_vector(W-1 downto 0);
      b:in std_logic_vector(W-1 downto 0);
      r:out std_logic_vector(W-1 downto 0);
      co:out std_logic
);
end sumador;

architecture data_flow of sumador is
  signal u_a: unsigned(W downto 0);
  signal u_b: unsigned(W downto 0);
  signal u_r: unsigned(W downto 0);
begin
  u_b <= '0' & unsigned(b);
  u_a <= '0' & unsigned(a);
  
  u_r <= u_b + u_a;
  r <= std_logic_vector(u_r(W-1 downto 0));
  co <= std_logic(u_r(W));
  

end data_flow;

