library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_bin2oh is
    end tb_bin2oh;
    
architecture tb of tb_bin2oh is

    constant W : natural:= 3;
        signal bin : std_logic_vector (W-1 downto 0);
        signal oh  : std_logic_vector (2**W -1 downto 0);
    
    begin
        -- seleccionar la arquitectura
        --...................(architecture)
        uut: entity work.bin2oh(for_arch)
        GENERIC MAP (W =>W)
         port map (bin => bin,
                   oh  => oh);
    
        stimuli : process
        begin
            bin <= (others => '0'); 
            wait for 5 ns;   
            bin <= "001"; 
            wait for 5 ns;   
            bin <= "010"; 
            wait for 5 ns;   
            bin <= "011"; 
            wait for 5 ns;   
            bin <= "100"; 
            wait for 5 ns;   
            bin <= "101"; 
            wait for 5 ns;   
            bin <= "110"; 
            wait for 5 ns;   
            bin <= "111"; 
            wait for 5 ns; 
            wait;
        end process;
    
end tb;