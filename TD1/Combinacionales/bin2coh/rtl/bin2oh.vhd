library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bin2oh is
generic(W:natural:=3);
  port (
        bin:in std_logic_vector(W-1 downto 0) ;
        oh:out std_logic_vector(2**W -1 downto 0)
       );
end bin2oh ;

architecture simple_arch of bin2oh is
    signal addr:std_logic_vector(2**W -1 downto 0):=(others=>'0') ;
begin
    demux : process( bin )
    begin
        oh <= (others=>'0');
        oh( to_integer(unsigned(bin)) )<='1';    
    end process ; -- demux             
end architecture ; -- arch

------------------otra arquitectura-----------------------------
architecture for_arch of bin2oh is
    signal addr:std_logic_vector(2**W -1 downto 0):=(others=>'0') ;
    signal idx:integer:=0;
begin
    idx <= to_integer(unsigned(bin));

    demux : for i in 0 to 2**W -1 generate
        addr(i)<='1' when i=idx else'0';
    end generate ; -- demux      
    
    oh<=addr;
end architecture ; -- arch