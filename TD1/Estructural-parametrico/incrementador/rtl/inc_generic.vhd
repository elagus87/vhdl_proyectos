library ieee;
use ieee.std_logic_1164.all;

entity inc_generic is
  generic(W: natural:=4);  
  port (
        bin : in  std_logic_vector(W-1 downto 0) ;
        bin_inc : out std_logic_vector(W downto 0) 
       );
end inc_generic;

architecture parametric of inc_generic is
    signal carry:std_logic_vector(W downto 0);-- un bit extra
begin
     --condición de frontera
    carry(0) <= '1';
    --instancias de las celdas incrementadoras de 1 bit
    celdas: for k in 0 to W-1 generate
        bin_inc(k) <= bin(k) xor carry(k);-- primer salida de cada celda
        carry(k+1) <= bin(k) and carry(k);-- segunda salida
    end generate ; -- celdas
    -- ultima salida, fuera del for generate
    bin_inc(W) <= carry(W);
end architecture ;