library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-- paquetes creados por ing. Fernando Orge
use work.random_stim_pkg.all;
use work.report_pkg.all;

entity tb_alu2 is
end tb_alu2;

architecture tb of tb_alu2 is
    constant wt : natural:= 16;
    constant INTERVAL: time :=5 ns;
    
    component alu
    generic(W:natural:=wt);
    port (  op1    : in std_logic_vector (W-1 downto 0);
            op2    : in std_logic_vector (W-1 downto 0);
            opcode : in std_logic_vector (3 downto 0);
            result : out std_logic_vector (W-1 downto 0);
            flags  : out std_logic_vector (3 downto 0));
    end component;

    signal op1    : std_logic_vector (Wt-1 downto 0);
    signal op2    : std_logic_vector (Wt-1 downto 0);
    signal opcode : std_logic_vector (3 downto 0);
    signal result : std_logic_vector (Wt-1 downto 0);
    signal flags  : std_logic_vector (3 downto 0);

begin

	-- Instantiate the Unit Under Test (UUT)
    uut: entity work.alu(entity_instance)
    GENERIC MAP (W =>W)
     port map (op1    => op1,
              op2    => op2,
              opcode => opcode,
              result => result,
              flags  => flags);

    -- stimulus process 
    opcode_proc: process 
        begin 
            op1 <= x"0FFF";
            op2 <= x"3A11";
            opcode <="0000";
            wait for INTERVAL;
            opcode <="0010";
            wait for INTERVAL;
            opcode <="0011";
            wait for INTERVAL;
            opcode <="0100";
            wait for INTERVAL;
            opcode <="0101";
            wait for INTERVAL;
            opcode <="0110";
            wait for INTERVAL;
            opcode <="0111";
            wait for INTERVAL;
            -- aritmetica
            opcode <="1000";
            wait for INTERVAL;
            opcode <="1010";
            wait for INTERVAL;
            opcode <="1100";
            wait for INTERVAL;
            --extra
            opcode <="1111";
            wait ;
        end process opcode_proc;
end tb;
