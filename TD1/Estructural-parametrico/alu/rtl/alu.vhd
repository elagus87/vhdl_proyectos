library IEEE;
use ieee.std_logic_1164.all;

entity alu is
    generic(W:natural:=16);
    port (
          op1:in std_logic_vector(W-1 downto 0);
          op2:in std_logic_vector(W-1 downto 0);
          opcode :in std_logic_vector(3 downto 0);
          result:out std_logic_vector(W-1 downto 0);
          flags :out std_logic_vector(3 downto 0)
    ) ;
end alu;

architecture component_port of alu is
    signal r_arith,r_logic:std_logic_vector(W-1 downto 0) ;
---------------------------------   
    component logic_unit 
    generic(W:natural:=16);
      port (
          i_op1 :in std_logic_vector(W-1 downto 0);
          i_op2 :in std_logic_vector(W-1 downto 0);
          i_opcode :in std_logic_vector(3 downto 0);
          o_result :out std_logic_vector(W-1 downto 0)
          ) ;
    end component ; -- logic_unit

    component arith_unit
    generic(W:natural:=16);
      port (
          i_op1:in std_logic_vector(W-1 downto 0);
          i_op2:in std_logic_vector(W-1 downto 0);
          i_opcode :in std_logic_vector(3 downto 0);
          o_result:out std_logic_vector(W-1 downto 0);
          o_flags :out std_logic_vector(3 downto 0)
      ) ;
      end component; 
----------------------------------------------      
begin
---- Mux de salida ----    
    result <= r_logic when opcode(3)='0' else
              r_arith;
----- Componentes -----
    arith: arith_unit
    generic map(W=>W)
    port map (
        -- formal => real
        -- => operador de asociacion
           i_op1    => op1   ,
           i_op2    => op2   ,
           i_opcode => opcode,
           o_result => r_arith,
           o_flags  => flags
    );
    logic: logic_unit
    generic map(W=>W)
    port map (
           i_op1    => op1   ,
           i_op2    => op2   ,
           i_opcode => opcode,
           o_result => r_logic
    );
end architecture ; -- component_port

--#################################################--

architecture entity_instance of alu is
    signal r_arith,r_logic:std_logic_vector(W-1 downto 0) ;   
begin
---- Mux de salida ----    
    result <= r_logic when opcode(3)='0' else
              r_arith;
----- Componentes -----
arith: entity work.arith_unit
    generic map(W=>W)
    port map (
        i_op1    => op1   ,
        i_op2    => op2   ,
        i_opcode => opcode,
        o_result => r_arith,
        o_flags  => flags
    );

logic: entity work.logic_unit
    generic map(W=>W)
    port map (
           i_op1    => op1   ,
           i_op2    => op2   ,
           i_opcode => opcode,
           o_result => r_logic
    );

end architecture ; -- entity_instance