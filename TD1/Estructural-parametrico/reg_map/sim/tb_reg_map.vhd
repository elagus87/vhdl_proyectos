library ieee;
use ieee.std_logic_1164.all;

entity tb_reg_map is
end tb_reg_map;

architecture tb of tb_reg_map is

    component reg_map
        port (clk  : in std_logic;
              rst  : in std_logic;
              en   : in std_logic;
              srst : in std_logic;
              load : in std_logic_vector (2 downto 0);
              data : in std_logic_vector (bits-1 downto 0);
              r0   : out std_logic_vector (bits-1 downto 0);
              r1   : out std_logic_vector (bits-1 downto 0);
              r2   : out std_logic_vector (bits-1 downto 0);
              r3   : out std_logic_vector (bits-1 downto 0));
    end component;

    signal clk  : std_logic;
    signal rst  : std_logic;
    signal en   : std_logic;
    signal srst : std_logic;
    signal load : std_logic_vector (2 downto 0);
    signal data : std_logic_vector (bits-1 downto 0);
    signal r0   : std_logic_vector (bits-1 downto 0);
    signal r1   : std_logic_vector (bits-1 downto 0);
    signal r2   : std_logic_vector (bits-1 downto 0);
    signal r3   : std_logic_vector (bits-1 downto 0);

    constant TbPeriod : time := 5 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : reg_map
    port map (clk  => clk,
              rst  => rst,
              en   => en,
              srst => srst,
              load => load,
              data => data,
              r0   => r0,
              r1   => r1,
              r2   => r2,
              r3   => r3);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        en <= '0';
        srst <= '0';
        load <= (others => '0');
        data <= (others => '0');

        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        rst <= '1';
        wait for 5 ns;
        rst <= '0';
        wait for 5 ns;
        -- EDIT Add stimuli here
        wait for TbPeriod;
        en <= '1';
        load <= "00";
        data <= (others => '1');
        wait for TbPeriod;
        load <= "01";
        wait for TbPeriod;
        load <= "10";
        wait for TbPeriod;
        load <= "11";
        wait for TbPeriod;      

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;