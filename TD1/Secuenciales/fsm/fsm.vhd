----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:18:29 10/17/2018 
-- Design Name: 
-- Module Name:    fsm - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fsm is
    Port ( clk : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           x1 : in  STD_LOGIC;
           x2 : in  STD_LOGIC;
           y : out  STD_LOGIC_VECTOR (1 downto 0);
           z : out  STD_LOGIC);
end fsm;

architecture Behavioral of fsm is

	signal state, next_state: STD_LOGIC_VECTOR(1 downto 0);

begin

	state_register:process(clk,rst)
	begin
		if (rst='1') then
			state <="10";
		elsif rising_edge(clk) then
			state <= next_state;
		end if;
	end process;
	
	next_state_proc:process(state,x1,x2)
	begin
	next_state<=state;
	case state is
		when "10" =>
			if x1='1' then next_state <="01";
			end if;
		when "01" => 
		if x2='0' then next_state<="10";
		elsif x2='1' then next_state<="11";
		end if;
		when "11" =>
			if x2='0' then next_state <="10";
			end if;
		when others => next_state <="10";
	end case;
	end process;
	
	out_logic:process(state,x1,x2)
	begin
		z<='0';
		case (state) is 
			when "01" =>
				if x2='0' then z<='1';
				end if;
			when "11" =>
				if x2 ='0' then z<='1';
				end if;
			when others =>
				z<='0';
		end case;
		end process;
		
	y<=state;
	
	
			

end Behavioral;

