-- Vhdl test bench created from schematic C:\Users\elagu\OneDrive\Documentos\vhdl_proyectos\siso\siso_4.sch - Thu Jun 27 14:01:29 2019
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY siso_4_siso_4_sch_tb IS
END siso_4_siso_4_sch_tb;
ARCHITECTURE behavioral OF siso_4_siso_4_sch_tb IS 

   COMPONENT siso_4
   PORT( clk	:	IN	STD_LOGIC; 
          rst	:	IN	STD_LOGIC; 
          en	:	IN	STD_LOGIC; 
          q_reg0	:	OUT	STD_LOGIC; 
          q_reg1	:	OUT	STD_LOGIC; 
          q_reg2	:	OUT	STD_LOGIC; 
          q_reg3	:	OUT	STD_LOGIC; 
          di	:	IN	STD_LOGIC);
   END COMPONENT;

   SIGNAL clk	:	STD_LOGIC;
   SIGNAL rst	:	STD_LOGIC;
   SIGNAL en	:	STD_LOGIC;
   SIGNAL q_reg0	:	STD_LOGIC;
   SIGNAL q_reg1	:	STD_LOGIC;
   SIGNAL q_reg2	:	STD_LOGIC;
   SIGNAL q_reg3	:	STD_LOGIC;
   SIGNAL di	:	STD_LOGIC;

BEGIN

   UUT: siso_4 PORT MAP(
		clk => clk, 
		rst => rst, 
		en => en, 
		q_reg0 => q_reg0, 
		q_reg1 => q_reg1, 
		q_reg2 => q_reg2, 
		q_reg3 => q_reg3, 
		di => di
   );

-- *** Test Bench - User Defined Section ***
   clock : PROCESS
   BEGIN
		clk<='0';
      WAIT for 10 ns;
		clk<='1';
      WAIT for 10 ns;
   END PROCESS;
-- *** End Test Bench - User Defined Section ***
   enable: PROCESS
   BEGIN
		en<='0';
      WAIT for 20 ns;
		en<='1';
		WAIT for 83 ns;
		en<='0';
      WAIT for 20 ns;
		en<='1';
      WAIT;
   END PROCESS;

	rst<='0';
   stimulus: PROCESS
   BEGIN
		di<='0';
      WAIT for 20 ns;
		di<='1';
      WAIT for 15 ns;
		di<='0';
      WAIT for 25 ns;
		di<='1';
      WAIT for 20 ns;
		di<='0';
      WAIT for 15 ns;
		di<='U';
      WAIT for 75 ns;
		
	   assert false
		report "fin"
		severity failure;
   END PROCESS;
END;
