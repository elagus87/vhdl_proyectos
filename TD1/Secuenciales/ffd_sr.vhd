LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity ffd_sr is
  port (
	   d :in std_logic;
	 clk :in std_logic;
    srst :in std_logic;
	   q :out std_logic) ;
end ffd_sr ;

architecture Behavioral of ffd_sr is
	signal q_reg,q_next : std_logic;
begin
	process ( clk )
		begin
		if( rising_edge (clk) ) then
			q_reg <= q_next;
		end if ;
	end process ;    
-- logica de estado futuro
	q_next <= '0' when srst='1' else
			   d; 
-- logica de salida
	q <= q_reg;
end architecture;