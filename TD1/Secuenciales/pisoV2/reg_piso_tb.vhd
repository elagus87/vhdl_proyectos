LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY reg_piso_tb IS
END reg_piso_tb;
 
ARCHITECTURE behavior OF reg_piso_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT reg_piso
    PORT(
         d : IN  std_logic_vector(7 downto 0);
         ld : IN  std_logic;
         en : IN  std_logic;
         clk : IN  std_logic;
         rst : IN  std_logic;
         q : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal d_t : std_logic_vector(7 downto 0) := (others => '0');
   signal ld_t : std_logic := '0';
   signal en_t : std_logic := '0';
   signal clk_t : std_logic := '0';
   signal rst_t : std_logic := '0';

 	--Outputs
   signal q_t : std_logic;

   -- Clock period definitions
   constant Tclk : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: reg_piso PORT MAP (
          d => d_t,
          ld => ld_t,
          en => en_t,
          clk => clk_t,
          rst => rst_t,
          q => q_t
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk_t <= '0';
		wait for Tclk/2;
		clk_t <= '1';
		wait for Tclk/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst_t <='1';
      wait for 100 ns;	
      rst_t <='0';
      d_t <= "11001100"; -- siempre en 1
      en_t <= '1';
      wait for Tclk;
      ld_t <= '1';
      wait for Tclk;
      ld_t <= '0';
      wait for Tclk*8;
      en_t <= '0';
      wait for Tclk*8;
      assert false
      report "fin de la simulacion" 
      severity failure;
   end process;

END;
