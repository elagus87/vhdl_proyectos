LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity reg_piso is
generic(K:natural:=8);
  port (
		   d :in std_logic_vector(K-1 downto 0);
		  ld :in std_logic;
		  en :in std_logic;
		 clk :in std_logic;
	     rst :in std_logic;
		  q :out std_logic);
end reg_piso ;

architecture beh of reg_piso is
signal sx : std_logic_vector (K downto 0);
begin
	sx ( 0 ) <= sx(K);
	process ( clk , rst )
	begin
		if( rst = '1' ) then
			sx (K downto 1) <= ( others => '0');
		elsif( rising_edge ( clk ) ) then
			if( ld = '1' ) then
				sx(K downto 1) <= d;
			elsif ( en = '1' ) then
				sx(K downto 1) <= sx (K-1 downto 0);
			end if ;
		end if ;
	end process ;
	q <= sx(K) ;
end beh;