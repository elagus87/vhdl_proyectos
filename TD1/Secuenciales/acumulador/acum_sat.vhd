
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity acum_sat is
    Port ( arst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           bin : in  STD_LOGIC_VECTOR (7 downto 0);
           mode : in  STD_LOGIC;
           acc : out  STD_LOGIC_VECTOR (15 downto 0);
           sat : out  STD_LOGIC);
end acum_sat;

architecture Behavioral of acum_sat is
-- signals propias del diagrama
	signal acc_reg, acc_next:STD_LOGIC_VECTOR(15 downto 0);
	signal suma:STD_LOGIC_VECTOR(15 downto 0);
	signal u_sum:unsigned(16 downto 0);
	signal s_sum:signed(15 downto 0);
-- constantes de saturacion
	constant s_max:signed(15 downto 0):=X"7FFF";
	constant u_max:unsigned(15 downto 0):=X"FFFF";
	constant s_min:signed(15 downto 0):=X"8000";
-- signals accesorias
	signal u_bin:unsigned(16 downto 0);
	signal u_acc:unsigned(16 downto 0);
	signal s_bin:signed(15 downto 0);
	signal s_acc:signed(15 downto 0);
	signal carry:STD_LOGIC;
	signal ov:STD_LOGIC_VECTOR(2 downto 0);

begin
-- suma no signada
	u_bin <= unsigned("000000000" & bin);
	u_acc <= unsigned('0' & acc_reg);
	u_sum <= u_bin + u_acc;
	carry <= u_sum(16);

-- suma signada
	s_bin <= signed("00000000"&bin) when bin(7)='0' else
				signed("11111111"&bin); 
	s_acc <= signed(acc_reg);
	s_sum <= s_bin + s_acc;
	ov <= s_sum(15)&s_bin(15)&s_acc(15);

-- logica de seleccion por mode
	suma <= STD_LOGIC_VECTOR(u_sum(15 downto 0)) when mode='0' else
			STD_LOGIC_VECTOR(s_sum);

-- seleccion en caso de saturacion
	acc_next <= STD_LOGIC_VECTOR(u_max) when carry ='1' and mode='0' else
			   	STD_LOGIC_VECTOR(s_max) when ov="100" and mode='1' else
					STD_LOGIC_VECTOR(s_min) when ov="011" and mode='1' else
					suma;

-- registro
	registro : process( clk,arst )
	begin
		if arst='1' then
			acc_reg<=(others=>'0');
		elsif rising_edge(clk) then
			acc_reg<=acc_next;		
		end if ;
	end process ; -- registro

-- logica de signal sat
	sat <= '1' when (carry='1')or(ov="100")or(ov="011") else
		    '0';

-- logica de salida
	acc <= acc_reg;

end Behavioral;

