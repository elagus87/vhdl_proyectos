LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity lfsr is
generic(K:natural:=4);
  port (
		 clk :in std_logic;
	     rst :in std_logic;
		  so :out std_logic);
end lfsr ;

architecture Behavioral of lfsr is
	signal s : std_logic_vector(K downto 0);
begin
-- logica de entrada	
	s(0) <= s(K) xor s(K-1);
-- logica secuencial
	process ( clk , rst )
		begin
		if (rst = '1') then
			s(K downto 1) <= (others =>'0');
		elsif( rising_edge (clk) ) then
			s(K downto 1) <= s(K-1 downto 0);
		end if ;
	end process ;    

-- logica de estado fururo

-- logica de salida
	s0 <= s(K downto 1);
end architecture;