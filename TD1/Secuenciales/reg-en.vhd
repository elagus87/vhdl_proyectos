LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity ffd_sr is
generic(W:natural:=8);
  port (
	   d :in std_logic_vector(W-1 downto 0);
	  en :in std_logic_vector(W-1 downto 0);
	 clk :in std_logic;
     rst :in std_logic;
	   q :out std_logic_vector(W-1 downto 0)) ;
end ffd_sr ;

architecture Behavioral of ffd_sr is
	signal q_reg,q_next : std_logic_vector(W-1 downto 0);
begin
	process ( clk , rst )
		begin
		if (rst = '1') then
			q_reg <= '1';
		elsif( rising_edge (clk) ) then
			q_reg <= q_next;
		end if ;
	end process ;    
-- logica de estado futuro
	q_next <= q_reg when en='0' else
			  d; 
-- logica de salida
	q <= q_reg;
end architecture;