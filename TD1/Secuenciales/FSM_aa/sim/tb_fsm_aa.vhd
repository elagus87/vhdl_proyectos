library ieee;
use ieee.std_logic_1164.all;
USE ieee.numeric_std.ALL;


entity tb_fsm_aa is
end tb_fsm_aa;

architecture tb of tb_fsm_aa is

    component fsm_aa
    port (clk,ini_L,y:in std_logic;
          o: out std_logic);
    end component;

    signal clk : std_logic;
    signal ini_L : std_logic;
    signal y  : std_logic;
    signal o  : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

    begin

        dut : fsm_aa
        port map (clk => clk,
                  ini_L => ini_L,
                  y  => y,
                  o => o);
    
        -- Clock generation
        TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';
    
        -- EDIT: Check that clk is really your main clock signal
        clk <= TbClock;
    
        stimuli : process
        begin--1001
            -- EDIT Adapt initialization as needed
            ini_L <= '0';
            y <= '0';
            wait for 15 ns;
            ini_L <= '1';
            wait for 10 ns;
            y <= '1';
            wait for 10 ns;
            y <= '0';
            wait for 10 ns;
            y <= '0';
            wait for 10 ns;
            y <= '1';
            wait for 10 ns;
            y <= '0';
            wait for 10 ns;
            y <= '0';
            wait for 10 ns;
            y <= '1';
            wait for 10 ns;
            y <= '0';
            wait for 20 ns;

            -- Stop the clock and hence terminate the simulation
            TbSimEnded <= '1';
            wait;
        end process;


end tb;