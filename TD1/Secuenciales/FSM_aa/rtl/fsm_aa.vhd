LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity fsm_aa is
  port (
        clk,ini_L,y:in std_logic;
        o: out std_logic
       ) ;
end fsm_aa ;

architecture behavioral of fsm_aa is
  type state_type is (A,B,C,D);
  signal s_reg, s_next : state_type;
  attribute FSM_ENCODING:string;
  attribute FSM_ENCODING of state_type: type is "one-hot";

  signal ini:std_logic:='0';
begin

  --trucazo
  ini <= not(ini_L);

  state_register : process( clk,ini )
  begin
    if (ini='1') then
      s_reg <= A;
    elsif (rising_edge(clk)) then
      s_reg <= s_next;      
    end if ;
  end process ; -- state_register

  next_state : process( s_reg,y )
  begin
    s_next <= s_reg;
    case( s_reg ) is
      when A =>
        if y='1' then s_next <= B;
        end if;     
      when B =>
        if y='0' then s_next <= C;
        end if;
      when C =>
        if y='0' then s_next <= D;
        else s_next <= C; -- ver
        end if;
      when D =>
        if y='0' then s_next <= A;
        else s_next <= B;
        end if;
    end case ;
  end process ; -- next_state

  out_logic : process( s_reg,y )
  begin
    if (s_reg=D and y='1') then
      o <= '1';
    else
      o <= '0';
    end if ;
  end process ; -- out_logic
end architecture ;