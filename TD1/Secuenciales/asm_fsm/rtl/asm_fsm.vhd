library ieee;
use ieee.std_logic_1164.all;

entity asm_fsm is
  port (
        clk, rst: in std_logic;
        y : in std_logic;
        z0,z1: out std_logic
       );
end asm_fsm ;

architecture behavioral of asm_fsm is
    type state_type is (s0,s1,s2,s3);
    signal s_reg,s_next: state_type;
    attribute FSM_ENCODING : string;
    attribute FSM_ENCODING of s_reg: signal is "auto";
    -- presumo que asignara en binario
begin

    -- registro de estados
    state_register : process( clk,rst )
    begin
        if rst='1' then
            s_reg <= s0;
        elsif ( rising_edge(clk) ) then
            s_reg <= s_next;
        end if ;
    end process ;

    --logica de estado futuro y salidas
    next_out_logic : process( s_reg,y )
    begin
        -- defaults
        s_next <= s_reg;
        z0 <= '0';
        z1 <= '0';
        -- analisis de cambios
        case( s_reg ) is        
            when s0 =>
                if y='1' then s_next <= s2;                    
                end if ;        
            when s1 =>
                s_next <= s2;                           
            when s2 =>
                if y='1' then 
                    s_next <= s3;
                    z1 <= '1';                    
                end if ;        
            when s3 =>
                if y='0' then s_next <= s2; 
                else
                    s_next <= s1; 
                    z0 <= '1';                       
                    z1 <= '1';                       
                end if ;        
        end case ;
    end process ; -- next_out_logic

end architecture;