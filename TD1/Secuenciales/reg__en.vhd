LIBRARY ieee;
USE ieee.std_logic_1164.ALL;


entity ffd_en is
generic(W:natural:=8);
  port (
	   d :in std_logic_vector(W-1 downto 0);
	  en :in std_logic_vector(W-1 downto 0);
	 clk :in std_logic;
     rst :in std_logic;
	   q :out std_logic_vector(W-1 downto 0)) ;
end ffd_en ;

architecture Behavioral of ffd_en is
	signal q_reg,q_next : std_logic_vector(W-1 downto 0);
begin