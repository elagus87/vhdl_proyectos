LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity reg_siso is
generic(K:natural:=8);
  port (
		  si :in std_logic;
		 clk :in std_logic;
	     rst :in std_logic;
		  so :out std_logic);
end reg_siso ;

architecture Behavioral of reg_siso is
	signal s_k : std_logic_vector(K downto 0);
begin
	
	s_k(0) <= si;

	process ( clk , rst )
		begin
		if (rst = '1') then
			s_k(K downto 1) <= (others =>'0');
		elsif( rising_edge (clk) ) then
			s_k(K downto 1) <= s_k(K-1 downto 0);
		end if ;
	end process ;    

-- logica de salida
	s0 <= s_k(K);
end architecture;