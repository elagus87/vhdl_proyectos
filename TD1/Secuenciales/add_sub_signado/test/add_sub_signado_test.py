import cocotb
from cocotb.triggers import Timer
from cocotb.result import TestFailure, TestSuccess
from add_sub_signado_model import add_sub_signado_model
import random

@cocotb.test()
def randomised_test(dut):
	step = 10
	W=4    
	yield Timer(step, units='ns')
	for i in range(20):
		A = random.randint(-2**(W-1), (2**(W-1))-1)
		B = random.randint(-2**(W-1), (2**(W-1))-1)
		#0 suma, 1 resta
		if i>10:
			M = 1
		else:
			M = 0

		dut.a = A
		dut.b = B
		dut.m = M

		yield Timer(step, units='ns')

		test_r = int(add_sub_signado_model(B, A, M))
		r = dut.r.value.signed_integer

		dut._log.info("###########\n Valores de test %d - %d = %d \n ###########" %(int(B),int(A),test_r) )

		if test_r>(2**(W-1))-1 or test_r<-2**(W-1):
			if dut.ov.value==0:
				raise TestFailure("fallo de Overflow, test_r=%d y r=%d" %(test_r,r))
			else:
			 	dut._log.info("Overflow Ok!")
		else:
			if r!= test_r:
				if M==0:
					raise TestFailure("que mal: %d + %d = %d" %(int(B),int(A),r))
				else:
					raise TestFailure("que mal: %d - %d = %d" %(int(B),int(A),r))
			else:
				dut._log.info("cuenta Ok!")

	dut._log.info('\t\t\t\t ######### \n\t\t\t\t TODO BIEN !!! \n\t\t\t\t #########')