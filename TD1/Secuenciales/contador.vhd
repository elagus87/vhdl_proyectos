LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

entity contador is
generic(W:natural:=4); -- entonces cuenta hasta 16
  port (
		   d :in std_logic_vector(W-1 downto 0);
		  ld :in std_logic;
		  en :in std_logic;
		 clk :in std_logic;
	     rst :in std_logic;
	      tc :out std_logic;
		   q :out std_logic);
end contador ;

architecture Behavioral of contador is
	constant ones : unsigned ( W-1 downto 0) := ( others => '1');
	constant zeros : unsigned ( 25 downto 0) := ( others => '0');
	signal count_next, count_reg : unsigned ( W-1 downto 0);
	signal div_next, div_reg : unsigned ( 25 downto 0);
	signal c_end :std_logic;
	signal div_tc:std_logic;
begin
	process ( clk , rst )
	begin
		if( rst = '1' ) then
			count_reg <= ( others => '0');
			div_reg <= ( others => '0');
		elsif( rising_edge ( clk ) ) then
			count_reg <= count_next;
			div_reg <= div_next;
		end if ;
	end process ;

	--logica de estado futuro
	count_next <= d when ld='1' else
				  count_reg + 1 when div_tc='1' else
				  count_reg;
	div_next <= div_reg+1 when en='1' else
				zeros when div_reg="10111110101111000010000000" else
				div_reg;
	div_tc <= '1' when div_reg="10111110101111000010000000" else
			  '0';

	-- termina count
	c_end <= '1' when (count_reg = ones) else
		     '0';
	tc <= '1' when (c_end and en)='1' else
		  '0'; 	

	--logica de salida
	q <= std_logic_vector(count_reg);
end architecture;