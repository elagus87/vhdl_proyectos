library	ieee;
use		ieee.std_logic_1164.all;
use		ieee.numeric_std.all;
use 	ieee.std_logic_textio.all;

entity tb_acum_sat is
end entity tb_acum_sat;

architecture testbench of tb_acum_sat is
	signal tb_arst : std_logic := '0';
	signal tb_clk : std_logic := '0';
	signal tb_bin : std_logic_vector(7 downto 0) := (others => '0');
	signal tb_mode : std_logic := '0';
	signal tb_acc : std_logic_vector(15 downto 0);
	signal tb_sat : std_logic;

    constant CLK_PERIOD : time := 20 ns;
	constant SYN_DELAY : time := 1 ns;
	
begin
	uut : entity work.acum_sat
		port map
		(
			arst => tb_arst,
			clk => tb_clk,
			bin => tb_bin,
			mode => tb_mode,
			acc => tb_acc,
			sat => tb_sat
		);

	clk_process : process
	begin
		wait for CLK_PERIOD / 2;
		tb_clk <= '1';
		wait for CLK_PERIOD / 2;
		tb_clk <= '0';
	end process;
	
	rst_process : process
	begin
		tb_arst <= '0';
		wait for CLK_PERIOD * 2 + SYN_DELAY;
		tb_arst <= '1';
		wait for CLK_PERIOD;
		tb_arst <= '0';

		if (tb_acc /= std_logic_vector(to_unsigned(0, 16))) then
			report "[ERROR] en acc";
			report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(unsigned(tb_acc)));
			report "[ESPERADO] acc = " & INTEGER'IMAGE(0);
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 1***"
			severity failure;
		end if;

		tb_bin <= (others => '1');

		for m in 1 to 256 loop
			wait for CLK_PERIOD;

			if (tb_acc /= std_logic_vector(to_unsigned(m * 255, 16))) then
				report "[ERROR] en acc";
				report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(unsigned(tb_acc)));
				report "[ESPERADO] acc = " & INTEGER'IMAGE(m * 255);
				assert false			
					report "*** ERROR DETECTADO EN LA SIMULACION 2***"
				severity failure;
			end if;
		end loop;

		for m in 1 to 4 loop
			wait for CLK_PERIOD;
		end loop;

		if (tb_acc /= std_logic_vector(to_unsigned(65535, 16))) then
			report "[ERROR] en acc";
			report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(unsigned(tb_acc)));
			report "[ESPERADO] acc = " & INTEGER'IMAGE(65535);
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 3***"
			severity failure;
		end if;

		if (tb_sat /= '1') then
			report "[ERROR] en sat";
			report "[ESPERADO] sat = '1'";
			report "[OBTENIDO] sat = '0'";
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 4***"
			severity failure;
		end if;

		tb_arst <= '0';
		wait for CLK_PERIOD * 2;
		tb_arst <= '1';
		wait for CLK_PERIOD;
		tb_arst <= '0';

		if (tb_acc /= std_logic_vector(to_unsigned(0, 16))) then
			report "[ERROR] en acc";
			report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(unsigned(tb_acc)));
			report "[ESPERADO] acc = " & INTEGER'IMAGE(0);
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 5***"
			severity failure;
		end if;

		tb_bin <= std_logic_vector(to_unsigned(3, 8));
		tb_mode <= '1';

		for m in 1 to 100 loop
			wait for CLK_PERIOD;

			if (tb_acc /= std_logic_vector(to_unsigned(m * 3, 16))) then
				report "[ERROR] en acc";
				report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(unsigned(tb_acc)));
				report "[ESPERADO] acc = " & INTEGER'IMAGE(m * 3);
				assert false			
					report "*** ERROR DETECTADO EN LA SIMULACION 6***"
				severity failure;
			end if;
		end loop;

		tb_bin <= std_logic_vector(to_signed(-3, 8));

		for m in 1 to 100 loop
			wait for CLK_PERIOD;

			if (tb_acc /= std_logic_vector(to_unsigned(300 - m * 3, 16))) then
				report "[ERROR] en acc";
				report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(unsigned(tb_acc)));
				report "[ESPERADO] acc = " & INTEGER'IMAGE(300 - m * 3);
				assert false			
					report "*** ERROR DETECTADO EN LA SIMULACION 7***"
				severity failure;
			end if;
		end loop;

		for m in 1 to 100 loop
			wait for CLK_PERIOD;

			if (tb_acc /= std_logic_vector(to_signed(-m * 3, 16))) then
				report "[ERROR] en acc";
				report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(signed(tb_acc)));
				report "[ESPERADO] acc = " & INTEGER'IMAGE(-m * 3);
				assert false			
					report "*** ERROR DETECTADO EN LA SIMULACION 8***"
				severity failure;
			end if;
		end loop;

		tb_arst <= '0';
		wait for CLK_PERIOD * 2;
		tb_arst <= '1';
		wait for CLK_PERIOD;
		tb_arst <= '0';

		if (tb_acc /= std_logic_vector(to_unsigned(0, 16))) then
			report "[ERROR] en acc";
			report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(unsigned(tb_acc)));
			report "[ESPERADO] acc = " & INTEGER'IMAGE(0);
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 9***"
			severity failure;
		end if;

		tb_bin <= std_logic_vector(to_unsigned(127, 8));

		for m in 1 to 400 loop
			wait for CLK_PERIOD;
		end loop;

		if (tb_acc /= std_logic_vector(to_signed(32767, 16))) then
			report "[ERROR] en acc";
			report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(signed(tb_acc)));
			report "[ESPERADO] acc = " & INTEGER'IMAGE(32767);
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 10***"
			severity failure;
		end if;

		if (tb_sat /= '1') then
			report "[ERROR] en sat";
			report "[ESPERADO] sat = '1'";
			report "[OBTENIDO] sat = '0'";
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 11***"
			severity failure;
		end if;

		tb_arst <= '0';
		wait for CLK_PERIOD * 2;
		tb_arst <= '1';
		wait for CLK_PERIOD;
		tb_arst <= '0';

		if (tb_acc /= std_logic_vector(to_unsigned(0, 16))) then
			report "[ERROR] en acc";
			report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(unsigned(tb_acc)));
			report "[ESPERADO] acc = " & INTEGER'IMAGE(0);
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 12***"
			severity failure;
		end if;

		tb_bin <= std_logic_vector(to_signed(128, 8));

		for m in 1 to 400 loop
			wait for CLK_PERIOD;
		end loop;

		if (tb_acc /= std_logic_vector(to_signed(-32768, 16))) then
			report "[ERROR] en acc";
			report "[OBTENIDO] acc = " & INTEGER'IMAGE(to_integer(signed(tb_acc)));
			report "[ESPERADO] acc = " & INTEGER'IMAGE(-32768);
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 13***"
			severity failure;
		end if;

		if (tb_sat /= '1') then
			report "[ERROR] en sat";
			report "[ESPERADO] sat = '1'";
			report "[OBTENIDO] sat = '0'";
			assert false			
				report "*** ERROR DETECTADO EN LA SIMULACION 14***"
			severity failure;
		end if;

		assert false			
			report "*** SIMULACION EXITOSA :) ***"
		severity failure;
	end process;
end architecture testbench;
