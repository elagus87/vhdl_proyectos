
library ieee;
use ieee.std_logic_1164.all;
USE ieee.numeric_std.ALL;


entity tb_top_cont is
end tb_top_cont;

architecture tb of tb_top_cont is

    component top_cont
        port (d   : in std_logic_vector (3 downto 0);
              ld  : in std_logic;
              en  : in std_logic;
              clk : in std_logic;
              rst : in std_logic;
              tc  : out std_logic;
              seg : out std_logic_vector (7 downto 0));
    end component;

    signal d   : std_logic_vector (3 downto 0);
    signal ld  : std_logic;
    signal en  : std_logic;
    signal clk : std_logic;
    signal rst : std_logic;
    signal tc  : std_logic;
    signal seg : std_logic_vector (7 downto 0);

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : top_cont
    port map (d   => d,
              ld  => ld,
              en  => en,
              clk => clk,
              rst => rst,
              tc  => tc,
              seg => seg);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        d <= (others => '0');
        ld <= '0';
        en <= '0';
        rst <= '1';
        wait for 15 ns;
        rst <= '0';
        wait for 10 ns;
        d <= "0011";
        ld<='1';
        wait for 10 ns;
        ld<='0';
        wait for 10 ns;
        en <= '1';
        wait for 100000000 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;