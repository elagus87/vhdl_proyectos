LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

entity top_cont is
generic(W:natural:=4); -- entonces cuenta hasta 16
    port (
             d :in std_logic_vector(W-1 downto 0);
            ld :in std_logic;
            en :in std_logic;
           clk :in std_logic;
           rst :in std_logic;
            tc :out std_logic;
          seg  :out	std_logic_vector(7 downto 0));
end top_cont ;

architecture structural of top_cont is
  signal count :std_logic_vector(W-1 downto 0);
begin

  contador: entity work.contador
    generic map(W => 4)
    port map(
              d   => d,
              ld  => ld,
              en  => en,
              clk => clk,
              rst => rst,
              tc  => tc,
              q   => count
            );
  bcd7seg_cp: entity work.bcd7seg_cp
    port map(
              sw => count,
             seg => seg
            );
end architecture ; -- structural