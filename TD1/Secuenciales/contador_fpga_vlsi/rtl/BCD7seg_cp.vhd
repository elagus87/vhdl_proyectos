library	ieee;
use		ieee.std_logic_1164.all;

entity	bcd7seg_cp is
	port(
		sw : in		std_logic_vector(3 downto 0);
		seg : out	std_logic_vector(7 downto 0)
		);
end entity bcd7seg_cp;

architecture deco_when of bcd7seg_cp is
begin
	seg <=  "00111111" when ( sw = "0000" ) else
			"00000110" when ( sw = "0001" ) else
			"01011011" when ( sw = "0010" ) else
			"01001111" when ( sw = "0011" ) else
			"01100110" when ( sw = "0100" ) else
			"01101101" when ( sw = "0101" ) else
			"01111100" when ( sw = "0110" ) else
			"00000111" when ( sw = "0111" ) else
			"01111111" when ( sw = "1000" ) else
			"01101111" when ( sw = "1001" ) else
			"00000000";
end architecture deco_when;