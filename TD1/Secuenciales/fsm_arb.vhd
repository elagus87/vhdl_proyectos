library ieee;
use ieee.std_logic_1164.all;

entity fsm_arb is
  port (
        clk, rst: in std_logic;
        x1,x2 : in std_logic;
        y: out std_logic_vector(1 downto 0);
        z: out std_logic
       );
end fsm_arb ;

architecture arch of fsm_arb is
    signal state_reg,state_next:std_logic_vector(1 downto 0);
begin

    -- registro de estados
    state_register : process( clk,rst )
    begin
        if rst='1' then
            state_reg <= (others =>'0');
        elsif ( rising_edge(clk) ) then
            state_reg <= state_next;
        end if ;
    end process ;

    --logica de estado futuro
    nest_state : process( state_reg,x1,x2 )
    begin
        state_next <= state_reg;-- si no lo toco va el registrado
        case( state_reg ) is
            when "10" =>
                if x1='1' then state_next <= "01";                    
                end if ;
            when "01" =>
                if x2='0' then state_next <= "10";                    
                elsif x2='1' then state_next <= "11";                    
                end if ;
            when "11" =>
                if x2='0' then state_next <= "10";                    
                end if ;
            when others =>
                state_next <= "10";
        end case ;
    end process ; -- nest_state

    -- logica de salida z
    output_logic : process( state,x1,x2 )
    begin
        z <= '0'; -- default
        case( state_reg ) is
            when "01" =>
                if x2='0' then z<='1';                    
                end if ;
            when "11" =>
                if x2='0' then z<='1';                    
                end if ;
            when others =>
                z <= '0';        
        end case ;
    end process ; -- output_logic

    -- logica de salida y
    y <= state_reg;
    
end architecture ; -- arch