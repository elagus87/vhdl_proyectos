-------------------------------------------------------
--! @file  dds_top.vhd
--! @brief sum calculation
--! @todo
--! @defgroup dds
-------------------------------------------------------

--! Standard library.
library ieee;
--! Logic elements.
use ieee.std_logic_1164.all;
--! arithmetic functions.
use ieee.numeric_std.all;

--! @brief   implementation
--! @details implementation of xxx
--! @ingroup dds

entity dds_top is
  Port ( arst : in  STD_LOGIC;
         clk : in  STD_LOGIC;
         vel : in  STD_LOGIC_VECTOR (31 downto 0);
         phase : out  STD_LOGIC_VECTOR (31 downto 0));
end dds_top;

architecture rtl of dds_top is

  	signal phase_reg	:	unsigned(31 downto 0);
  	signal phase_next	:	unsigned(31 downto 0);
  	signal vel_s		:  unsigned(31 downto 0);
  begin
  	vel_s <= unsigned(vel);
  registro_de_fase:process(clk,arst)
  	begin
  		if (arst = '1') then
  			phase_reg <= (others=>'0');
  		elsif(rising_edge(clk)) then
  			phase_reg <= phase_next;
  		end if;
  	end process;

  logica_de_estado_futuro:	phase_next <= (phase_reg + vel_s);
  logica_de_salida:				phase <= std_logic_vector(phase_reg);
end rtl;
