-------------------------------------------------------
--! @file  dds_top_pkg.vhd
--! @brief Core package
--! @todo
--! @defgroup dds
-------------------------------------------------------

--! Standard library.
library ieee;
--! Logic elements.
use ieee.std_logic_1164.all;
--! arithmetic functions.
use ieee.numeric_std.all;

--! @brief   package
--! @details package of xxx
--! @ingroup dds

package dds_top_pkg is

  component dds_top is
    port (
      arst  : in  STD_LOGIC;
      clk   : in  STD_LOGIC;
      vel   : in  STD_LOGIC_VECTOR (31 downto 0);
      phase : out STD_LOGIC_VECTOR (31 downto 0)
    );
  end component;
end package;
