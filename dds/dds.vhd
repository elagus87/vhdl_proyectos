----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:50:40 08/04/2017 
-- Design Name: 
-- Module Name:    dds - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity dds is
    Port ( arst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           vel : in  STD_LOGIC_VECTOR (31 downto 0);
           phase : out  STD_LOGIC_VECTOR (31 downto 0));
end dds;

architecture Behavioral of dds is

	signal phase_reg	:	unsigned(31 downto 0);
	signal phase_next	:	unsigned(31 downto 0);
	signal vel_s		:  unsigned(31 downto 0);
begin
	vel_s <= unsigned(vel);
registro_de_fase:process(clk,arst)
	begin
		if (arst = '1') then
			phase_reg <= (others=>'0');
		elsif(rising_edge(clk)) then
			phase_reg <= phase_next;
		end if;
	end process;
	
logica_de_estado_futuro:	phase_next <= (phase_reg + vel_s);
logica_de_salida:			phase <= std_logic_vector(phase_reg);

end Behavioral;

