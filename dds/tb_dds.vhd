--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:31:12 08/09/2017
-- Design Name:   
-- Module Name:   C:/Users/elagu/OneDrive/Documentos/vhdl_proyectos/dds/tb_dds.vhd
-- Project Name:  dds
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: dds
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY tb_dds IS
END tb_dds;
 
ARCHITECTURE behavior OF tb_dds IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT dds
    PORT(
         arst : IN  std_logic;
         clk : IN  std_logic;
         vel : IN  std_logic_vector(31 downto 0);
         phase : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal arst : std_logic := '0';
   signal clk : std_logic := '0';
   signal vel : std_logic_vector(31 downto 0) := (others => '0');

 	--Outputs
   signal phase : std_logic_vector(31 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: dds PORT MAP (
          arst => arst,
          clk => clk,
          vel => vel,
          phase => phase
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
	--variable count : unsigned(16 downto 0):=(others=>'0');
   begin		
		arst<='1';
		wait for clk_period*10;
		arst<='0';
		vel<="00000000000000000000000000000001";--1
		wait for clk_period*10;
		vel<="00000000000000000000000000001010";--10
		wait for clk_period*10;
		vel<="00000000000000000000000001100100";--100
		wait for clk_period*10;
		vel<="10000000000000000000000000000000";--2.147.483.648
		wait for clk_period*100;
		
		assert false 
		report "simulacion terminada" 
		severity FAILURE;
   end process;

END;
